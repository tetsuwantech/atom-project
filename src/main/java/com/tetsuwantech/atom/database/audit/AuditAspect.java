
package com.tetsuwantech.atom.database.audit;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ReflectionUtils;

import com.tetsuwantech.atom.database.GenericEntity;
import com.tetsuwantech.atom.database.authentication.SubjectService;
import com.tetsuwantech.atom.util.DateTimePeriodUtils;
import com.tetsuwantech.atom.util.authentication.SubjectUtils;

@Aspect
public class AuditAspect {

	private static final Logger log = LoggerFactory.getLogger(AuditAspect.class);

	@Inject
	private AuditService auditService;

	@Inject
	private SubjectService subjectService;

	@Inject
	private DateTimePeriodUtils dateTimePeriodUtils;

	/*
	 * target is the field on the class type is the overall object the field is in
	 */
	@SuppressWarnings(value = "unchecked")
	private List<Audit> doField(Auditable auditable, Field field, Action action, Object target, GenericEntity type) throws IllegalArgumentException, IllegalAccessException {

		List<Audit> audits = new LinkedList<>();

		Table table = AnnotationUtils.findAnnotation(type.getClass(), Table.class);
		Audit audit = new Audit(action, subjectService.get(SubjectUtils.getSubject().getId()), table.name(), type.getId(), dateTimePeriodUtils.now());

		// either it's @Column or @JoinColumn and this affects the old and new values
		field.setAccessible(true);
		if (field.isAnnotationPresent(JoinColumn.class)) {

			// A joinColumn means it's a GenericEntity
			if (action != Action.DELETE) {
				String newValue = null;
				if (target != null && ((GenericEntity) target).getId() != null) {
					newValue = String.valueOf(((GenericEntity) target).getId());
				}
				audit.setNewValue(newValue);
			}

			// @JoinColumn means we need to poke inside to get the id - name can't be empty
			JoinColumn joinColumn = field.getAnnotation(JoinColumn.class);
			audit.setColumn(joinColumn.name());

			/*
			 * if INSERT or DELETE we need to process recurse items
			 */
			if (auditable.recurse() && field.get(type) != null) {

				if (action == Action.INSERT || action == Action.DELETE) {
					audits.addAll(doAudit(action, (GenericEntity) field.get(type)));
				}

				/*
				 * if UPDATE and new value is null and recurse, then it's being deleted
				 */
				if (action == Action.UPDATE && target == null) {
					audits.addAll(doAudit(Action.DELETE, (GenericEntity) field.get(type)));
				}
			}

			if (action == Action.UPDATE || action == Action.DELETE) {
				audit.setOldValue(Optional.ofNullable(field.get(type)).map(entity -> String.valueOf(((GenericEntity) entity).getId())).orElse(null));
			}

		} else {

			// For a collection, it's either add all, or delete all
			// update is handled differently with each item being added
			if (target instanceof Collection) {

				// DELETE means we recurse the original field
				if (action == Action.DELETE) {
					for (GenericEntity entity : (List<GenericEntity>) field.get(type)) {
						audits.addAll(doAudit(Action.DELETE, entity));
					}

					// INSERT means we recurse the new field
				} else if (action == Action.INSERT) {
					for (GenericEntity entity : (List<GenericEntity>) target) {
						audits.addAll(doAudit(Action.INSERT, entity));
					}

					// UPDATE means its top level call, and so check for new and removed items
				} else if (action == Action.UPDATE) {

					// old entities
					List<GenericEntity> a = new LinkedList<>((List<GenericEntity>) field.get(type));

					// new entities
					List<GenericEntity> b = new LinkedList<>((List<GenericEntity>) target);

					// check if we are removing items, or adding items
					if (a.size() < b.size()) {
						// remove from b all old entities
						b.removeAll(a);

						for (GenericEntity entity : b) {
							audits.addAll(doAudit(Action.INSERT, entity));
						}
					} else if (b.size() < a.size()) {
						// remove from a all new entities
						a.removeAll(b);

						for (GenericEntity entity : a) {
							audits.addAll(doAudit(Action.DELETE, entity));
						}

					}

				}

			} else {

				if (action != Action.DELETE) {
					audit.setNewValue(Optional.ofNullable(target).map(String::valueOf).orElse(null));
				}

				Column column = field.getAnnotation(Column.class);
				audit.setColumn(column == null || column.name().isEmpty() ? field.getName() : column.name());

				// at this point, if the field was found then this will work
				if (action == Action.UPDATE || action == Action.DELETE) {
					audit.setOldValue(Optional.ofNullable(field.get(type)).map(String::valueOf).orElse(null));
				}
			}

		}

		// if UPDATE, then only add if old and new are different
		if (!StringUtils.equals(audit.getOldValue(), audit.getNewValue()) || action == Action.INSERT && StringUtils.isNotBlank(audit.getNewValue())) {
			audits.add(audit);
		}

		return audits;
	}

	/*
	 * target is the field on the class type is the overall object the field is in
	 */
	private List<Audit> doAudit(Action action, GenericEntity type) throws IllegalArgumentException, IllegalAccessException {

		// collects all audits for all fields we loop over
		List<Audit> audits = new LinkedList<>();

		// list of entities - made grow as we follow recurse = true annotations
		List<GenericEntity> types = new ArrayList<>();
		types.add(type);

		for (GenericEntity entity : types) {

			ReflectionUtils.doWithFields(entity.getClass(), field -> {

				// this might recurse down into a collection
				field.setAccessible(true);
				audits.addAll(doField(field.getAnnotation(Auditable.class), field, action, field.get(entity), entity));

			}, field -> field.isAnnotationPresent(Auditable.class));
			ReflectionUtils.doWithMethods(entity.getClass(), method -> {

				Auditable auditable = method.getAnnotation(Auditable.class);

				Field field = null;
				String fieldName;
				if (auditable.field().isEmpty()) {
					fieldName = method.getName().substring(3, 4).toLowerCase() + method.getName().substring(4);
					try {
						field = entity.getClass().getDeclaredField(fieldName);
					} catch (NoSuchFieldException | SecurityException exception) {
						log.error("AspectJ field not found: {}", fieldName, exception);
					}
				} else {
					try {
						field = entity.getClass().getDeclaredField(auditable.field());
					} catch (NoSuchFieldException | SecurityException exception) {
						log.error("AspectJ field not found: {}", auditable.field(), exception);
					}
				}

				field.setAccessible(true);
				audits.addAll(doField(auditable, field, action, field.get(entity), entity));
			}, method -> method.isAnnotationPresent(Auditable.class));

		}

		return audits;
	}

	@Before("(execution(@Auditable void set*(..)) || set(@Auditable * *)) && @annotation(auditable) && args(target) && target(entity)")
	public void doUpdate(JoinPoint joinPoint, Auditable auditable, Object target, GenericEntity entity) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

		// if we don't find an id, it means we're setting a field on object creation -
		// so skip the rest
		boolean doAudit = entity.getId() != null && SubjectUtils.getSubject() != null && SubjectUtils.getSubject().getId() != null;
		if (doAudit) {

			List<Field> fields = new ArrayList<>();
			switch (joinPoint.getKind()) {

			case JoinPoint.FIELD_SET:

				// this should always work
				ReflectionUtils.doWithFields(entity.getClass(), field -> fields.add(0, field), field -> field.getName().equals(joinPoint.getSignature().getName()));

				break;

			case JoinPoint.METHOD_EXECUTION:

				if (auditable.field().isEmpty()) {
					// this assumes fieldname is xxx, which is same as setXxx()
					fields.add(0, entity.getClass().getDeclaredField(joinPoint.getSignature().getName().substring(3, 4).toLowerCase() + joinPoint.getSignature().getName().substring(4)));

				} else {
					fields.add(0, entity.getClass().getDeclaredField(auditable.field()));
				}

				break;

			default:
				throw new NoSuchFieldException();

			}

			// if the updated field was a collection, this this returns a list of audit
			// objects
			List<Audit> audits = doField(auditable, fields.get(0), Action.UPDATE, target, entity);

			for (Audit audit : audits) {
				auditService.create(audit);
			}

		}
	}

	@Around("execution(GenericEntity com.tetsuwantech.atom.database.GenericDAO.persistOrMerge(GenericEntity)) && args(entity)")
	public GenericEntity doSave(ProceedingJoinPoint joinPoint, GenericEntity entity) throws Throwable {

		// don't audit Audit!
		boolean doAudit = entity.getId() == null && !(entity instanceof Audit) && SubjectUtils.getSubject() != null && SubjectUtils.getSubject().getId() != null;

		// grab result first
		GenericEntity result = (GenericEntity) joinPoint.proceed(new Object[] { entity });

		if (doAudit) {

			List<Audit> audits = doAudit(Action.INSERT, result);

			// loop through collection we got and save each audit object
			for (Audit audit : audits) {
				auditService.create(audit);
			}
		}

		return result;
	}

	@Before("execution(void com.tetsuwantech.atom.database.GenericDAO.remove(GenericEntity)) && args(entity)")
	public void doDelete(JoinPoint joinPoint, GenericEntity entity) throws Throwable {

		boolean doAudit = SubjectUtils.getSubject() != null && SubjectUtils.getSubject().getId() != null;
		if (doAudit) {

			List<Audit> audits = doAudit(Action.DELETE, entity);

			// loop through collection we made above
			for (Audit audit : audits) {
				auditService.create(audit);
			}
		}
	}

}