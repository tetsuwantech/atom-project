
package com.tetsuwantech.atom.database.audit;

import java.time.Instant;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

import com.tetsuwantech.atom.database.GenericEntity;
import com.tetsuwantech.atom.database.authentication.Subject;

@Entity
@Cacheable
@Table(name = "audit")
public class Audit extends GenericEntity {

	private static final long serialVersionUID = 1L;

	// only allow updates to subject
	@NotNull
	@OneToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "subject_id", updatable = false)
	private Subject subject;

	@NotNull
	@Column(name = "audit_table", length = LONG_LENGTH)
	private String table;

	@NotNull
	@Column(name = "audit_id")
	private Long auditId;

	@NotNull
	@Column(name = "audit_date")
	private Instant date;

	@NotNull
	@Column(name = "audit_column", length = LONG_LENGTH)
	private String column;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	@Column(name = "old_value")
	private String oldValue;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	@Column(name = "new_value")
	private String newValue;

	@NotNull
	@Column(length = LENGTH)
	@Enumerated(EnumType.STRING)
	private Action action;

	public Audit() {

	}

	public Audit(Action action, Subject subject, String table, Long auditId, Instant date) {
		this.action = action;
		this.subject = subject;
		this.table = table;
		this.auditId = auditId;
		this.date = date;

		this.column = "";
	}

	/**
	 * @return the subject
	 */
	public Subject getSubject() {
		return subject;
	}

	/**
	 * @param subject
	 *            the subject to set
	 */
	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	/**
	 * @return the table
	 */
	public String getTable() {
		return table;
	}

	/**
	 * @param table
	 *            the table to set
	 */
	public void setTable(String table) {
		this.table = table;
	}

	/**
	 * @return the auditId
	 */
	public Long getAuditId() {
		return auditId;
	}

	/**
	 * @param auditId
	 *            the auditId to set
	 */
	public void setAuditId(Long auditId) {
		this.auditId = auditId;
	}

	/**
	 * @return the date
	 */
	public Instant getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(Instant date) {
		this.date = date;
	}

	/**
	 * @return the column
	 */
	public String getColumn() {
		return column;
	}

	/**
	 * @param column
	 *            the column to set
	 */
	public void setColumn(String column) {
		this.column = column;
	}

	/**
	 * @return the oldValue
	 */
	public String getOldValue() {
		return oldValue;
	}

	/**
	 * @param oldValue
	 *            the oldValue to set
	 */
	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}

	/**
	 * @return the newValue
	 */
	public String getNewValue() {
		return newValue;
	}

	/**
	 * @param newValue
	 *            the newValue to set
	 */
	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}

	/**
	 * @return the action
	 */
	public Action getAction() {
		return action;
	}

	/**
	 * @param action
	 *            the action to set
	 */
	public void setAction(Action action) {
		this.action = action;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Audit [subject=" + subject + ", table=" + table + ", auditId=" + auditId + ", date=" + date + ", column=" + column + ", oldValue=" + oldValue + ", newValue=" + newValue + ", action=" + action + "]";
	}
}
