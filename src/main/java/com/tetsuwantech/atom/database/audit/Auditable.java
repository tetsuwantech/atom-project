
package com.tetsuwantech.atom.database.audit;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotates an auditable field or method. Tracks data changes for INSERT,
 * UPDATE or DELETE in the audit table.
 *
 * @author Jim Richards
 */
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Auditable {

	/**
	 * (Optional) The name of the field if different to the set/get method
	 * 
	 * @return default value
	 */
	public String field() default "";

	/**
	 * (Optional) Tells the audit tool to recurse into the
	 * {@link com.tetsuwantech.atom.database.GenericEntity} and audit all the
	 * sub-entities. Usually used with orphanRemoval = true or
	 * {@link javax.persistence.CascadeType} ALL or REMOVE actions.
	 * 
	 * @return false
	 */
	public boolean recurse() default false;

}
