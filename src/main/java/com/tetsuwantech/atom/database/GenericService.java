
package com.tetsuwantech.atom.database;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.List;

public interface GenericService<E extends GenericEntity, K> {

	void create(E object);

	E save(E object);

	E get(Long id);

	void remove(Long id);

	List<E> findAll();

	List<E> findAll(K where);

	List<E> findAll(K where, String column);

	List<E> findAll(K where, String column, String orderBy);

	E findFirst(K where);

	E findFirst(K where, String column);

	Long count(K where);

	Long count(K where, String column);

	List<E> search(K where, String column, String text, String[] fields);
}
