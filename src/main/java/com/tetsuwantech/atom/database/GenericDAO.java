
package com.tetsuwantech.atom.database;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.lang.reflect.Field;
import java.util.List;
import java.util.Optional;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.SingularAttribute;

import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.hibernate.jpa.QueryHints;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named(value = "GenericDAO")
public abstract class GenericDAO<E extends GenericEntity, K> {

	private static final Logger log = LoggerFactory.getLogger(GenericDAO.class);

	protected Class<E> type;
	private Field id;

	@PersistenceContext
	protected EntityManager entityManager;

	public GenericDAO() {
	}

	public GenericDAO(Class<E> type) {
		this.type = type;

		for (Field field : GenericEntity.class.getDeclaredFields()) {
			if (field.getAnnotation(Id.class) != null) {
				this.id = field;
				break;
			}
		}

	}

	// https://hibernate.atlassian.net/browse/HHH-4910
	public E persistOrMerge(E object) {

		// check if it's new
		if (object.getId() == null) {
			// this will create a new object, and keep it in object
			entityManager.persist(object);
		} else {
			// need to keep the returned object
			object = entityManager.merge(object);
		}

		return object;
	}

	public E find(Long id) {
		return Optional.ofNullable(id).map(i -> entityManager.find(type, i)).orElse(null);
	}

	public void remove(E object) {
		// needs to be managed
		entityManager.remove(object);
	}

	protected void where(CriteriaBuilder criteriaBuilder, CriteriaQuery<?> criteriaQuery, Root<E> rootQuery, K where, String column) {

		if (where != null) {

			SingularAttribute<? super E, ?> attribute = null;
			EntityType<E> entity = entityManager.getMetamodel().entity(type);
			for (SingularAttribute<? super E, ?> singleAttribute : entity.getSingularAttributes()) {

				// we want column name, or where.class rather than Long here
				if (column != null && singleAttribute.getName().equals(column) || column == null && singleAttribute.getJavaType().equals(where.getClass())) {
					// winner!
					attribute = singleAttribute;
					break;
				}
			}

			// where t.object = object.getID()
			criteriaQuery.where(criteriaBuilder.equal(rootQuery.get(attribute), where));
		}

	}

	public List<E> findAll(K where, String column, String orderBy) {

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<E> criteriaQuery = criteriaBuilder.createQuery(type);
		Root<E> rootQuery = criteriaQuery.from(type);
		criteriaQuery.select(rootQuery);

		where(criteriaBuilder, criteriaQuery, rootQuery, where, column);

		criteriaQuery.orderBy(criteriaBuilder.asc(rootQuery.get(orderBy != null ? orderBy : id.getName())));
		TypedQuery<E> query = entityManager.createQuery(criteriaQuery);
		query.setHint(QueryHints.HINT_CACHEABLE, true);

		return query.getResultList();
	}

	public E findFirst(K where, String column) {

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<E> criteriaQuery = criteriaBuilder.createQuery(type);
		Root<E> rootQuery = criteriaQuery.from(type);
		criteriaQuery.select(rootQuery);

		where(criteriaBuilder, criteriaQuery, rootQuery, where, column);

		TypedQuery<E> query = entityManager.createQuery(criteriaQuery);
		query.setHint(QueryHints.HINT_CACHEABLE, true);
		query.setMaxResults(1);

		List<E> results = query.getResultList();
		return results.isEmpty() ? null : results.get(0);
	}

	public Long count(K where, String column) {

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
		Root<E> rootQuery = criteriaQuery.from(type);
		criteriaQuery.select(criteriaBuilder.count(rootQuery));

		where(criteriaBuilder, criteriaQuery, rootQuery, where, column);

		TypedQuery<Long> query = entityManager.createQuery(criteriaQuery);
		query.setHint(QueryHints.HINT_CACHEABLE, true);

		return query.getSingleResult();
	}

	public List<E> search(K where, String column, String text, String[] fields) {

		FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
		QueryBuilder queryBuilder = fullTextEntityManager.getSearchFactory().buildQueryBuilder().forEntity(type).get();

		// need to have better solution for onFields() - we can scan for @Field annotations ...
		Query textQuery = queryBuilder.keyword().onFields(fields).matching(text).createQuery();
		BooleanQuery.Builder booleanQuery = new BooleanQuery.Builder();
		booleanQuery.add(textQuery, BooleanClause.Occur.MUST);

		if (where != null) {

			// The gets the field name from type
			EntityType<E> entity = entityManager.getMetamodel().entity(type);
			SingularAttribute<? super E, ?> attribute = null;
			for (SingularAttribute<? super E, ?> singleAttribute : entity.getSingularAttributes()) {

				// we want column name, or where.class rather than Long here
				if (column != null && singleAttribute.getName().equals(column) || column == null && singleAttribute.getJavaType().equals(where.getClass())) {
					// winner!
					attribute = singleAttribute;
					break;
				}
			}

			// this gets the value of the column from where
			Class<?> target = where.getClass();
			Object value = null;
			do {
				try {
					Field field = target.getDeclaredField(StringUtils.defaultIfEmpty(column, "id"));
					field.setAccessible(true);
					value = field.get(where);
				} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException exception) {
					log.error("Unable to get or access column: {}", column, exception);
				}
			} while ((target = target.getSuperclass()) != null && value == null);

			Query whereQuery = queryBuilder.keyword().onField(attribute.getName() + "." + StringUtils.defaultIfEmpty(column, "id")).matching(value).createQuery();
			booleanQuery.add(whereQuery, BooleanClause.Occur.MUST);

		}

		Query query = booleanQuery.build();
		FullTextQuery fullTextQuery = fullTextEntityManager.createFullTextQuery(query, type);
		fullTextQuery.setHint(QueryHints.HINT_CACHEABLE, true);

		@SuppressWarnings(value = "unchecked")
		List<E> results = fullTextQuery.getResultList();

		return results;
	}

}
