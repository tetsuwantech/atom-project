
package com.tetsuwantech.atom.database.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.List;

import javax.inject.Named;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tetsuwantech.atom.database.GenericServiceImpl;

@Transactional
@Named(value = "SubjectService")
public class SubjectServiceImpl extends GenericServiceImpl<Subject, String> implements SubjectService {

	@SuppressWarnings(value = "unused")
	private static final Logger log = LoggerFactory.getLogger(SubjectServiceImpl.class);

	@Override
	public Subject get(Long id) {

		Subject subject = super.get(id);

		subject.getAuthorities().size();

		return subject;
	}

	@Override
	public List<Subject> findAll(String where, String column, String orderBy) {

		List<Subject> subjects = super.findAll(where, column, orderBy);
		subjects.forEach(subject -> subject.getAuthorities().size());

		return subjects;
	}

	@Override
	public Subject findFirst(String where, String column) {

		Subject subject = super.findFirst(where, column);

		if (subject != null) {
			subject.getAuthorities().size();
		}

		return subject;
	}

}
