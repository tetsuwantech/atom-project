
package com.tetsuwantech.atom.database.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

import com.tetsuwantech.atom.database.GenericEntity;
import com.tetsuwantech.atom.database.audit.Auditable;

@Entity
@Cacheable
@Table(name = "subject_authority")
public class Authority extends GenericEntity implements GrantedAuthority {

	private static final long serialVersionUID = 1L;

	@Auditable
	@ManyToOne(targetEntity = Subject.class)
	@JoinColumn(name = "subject_id", updatable = false)
	private Subject subject;

	@Auditable
	@Column(length = LENGTH)
	@Enumerated(EnumType.STRING)
	private Type authority;

	public enum Type {
		ROLE_ANONYMOUS,
		ROLE_USER,
		ROLE_ADMIN;
	}

	/**
	 * @return the subject
	 */
	public Subject getSubject() {
		return subject;
	}

	/**
	 * @param subject
	 *            the subject to set
	 */
	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	/**
	 * @return the authority
	 */
	@Override
	public String getAuthority() {
		return authority.toString();
	}

	/**
	 * @param authority
	 *            the authority to set
	 */
	public void setAuthority(String authority) {
		this.authority = Type.valueOf(authority);
	}

	public void setAuthority(Type authority) {
		this.authority = authority;
	}

}
