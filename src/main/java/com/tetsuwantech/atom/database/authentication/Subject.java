
package com.tetsuwantech.atom.database.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.time.Instant;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Store;

import com.tetsuwantech.atom.database.GenericEntity;
import com.tetsuwantech.atom.database.audit.Auditable;

@Entity
@Cacheable
@Table(name = "subject", uniqueConstraints = @UniqueConstraint(columnNames = "username"))
public class Subject extends GenericEntity {

	private static final long serialVersionUID = 1L;

	@Auditable
	@Column(length = LENGTH, unique = true, updatable = true)
	@Email
	@NotNull
	@Size(min = 1, max = LENGTH)
	private String username;

	@Auditable
	@Column(length = LENGTH)
	@Size(min = 0, max = LENGTH)
	private String password;

	@OneToMany(targetEntity = Authority.class, mappedBy = "subject", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Authority> authorities = new LinkedList<>();

	@Auditable
	@Column(name = "joined")
	private Instant joined;

	@Auditable
	@Column(name = "password_expires")
	private Instant passwordExpires;

	@Auditable
	@Column(name = "last_login")
	private Instant lastLogin;

	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	@Auditable
	@Column(name = "given_name", length = LENGTH)
	@NotNull
	@Size(min = 1, max = LENGTH)
	private String givenName;

	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	@Auditable
	@Column(name = "family_name", length = LENGTH)
	@NotNull
	@Size(min = 1, max = LENGTH)
	private String familyName;

	@Auditable
	@Column(name = "time_zone")
	@NotNull
	@Size(min = 1, max = LENGTH)
	private String timeZone = TimeZone.getDefault().getID();

	@Auditable
	@Column(length = LENGTH, unique = true)
	private String uuid;

	@Auditable
	@Column(name = "uuid_expires")
	private Instant uuidExpires;

	@Auditable
	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private Status status = Status.PENDING;

	public enum Status {
		ENABLED,
		EXPIRED,
		PENDING,
		LOCKED;
	}

	public Subject() {
	}

	public Subject(String username) {
		this.username = username;
		this.givenName = "";
		this.familyName = "";
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Collection<Authority> getAuthorities() {
		return this.authorities;
	}

	@Auditable(recurse = true)
	public void setAuthorities(Collection<Authority> authorities) {
		this.authorities.clear();
		this.authorities.addAll(authorities);
	}

	/**
	 * @return the startDate
	 */
	public Instant getJoined() {
		return joined;
	}

	public void setJoined(Instant joined) {
		this.joined = joined;
	}

	/**
	 * @return the passwordExpires
	 */
	public Instant getPasswordExpires() {
		return this.passwordExpires;
	}

	/**
	 * @param passwordExpires
	 *            the passwordExpires to set
	 */
	public void setPasswordExpires(Instant passwordExpires) {
		this.passwordExpires = passwordExpires;
	}

	/**
	 * @return the lastLogin
	 */
	public Instant getLastLogin() {
		return this.lastLogin;
	}

	/**
	 * @param lastLogin
	 *            the lastLogin to set
	 */
	public void setLastLogin(Instant lastLogin) {
		this.lastLogin = lastLogin;
	}

	/**
	 * @return the givenName
	 */
	public String getGivenName() {
		return givenName;
	}

	/**
	 * @param givenName
	 *            the givenName to set
	 */
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	/**
	 * @return the familyName
	 */
	public String getFamilyName() {
		return familyName;
	}

	/**
	 * @param familyName
	 *            the familyName to set
	 */
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public String getFullName() {
		return StringUtils.stripToEmpty(getGivenName() + " " + getFamilyName());
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	/**
	 * @return the UUID
	 */
	public String getUuid() {
		return uuid;
	}

	/**
	 * @param uuid
	 *            the UUID to set
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	/**
	 * @return the expires
	 */
	public Instant getUuidExpires() {
		return uuidExpires;
	}

	/**
	 * @param uuidExpires
	 */
	public void setUuidExpires(Instant uuidExpires) {
		this.uuidExpires = uuidExpires;
	}

	/**
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(Status status) {
		this.status = status;
	}

}
