
package com.tetsuwantech.atom.database.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tetsuwantech.atom.database.GenericEntity;
import com.tetsuwantech.atom.database.GenericService;
import com.tetsuwantech.atom.database.GenericServiceImpl;
import com.tetsuwantech.atom.util.authentication.SubjectUtils;
import com.tetsuwantech.atom.web.authentication.SubjectModel;

@Transactional
public abstract class SecureServiceImpl<E extends GenericEntity, K> extends GenericServiceImpl<E, K> implements GenericService<E, K> {

	@SuppressWarnings(value = "unused")
	private static final Logger log = LoggerFactory.getLogger(SecureServiceImpl.class);

	private void validate(E entity) {

		if (!(entity instanceof Securable)) {
			throw new NotSecurableException(entity.getClass().toString());
		}

		// only check if user is logged in - you will need to make sure URLs are secured
		SubjectModel subject = SubjectUtils.getSubject();
		if (subject != null && !((Securable) entity).getSubject().getId().equals(subject.getId())) {
			throw new NotAllowedException("subject not owner: " + subject.getUsername());
		}

	}

	@Override
	public E get(Long id) {

		// check we have permission to edit this
		E entity = super.get(id);

		validate(entity);

		return entity;
	}

	@Override
	public E save(E entity) {

		// check we have permission to save this (id can't be null)
		validate(super.get(entity.getId()));

		// and save
		return super.save(entity);
	}

	@Override
	public void remove(Long id) {

		// check we have permission to edit this
		validate(super.get(id));

		// check we have permission to edit this
		super.remove(id);
	}

}
