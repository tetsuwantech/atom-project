
package com.tetsuwantech.atom.database;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Transactional
@Named(value = "GenericService")
public abstract class GenericServiceImpl<E extends GenericEntity, K> implements GenericService<E, K> {

	@PersistenceContext
	private EntityManager entityManager;

	@Inject
	private GenericDAO<E, K> genericDAO;

	@Override
	public void create(E object) {

		if (object.getId() != null) {
			throw new CreateNotAllowedException("Object has id:" + object.getId() + ".");
		}

		genericDAO.persistOrMerge(object);
	}

	@Override
	public E save(E object) {

		if (object.getId() == null) {
			throw new SaveNotAllowedException("Object id is null.");
		}

		return genericDAO.persistOrMerge(object);
	}

	@Override
	public E get(Long id) {
		E entity = genericDAO.find(id);

		if (entity == null) {
			throw new EntityNotFoundException("Object with " + id + " not found.");
		}

		// make sure the entity is cached
		if (entity instanceof Deletable) {
			((Deletable) entity).isDeletable();
		}

		return entity;
	}

	@Override
	public void remove(Long id) {

		E entity = genericDAO.find(id);
		if (entity instanceof Deletable && !((Deletable) entity).isDeletable()) {
			throw new NotDeletableException("Object not deletable with id: " + entity.getId());
		}

		genericDAO.remove(entity);
	}

	@Override
	public List<E> findAll() {
		return findAll(null, null, null);
	}

	@Override
	public List<E> findAll(K where) {
		return findAll(where, null, null);
	}

	@Override
	public List<E> findAll(K where, String column) {
		return findAll(where, column, null);
	}

	@Override
	public List<E> findAll(K where, String column, String orderBy) {

		List<E> entities = genericDAO.findAll(where, column, orderBy);
		entities.stream().filter(entity -> entity instanceof Deletable).forEach(entity -> ((Deletable) entity).isDeletable());

		return entities;
	}

	@Override
	public E findFirst(K where) {
		return findFirst(where, null);
	}

	@Override
	public E findFirst(K where, String column) {
		E entity = genericDAO.findFirst(where, column);

		if (entity instanceof Deletable) {
			((Deletable) entity).isDeletable();
		}

		return entity;
	}

	@Override
	public Long count(K where) {
		return count(where, null);
	}

	@Override
	public Long count(K where, String column) {
		return genericDAO.count(where, column);
	}

	@Override
	public List<E> search(K where, String column, String text, String[] fields) {

		List<E> entities = genericDAO.search(where, column, text, fields);
		entities.stream().filter(entity -> entity instanceof Deletable).forEach(entity -> ((Deletable) entity).isDeletable());

		return entities;
	}

}
