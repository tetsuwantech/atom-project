
package com.tetsuwantech.atom.util;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 - 2019 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.converter.Converter;

// https://www.petrikainulainen.net/programming/spring-framework/spring-from-the-trenches-using-type-converters-with-spring-mvc/
public class InstantConverter implements Converter<String, Instant> {

	private final DateTimeFormatter dateTimeFormatter;

	public InstantConverter(String dateTimeFormat) {
		this.dateTimeFormatter = DateTimeFormatter.ofPattern(dateTimeFormat);
	}

	@Override
	public Instant convert(String source) {
		return StringUtils.isEmpty(source) ? null : LocalDateTime.parse(source, dateTimeFormatter).atZone(TimeZoneUtil.getServerTimeZone()).toInstant();
	}
}
