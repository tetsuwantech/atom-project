package com.tetsuwantech.atom.util.authentication;

import java.util.function.Consumer;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.tetsuwantech.atom.web.authentication.SubjectModel;

public class SubjectUtils {

	public static final String ANONYMOUS_USER = "anonymousUser";

	private SubjectUtils() {

	}

	public static SubjectModel getSubject() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		SubjectModel subjectModel = null;
		if (authentication != null && authentication.getPrincipal() instanceof SubjectModel) {
			subjectModel = (SubjectModel) authentication.getPrincipal(); // get logged in username (if not logged in, then anonymousUser/null)
		}

		return subjectModel;
	}

	public static void login(SubjectModel subject) {
		// log user in for this - should be fine from here
		Authentication authentication = new UsernamePasswordAuthenticationToken(subject, subject.getPassword(), subject.getAuthorities());
		SecurityContext securityContext = SecurityContextHolder.getContext();
		securityContext.setAuthentication(authentication);
	}

	public static void logout(SubjectModel subject) {
		SecurityContext securityContext = SecurityContextHolder.getContext();
		securityContext.getAuthentication().setAuthenticated(false);
	}

	public static void doAuthenticatedAction(SubjectModel subject, Consumer<SubjectModel> subjectConsumer) {
		doAuthenticatedAction(subject, subject, subjectConsumer);
	}

	public static <T> void doAuthenticatedAction(SubjectModel subject, T argument, Consumer<T> subjectConsumer) {

		login(subject);
		subjectConsumer.accept(argument);
		logout(subject);
	}

}
