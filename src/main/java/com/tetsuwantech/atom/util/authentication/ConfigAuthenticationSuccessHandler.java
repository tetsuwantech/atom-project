
package com.tetsuwantech.atom.util.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.util.Optional;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import com.tetsuwantech.atom.web.authentication.SubjectModel;

public class ConfigAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

	@SuppressWarnings(value = "unused")
	private static final Logger log = LoggerFactory.getLogger(ConfigAuthenticationSuccessHandler.class);

	@Inject
	private SubjectHandler subjectHandler;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {

		SubjectModel subject = (SubjectModel) authentication.getPrincipal(); // get logged in Subject

		Optional.ofNullable(subjectHandler).ifPresent(handler -> handler.login(subject, request, response));

		super.onAuthenticationSuccess(request, response, authentication);
	}
}
