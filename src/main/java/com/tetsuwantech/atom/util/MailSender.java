
package com.tetsuwantech.atom.util;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;

import com.tetsuwantech.atom.web.authentication.SubjectModel;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;

public class MailSender {

	private static final Logger log = LoggerFactory.getLogger(MailSender.class);

	private String atomFrom;
	private String atomBcc;
	private String atomSubject;

	private Configuration freemarkerConfiguration;
	private JavaMailSender javaMailSender;

	public enum Message {
		WELCOME("welcome.ftl", "Welcome"),
		FORGOT_PASSWORD("forgot_password.ftl", "You forgot your password"),
		CREATE_ACCOUNT("create_account.ftl", "Please validate your account"),
		PAYMENT_FAILED("payment_failed.ftl", "Your subscription payment failed"),
		DEACTIVATE_ACCOUNT("deactivate_account.ftl", "Account deactivation");

		private String path;
		private String subject;

		Message(String path, String subject) {
			this.path = path;
			this.subject = subject;
		}

		/**
		 * @return the path
		 */
		public String getPath() {
			return path;
		}

		/**
		 * @return the subject
		 */
		public String getSubject() {
			return subject;
		}
	}

	public MailSender() {
	}

	public MailSender(Configuration freemarkerConfiguration, JavaMailSender javaMailSender) {
		setFreemarkerConfiguration(freemarkerConfiguration);
		setJavaMailSender(javaMailSender);
	}

	public void setFreemarkerConfiguration(Configuration freemarkerConfiguration) {
		this.freemarkerConfiguration = freemarkerConfiguration;
	}

	public void setJavaMailSender(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
	}

	@Inject
	void setAtomFrom(@Value("${atom.from:}") String atomFrom) {
		this.atomFrom = atomFrom;
	}

	@Inject
	void setAtomBcc(@Value("${atom.bcc:}") String atomBcc) {
		this.atomBcc = atomBcc;
	}

	@Inject
	void setAtomSubject(@Value("${atom.subject:}") String atomSubject) {
		this.atomSubject = atomSubject;
	}

	@Async
	public void send(SubjectModel subject, Message message, Map<String, Object> content) {

		try {

			MimeMessage mimeMessage = javaMailSender.createMimeMessage();

			// use the the flag to indicate you need a multipart message
			MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
			mimeMessageHelper.setFrom(atomFrom);
			mimeMessageHelper.setTo(new InternetAddress(subject.getUsername(), subject.getFullName()));
			if (StringUtils.isNotEmpty(atomBcc))
				mimeMessageHelper.setBcc(atomBcc);
			mimeMessageHelper.setSubject(atomSubject + (StringUtils.isNotBlank(atomSubject) ? " " : "") + message.getSubject());

			// set up freemarker to load template
			freemarker.template.Template template = freemarkerConfiguration.getTemplate("mail/template.ftl");
			content.put("path", message.getPath());

			Writer writer = new StringWriter();
			template.process(content, writer);
			mimeMessageHelper.setText(writer.toString(), true);

			javaMailSender.send(mimeMessage);

			log.info("mailto = {}, type = {}, subject = {}", subject.getUsername(), message.name(), message.getSubject());

		} catch (MessagingException | IOException | TemplateException exception) {
			log.error("Unable to send email", exception);
		}

	}

}
