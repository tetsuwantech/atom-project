
package com.tetsuwantech.atom.util;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.time.Instant;

import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class DateTimePeriodUtils {

	@SuppressWarnings(value = "unused")
	private static final Logger log = LoggerFactory.getLogger(DateTimePeriodUtils.class);

	public static boolean isBetween(DateTimePeriod period) {

		// where start_date <= current_timestamp and (end_date is null or end_date >=
		// current_timestamp)
		Instant now = Instant.now();
		return period.getStartDate().isBefore(now) && (period.getEndDate() == null || period.getEndDate().isAfter(now));
	}

	public static boolean isNotBetween(DateTimePeriod period) {
		return !isBetween(period);
	}

	public Instant now() {
		return Instant.now();
	}

}
