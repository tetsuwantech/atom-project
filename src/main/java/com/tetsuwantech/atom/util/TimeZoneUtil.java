package com.tetsuwantech.atom.util;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 - 2019 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Value;

public class TimeZoneUtil {

	private static ZoneId serverTimeZone;
	private static Map<String, String> allTimeZones = new LinkedHashMap<>();

	static {

		Set<String> allZones = ZoneId.getAvailableZoneIds();
		Instant now = Instant.now();

		// the set is not sorted, so we bag them up first
		Map<ZoneId, ZoneOffset> unsortedTimeZones = new HashMap<>();
		for (String zone : allZones) {
			ZoneId zoneId = ZoneId.of(zone);
			ZonedDateTime zonedDateTime = now.atZone(zoneId);
			ZoneOffset offset = zonedDateTime.getOffset();

			unsortedTimeZones.put(zoneId, offset);
		}

		// we want to start with -12:00 and go to +12:00, and for same time compare strings
		Comparator<Entry<ZoneId, ZoneOffset>> zoneOffsetCompare = (z1, z2) -> {
			if (z1.getValue().equals(z2.getValue())) {
				return z1.getKey().getId().compareTo(z2.getKey().getId());
			} else {
				return z1.getValue().compareTo(z2.getValue()) >= 0 ? -1 : 1;
			}
		};

		// sort them by offset
		unsortedTimeZones.entrySet().stream().sorted(zoneOffsetCompare).forEachOrdered(entry -> allTimeZones.put(entry.getKey().getId(), String.format("(UTC%s) %s", entry.getValue().getId().replace("Z", "+00:00"), entry.getKey().getId())));
	}

	public static Instant toServerTimeZone(Instant instant, ZoneId zoneId) {
		LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
		ZonedDateTime zonedTimeDate = ZonedDateTime.of(localDateTime, zoneId);
		return zonedTimeDate.withZoneSameInstant(serverTimeZone).toInstant();
	}

	public static Map<String, String> getAllTimeZones() {
		return allTimeZones;
	}

	@Inject
	void setTimeZone(@Value("${atom.timeZone:UTC}") String atomTimeZone) {
		serverTimeZone = ZoneId.of(atomTimeZone);
	}

	public static ZoneId getServerTimeZone() {
		return serverTimeZone;
	}

}
