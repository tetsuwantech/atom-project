
package com.tetsuwantech.atom.web.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.tetsuwantech.atom.database.authentication.Subject;
import com.tetsuwantech.atom.web.GenericModel;

public class NameModel extends GenericModel {

	private static final long serialVersionUID = 1L;

	@NotNull
	@Size(min = 1, max = LENGTH)
	private String givenName;

	@NotNull
	@Size(min = 1, max = LENGTH)
	private String familyName;

	public NameModel() {
		this("", "");
	}

	public NameModel(Subject subject) {
		super(subject);
		this.givenName = subject.getGivenName();
		this.familyName = subject.getFamilyName();
	}

	public NameModel(String givenName, String familyName) {
		this.givenName = givenName;
		this.familyName = familyName;
	}

	/**
	 * @return the givenName
	 */
	public String getGivenName() {
		return givenName;
	}

	/**
	 * @param givenName
	 *            the givenName to set
	 */
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	/**
	 * @return the familyName
	 */
	public String getFamilyName() {
		return familyName;
	}

	/**
	 * @param familyName
	 *            the familyName to set
	 */
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

}
