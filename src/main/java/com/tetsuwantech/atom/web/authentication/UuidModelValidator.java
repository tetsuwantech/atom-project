
package com.tetsuwantech.atom.web.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.tetsuwantech.atom.database.authentication.Subject.Status;
import com.tetsuwantech.atom.manager.authentication.SubjectManager;
import com.tetsuwantech.atom.util.DateTimePeriodUtils;

public class UuidModelValidator implements Validator {

	@SuppressWarnings(value = "unused")
	private static final Logger log = LoggerFactory.getLogger(UuidModelValidator.class);

	private SubjectManager subjectManager;
	private DateTimePeriodUtils dateTimePeriodUtils;

	public UuidModelValidator() {
	}

	public UuidModelValidator(SubjectManager subjectManager, DateTimePeriodUtils dateTimePeriodUtils) {
		this.subjectManager = subjectManager;
		this.dateTimePeriodUtils = dateTimePeriodUtils;
	}

	public SubjectManager getSubjectManager() {
		return subjectManager;
	}

	public void setSubjectManager(SubjectManager subjectManager) {
		this.subjectManager = subjectManager;
	}

	@Override
	public boolean supports(Class<?> target) {
		return UuidModel.class.equals(target);
	}

	@Override
	public void validate(Object object, Errors errors) {

		UuidModel uuid = (UuidModel) object;

		if (subjectManager != null) {

			// check uuid's subject is valid
			SubjectModel subject = subjectManager.findFirst(uuid.getUuid(), "uuid");

			if (subject == null) {
				errors.rejectValue("uuid", "Subject.notFound");
			} else {
				// check if the subject should be pending
				if (!uuid.isEnabled() && subject.getStatus() != Status.PENDING) {
					errors.rejectValue("uuid", "Subject.notEnabled");
				} else if (uuid.isEnabled() && subject.getStatus() != Status.ENABLED) {
					errors.rejectValue("uuid", "Subject.alreadyEnabled");
				}

				if (subject.getUuidExpired().isBefore(dateTimePeriodUtils.now())) {
					errors.rejectValue("uuid", "Uuid.expired");
				}
			}
		}

		try {
			UUID.fromString(uuid.getUuid());
		} catch (IllegalArgumentException exception) {
			errors.rejectValue("uuid", "Uuid.notValid");
		}

	}
}
