
package com.tetsuwantech.atom.web.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 - 2020 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tetsuwantech.atom.manager.authentication.SubjectManager;

@Controller
@RequestMapping(path = "/admin")
public class AdminController {

	@SuppressWarnings(value = "unused")
	private static final Logger log = LoggerFactory.getLogger(AdminController.class);

	@Inject
	private SubjectManager subjectManager;

	@GetMapping(value = "/subject")
	public String admin(Model model) {

		// consider an admin controller
		List<SubjectModel> subjects = subjectManager.findAll();
		model.addAttribute("subjects", subjects);

		return "/subjects/main";
	}

}
