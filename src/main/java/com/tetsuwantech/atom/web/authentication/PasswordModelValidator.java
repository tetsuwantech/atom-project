
package com.tetsuwantech.atom.web.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import edu.vt.middleware.password.AlphabeticalSequenceRule;
import edu.vt.middleware.password.CharacterCharacteristicsRule;
import edu.vt.middleware.password.DigitCharacterRule;
import edu.vt.middleware.password.LengthRule;
import edu.vt.middleware.password.NonAlphanumericCharacterRule;
import edu.vt.middleware.password.Password;
import edu.vt.middleware.password.PasswordData;
import edu.vt.middleware.password.PasswordValidator;
import edu.vt.middleware.password.QwertySequenceRule;
import edu.vt.middleware.password.RepeatCharacterRegexRule;
import edu.vt.middleware.password.Rule;
import edu.vt.middleware.password.RuleResult;

public class PasswordModelValidator implements Validator {

	@SuppressWarnings(value = "unused")
	private static final Logger log = LoggerFactory.getLogger(PasswordModelValidator.class);

	@Override
	public boolean supports(Class<?> target) {
		return PasswordModel.class.equals(target);
	}

	@Override
	public void validate(Object object, Errors errors) {

		PasswordModel password = (PasswordModel) object;

		// check passwords match, shouldn't be null at this stage
		if (StringUtils.isNotEmpty(password.getPassword1())) {

			if (!password.getPassword1().equals(password.getPassword2())) {
				errors.rejectValue("password2", "Password.notSame");
			} else {

				// check password meets validation rules
				List<Rule> ruleList = new ArrayList<>();

				// minimum length 6
				ruleList.add(new LengthRule(6, 56));

				// at least one digit
				CharacterCharacteristicsRule characterRule = new CharacterCharacteristicsRule();
				characterRule.setNumberOfCharacteristics(1);
				characterRule.getRules().add(new DigitCharacterRule(1));
				characterRule.getRules().add(new NonAlphanumericCharacterRule(1));
				ruleList.add(characterRule);

				// don't allow alphabetical sequences longer that 3 characters
				ruleList.add(new AlphabeticalSequenceRule(3, false));

				// don't allow qwerty sequences
				ruleList.add(new QwertySequenceRule(3, false));

				// don't allow 3 repeat characters
				ruleList.add(new RepeatCharacterRegexRule(3));

				PasswordValidator passwordValidator = new PasswordValidator(ruleList);
				PasswordData passwordData = new PasswordData(new Password(password.getPassword1()));
				RuleResult ruleResult = passwordValidator.validate(passwordData);

				// check new password matches rules
				List<String> messages = passwordValidator.getMessages(ruleResult);
				messages.forEach(message -> errors.rejectValue("password1", "Password.invalid", new String[] { message }, ""));
			}
		}
	}
}