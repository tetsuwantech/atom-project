package com.tetsuwantech.atom.web.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 - 2019 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.tetsuwantech.atom.web.GenericModel;

public class UuidModel extends GenericModel {

	private static final long serialVersionUID = 1L;

	@NotNull
	@Size(min = 1, max = LENGTH)
	private String uuid;

	private boolean enabled = true;

	public UuidModel() {
		this("");
	}

	public UuidModel(String uuid) {
		super();
		this.uuid = uuid;
	}

	/**
	 * @return the UUID
	 */
	public String getUuid() {
		return uuid;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
