
package com.tetsuwantech.atom.web.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.tetsuwantech.atom.database.authentication.Subject;
import com.tetsuwantech.atom.web.GenericModel;

public class PasswordModel extends GenericModel {

	private static final long serialVersionUID = 1L;

	@NotNull
	@Size(min = 1, max = LENGTH)
	private String password1;

	@NotNull
	@Size(min = 1, max = LENGTH)
	private String password2;

	public PasswordModel() {
		this("", "");
	}

	public PasswordModel(String password1, String password2) {
		this.password1 = password1;
		this.password2 = password2;
	}

	public PasswordModel(Subject subject) {
		super(subject);
		this.password1 = "";
		this.password2 = "";
	}

	/**
	 * @return the password1
	 */
	public String getPassword1() {
		return password1;
	}

	/**
	 * @param password1
	 *            the password1 to set
	 */
	public void setPassword1(String password1) {
		this.password1 = password1;
	}

	/**
	 * @return the password2
	 */
	public String getPassword2() {
		return password2;
	}

	/**
	 * @param password2
	 *            the password2 to set
	 */
	public void setPassword2(String password2) {
		this.password2 = password2;
	}
}
