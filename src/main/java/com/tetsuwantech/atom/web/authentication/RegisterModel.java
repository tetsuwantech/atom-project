
package com.tetsuwantech.atom.web.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.security.SecureRandom;
import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;

import com.tetsuwantech.atom.web.GenericModel;

public class RegisterModel extends GenericModel {

	private static final long serialVersionUID = 1L;

	@Valid
	private UsernameModel username = new UsernameModel();

	@Valid
	private PasswordModel password = new PasswordModel();

	@Valid
	private NameModel name = new NameModel();

	@Valid
	private UuidModel uuid = new UuidModel(UUID.randomUUID().toString());

	private Human human;
	private String answer;

	@AssertTrue
	private Boolean termsAndConditions = false;

	private String timeZone = "GMT";

	public enum Human {

		A("1+1 = ?", "2"),
		B("are you human?", "yes"),
		C("what comes next: 1, 2, 3, ?", "4"),
		D("what colour is an orange?", "orange"),
		E("are you a machine?", "no");

		private String question;
		private String answer;

		Human(String question, String answer) {
			this.question = question;
			this.answer = answer;
		}

		/**
		 * @return the test
		 */
		public String getQuestion() {
			return question;
		}

		/**
		 * @return the answer
		 */
		public String getAnswer() {
			return answer;
		}
	}

	public RegisterModel() {
		int i = new SecureRandom().nextInt(RegisterModel.Human.values().length);
		setHuman(Human.values()[i]);
	}

	/**
	 * @return the usernameModel
	 */
	public UsernameModel getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the usernameModel to set
	 */
	public void setUsername(UsernameModel username) {
		this.username = username;
	}

	/**
	 * @return the nameModel
	 */
	public NameModel getName() {
		return name;
	}

	/**
	 * @param name
	 *            the nameModel to set
	 */
	public void setName(NameModel name) {
		this.name = name;
	}

	public UuidModel getUuid() {
		return uuid;
	}

	public void setUuid(UuidModel uuid) {
		this.uuid = uuid;
	}

	/**
	 * @return the passwordModel
	 */
	public PasswordModel getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the passwordModel to set
	 */
	public void setPassword(PasswordModel password) {
		this.password = password;
	}

	/**
	 * @return the human
	 */
	public Human getHuman() {
		return human;
	}

	/**
	 * @param human
	 *            the human to set
	 */
	public void setHuman(Human human) {
		this.human = human;
	}

	/**
	 * @return the answer
	 */
	public String getAnswer() {
		return answer;
	}

	/**
	 * @param answer
	 *            the answer to set
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}

	/**
	 * @return the termsAndConditions
	 */
	public Boolean getTermsAndConditions() {
		return termsAndConditions;
	}

	/**
	 * @param termsAndConditions
	 *            the termsAndConditions to set
	 */
	public void setTermsAndConditions(Boolean termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

}
