
package com.tetsuwantech.atom.web.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.user.OAuth2User;

import com.tetsuwantech.atom.database.authentication.Subject;
import com.tetsuwantech.atom.database.authentication.Subject.Status;
import com.tetsuwantech.atom.web.GenericModel;

public class SubjectModel extends GenericModel implements UserDetails, OAuth2User {

	private static final long serialVersionUID = 1L;

	public static final String SUBJECT = "subject";
	public static final String USERNAME = "username";

	private String username;
	private String password;

	@DateTimeFormat(pattern = "dd/MMM/yyyy h:mm a")
	private Instant joined;

	@DateTimeFormat(pattern = "dd/MMM/yyyy h:mm a")
	private Instant lastLogin;

	private String givenName;
	private String familyName;

	private String timeZone = "GMT";
	private ZoneId timeZoneId = ZoneId.of(timeZone);

	private String uuid;

	@DateTimeFormat(pattern = "dd/MMM/yyyy h:mm a")
	private Instant expires;

	// package protected so tests can see it
	private Status status;

	@DateTimeFormat(pattern = "dd/MMM/yyyy h:mm a")
	private Instant passwordExpires;

	private boolean isSocial = false;
	private boolean isDeactivatable = true;

	// will be gravatar or google image
	private String avatarUrl;

	// the provider of the social connection
	private String clientRegistrationId;

	private List<SimpleGrantedAuthority> authorities = new LinkedList<>();

	public SubjectModel() {
	}

	public SubjectModel(Long id) {
		super(id);
	}

	public SubjectModel(Subject subject) {
		super(subject);

		setUsername(subject.getUsername());
		setJoined(subject.getJoined());
		setGivenName(subject.getGivenName());
		setFamilyName(subject.getFamilyName());
		setLastLogin(subject.getLastLogin());
		setTimeZone(subject.getTimeZone());

		// don't expose these
		this.status = subject.getStatus();

		if (StringUtils.isNotEmpty(subject.getUuid())) {
			setUuid(subject.getUuid());
			this.expires = subject.getUuidExpires();
		}
		setPassword(subject.getPassword());
		this.passwordExpires = subject.getPasswordExpires();

		List<GrantedAuthority> grantedAuthorities = new LinkedList<>(subject.getAuthorities());
		grantedAuthorities.forEach(grantedAuthority -> authorities.add(new SimpleGrantedAuthority(grantedAuthority.getAuthority())));

		// set this to false so gravatar is configured
		setSocial(false);
	}

	/**
	 * @return the lastLogin
	 */
	public Instant getLastLogin() {
		return this.lastLogin;
	}

	/**
	 * @param lastLogin
	 *            the lastLogin to set
	 */
	public void setLastLogin(Instant lastLogin) {
		this.lastLogin = lastLogin;
	}

	/**
	 * @return the givenName
	 */
	public String getGivenName() {
		return this.givenName;
	}

	/**
	 * @param givenName
	 *            the givenName to set
	 */
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	/**
	 * @return the familyName
	 */
	public String getFamilyName() {
		return this.familyName;
	}

	/**
	 * @param familyName
	 *            the familyName to set
	 */
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return this.givenName + " " + this.familyName;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
		this.timeZoneId = ZoneId.of(timeZone);
	}

	public ZoneId getTimeZoneId() {
		return timeZoneId;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String getPassword() {
		return password;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String getUsername() {
		return username;
	}

	/**
	 * @return the joined
	 */
	public Instant getJoined() {
		return this.joined;
	}

	/**
	 * @param joined
	 *            the joined to set
	 */
	public void setJoined(Instant joined) {
		this.joined = joined;
	}

	/**
	 * @return the uuid
	 */
	public String getUuid() {
		return this.uuid;
	}

	/**
	 * @param uuid
	 *            the uuid to set
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Instant getUuidExpired() {
		return expires;
	}

	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Instant getPasswordExpires() {
		return passwordExpires;
	}

	public void setPasswordExpires(Instant passwordExpires) {
		this.passwordExpires = passwordExpires;
	}

	public boolean isSocial() {
		return isSocial;
	}

	public void setSocial(boolean isSocial) {
		this.isSocial = isSocial;

		// use Gravatar
		if (!isSocial) {

			avatarUrl = "https://www.gravatar.com/avatar";
			try {
				byte[] digest = MessageDigest.getInstance("MD5").digest(username.trim().toLowerCase().getBytes());
				String hash = DatatypeConverter.printHexBinary(digest).toLowerCase();
				avatarUrl += "/" + hash;
			} catch (NoSuchAlgorithmException exception) {
				// will default to mystery person
				avatarUrl += "?d=mp&s=32";
			}

			clientRegistrationId = null;
		}
	}

	public boolean isDeactivatable() {
		return this.isDeactivatable;
	}

	public void setDeactivatable(boolean isDeactivatable) {
		this.isDeactivatable = isDeactivatable;
	}

	// there will always be a free plan, so in theory this will never trigger
	@Override
	public boolean isAccountNonExpired() {
		return !status.equals(Status.EXPIRED);
	}

	@Override
	public boolean isAccountNonLocked() {
		return !status.equals(Status.LOCKED);
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return this.passwordExpires == null || this.passwordExpires.isAfter(Instant.now());
	}

	@Override
	public boolean isEnabled() {
		return status.equals(Status.ENABLED);
	}

	public boolean isPending() {
		return status.equals(Status.PENDING);
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public String getClientRegistrationId() {
		return clientRegistrationId;
	}

	public void setClientRegistrationId(String clientRegistrationId) {
		this.clientRegistrationId = clientRegistrationId;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public Map<String, Object> getAttributes() {
		Map<String, Object> attributes = new HashMap<>();
		attributes.put("name", getFullName());
		attributes.put("email", getUsername());
		attributes.put("given_name", getGivenName());
		attributes.put("family_name", getFamilyName());
		attributes.put("picture", getAvatarUrl());
		return attributes;
	}

	@Override
	public String getName() {
		return getFullName();
	}
}
