
package com.tetsuwantech.atom.web.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class RegisterModelValidator implements Validator {

	@SuppressWarnings(value = "unused")
	private static final Logger log = LoggerFactory.getLogger(RegisterModelValidator.class);

	private Validator usernameModelValidator;
	private Validator passwordModelValidator;
	private Validator uuidModelValidator;

	public RegisterModelValidator(UsernameModelValidator usernameModelValidator, PasswordModelValidator passwordModelValidator, UuidModelValidator uuidModelValidator) {
		this.usernameModelValidator = usernameModelValidator;
		this.passwordModelValidator = passwordModelValidator;
		this.uuidModelValidator = uuidModelValidator;
	}

	@Override
	public boolean supports(Class<?> target) {
		return RegisterModel.class.equals(target);
	}

	@Override
	public void validate(Object object, Errors errors) {
		RegisterModel register = (RegisterModel) object;

		// check they are human
		if (!register.getAnswer().equals(register.getHuman().getAnswer()))
			errors.rejectValue("answer", "Subject.answer");

		// we want to check the other fields
		errors.pushNestedPath("username");
		ValidationUtils.invokeValidator(this.usernameModelValidator, register.getUsername(), errors);
		errors.popNestedPath();

		errors.pushNestedPath("password");
		ValidationUtils.invokeValidator(this.passwordModelValidator, register.getPassword(), errors);
		errors.popNestedPath();

		errors.pushNestedPath("uuid");
		ValidationUtils.invokeValidator(this.uuidModelValidator, register.getUuid(), errors);
		errors.popNestedPath();

	}
}
