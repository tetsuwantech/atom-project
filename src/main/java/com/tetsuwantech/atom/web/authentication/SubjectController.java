
package com.tetsuwantech.atom.web.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static java.util.UUID.randomUUID;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizedClientRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tetsuwantech.atom.database.authentication.ClientRegistration;
import com.tetsuwantech.atom.manager.authentication.SubjectManager;
import com.tetsuwantech.atom.util.DateTimePeriodUtils;
import com.tetsuwantech.atom.util.MailSender;
import com.tetsuwantech.atom.util.MailSender.Message;
import com.tetsuwantech.atom.util.authentication.SubjectHandler;
import com.tetsuwantech.atom.util.authentication.SubjectUtils;

@Controller
@RequestMapping(path = "/subject")
public class SubjectController {

	@SuppressWarnings(value = "unused")
	private static final Logger log = LoggerFactory.getLogger(SubjectController.class);

	private static final String MESSAGE = "message";
	private static final String TYPE = "type";
	private static final String PASSWORD = "password";
	private static final String UUID = "uuid";
	private static final String ERRORS = "errors";
	private static final String REGISTER = "register";
	private static final String DANGER = "danger";
	private static final String ARGS = "args";

	@Inject
	private MailSender mailSender;

	@Inject
	private SubjectManager subjectManager;

	@Inject
	private SubjectHandler subjectHandler;

	@Inject
	private DateTimePeriodUtils dateTimePeriodUtils;

	@Inject
	private OAuth2AuthorizedClientRepository authorizedClientRepository;

	private String atomActivate;
	private String atomDeactivate;
	private String atomProfile;

	@Inject
	void setAtomActivate(@Value("${atom.activate:}") String atomActivate) {
		this.atomActivate = atomActivate;
	}

	@Inject
	void setAtomDeactivate(@Value("${atom.deactivate:/subject?status=deactivate.valid&type=danger}") String atomDeactivate) {
		this.atomDeactivate = atomDeactivate;
	}

	@Inject
	void setAtomProfile(@Value("${atom.profile:/profile}") String atomProfile) {
		this.atomProfile = atomProfile;
	}

	// for forgot password, username exists so don't check for it
	@InitBinder(value = SubjectModel.USERNAME)
	private void initForgotPasswordModel(WebDataBinder binder) {
		binder.addValidators(new UsernameModelValidator(subjectManager, false));
	}

	@InitBinder(value = PASSWORD)
	private void initChangePasswordModel(WebDataBinder binder) {
		binder.addValidators(new PasswordModelValidator());
	}

	@InitBinder(value = REGISTER)
	private void initRegisterModel(WebDataBinder binder) {
		binder.addValidators(new RegisterModelValidator(new UsernameModelValidator(subjectManager, true), new PasswordModelValidator(), new UuidModelValidator()));
	}

	@InitBinder(value = UUID)
	private void initUuidModel(WebDataBinder binder) {
		binder.addValidators(new UuidModelValidator(subjectManager, dateTimePeriodUtils));
	}

	@ModelAttribute
	public void populateModel(Model model) {

		// for forgot password
		model.addAttribute(SubjectModel.USERNAME, new UsernameModel());
		model.addAttribute(PASSWORD, new PasswordModel());
		model.addAttribute(REGISTER, new RegisterModel());
	}

	/*
	 * main login view model used by external spring security response - no command object for this model error means user had bad
	 * credentials logout means user actively clicked logout link
	 */
	@GetMapping
	public String main(@RequestParam(required = false) String status, Model model) {

		if (status != null) {
			model.addAttribute(MESSAGE, "subjects." + status);
		}

		return "/subjects/login";
	}

	/*
	 * forgot password view model displays model if not logged in if logged in, then redirect to forgot password page
	 */
	@GetMapping(value = "/forgot")
	public String forgot(Model model) {
		model.addAttribute(MESSAGE, "subjects.forgot.edit");
		return "/subjects/forgot";
	}

	/*
	 * forgot password controller model takes username sets uuid in account send emails does not deactivate account
	 */
	@PostMapping(value = "/forgot")
	public String forgot(HttpServletRequest request, @Valid @ModelAttribute(name = SubjectModel.USERNAME) UsernameModel username,
			BindingResult result, Model model, RedirectAttributes redirectAttributes) throws MalformedURLException {

		// email not found
		if (result.hasErrors()) {
			model.addAttribute(MESSAGE, ERRORS);
			model.addAttribute(TYPE, DANGER);
			return "/subjects/forgot";
		} else {

			SubjectModel subject = subjectManager.findFirst(username.getUsername());

			// set UUID for this attempt - but don't invalidate the account
			subject.setUuid(randomUUID().toString());

			// this will also save subject with new uuid
			subjectManager.save(subject);

			Map<String, Object> content = new TreeMap<>();
			content.put(SubjectModel.SUBJECT, subject);

			String url = new URL(request.getScheme(), request.getServerName(),
					request.getServerPort() == 80 || request.getServerPort() == 443 ? -1 : request.getServerPort(),
					request.getContextPath() + "/subject/password?uuid=" + subject.getUuid()).toExternalForm();
			content.put("url", url);

			mailSender.send(subject, Message.FORGOT_PASSWORD, content);

			redirectAttributes.addFlashAttribute(MESSAGE, "subjects.forgot.send");
			redirectAttributes.addFlashAttribute(ARGS, new String[] { username.getUsername() });
		}

		model.asMap().clear();
		return "redirect:/subject";
	}

	/*
	 * password change view page
	 */
	@GetMapping(value = "/password")
	public String password(@Valid @ModelAttribute(name = UUID) UuidModel uuid, BindingResult result, Model model) {

		if (result.hasErrors()) {
			model.addAttribute(MESSAGE, ERRORS);
			model.addAttribute(TYPE, DANGER);
			return "/subjects/login";
		} else {
			model.addAttribute(MESSAGE, "subjects.password.edit");
			return "/subjects/password";
		}
	}

	/*
	 * change password controller - user can be logged in - but ignore?
	 */
	@PostMapping(value = "/password")
	public String password(HttpServletRequest request, HttpServletResponse response,
			@Valid @ModelAttribute(name = PASSWORD) PasswordModel password, BindingResult passwordResult,
			@Valid @ModelAttribute(name = UUID) UuidModel uuid, BindingResult uuidResult, Model model,
			RedirectAttributes redirectAttributes) {

		if (passwordResult.hasErrors() || uuidResult.hasErrors()) {
			model.addAttribute(MESSAGE, ERRORS);
			model.addAttribute(TYPE, DANGER);
			return "/subjects/password";
		} else {

			// validators will have caught errors before now
			SubjectModel subject = subjectManager.findFirst(uuid.getUuid(), UUID);

			// log user in for this here so it gets audited
			SubjectUtils.login(subject);

			// reset password - if the model had errors (password not same) then we won't be
			// here
			subject.setPassword(password.getPassword1());
			subject.setLastLogin(dateTimePeriodUtils.now());

			subjectManager.save(subject);
			Optional.ofNullable(subjectHandler).ifPresent(handler -> handler.login(subject, request, response));
			redirectAttributes.addFlashAttribute(MESSAGE, "subjects.password.save");
		}

		model.asMap().clear();
		return "redirect:" + atomActivate;
	}

	/*
	 * register user details controller not logged in user generates email to validate/unlock account
	 */
	@PostMapping(value = "/save")
	public String save(HttpServletRequest request, @Valid @ModelAttribute(name = REGISTER) RegisterModel register,
			BindingResult result, Model model, RedirectAttributes redirectAttributes) throws MalformedURLException {

		if (result.hasErrors()) {
			model.addAttribute(MESSAGE, ERRORS);
			model.addAttribute(TYPE, DANGER);
			return "/subjects/login";
		} else {

			// this will set dateJoined
			SubjectModel subject = new SubjectModel();
			subject.setUsername(register.getUsername().getUsername());
			subject.setPassword(register.getPassword().getPassword1());
			subject.setFamilyName(register.getName().getFamilyName());
			subject.setGivenName(register.getName().getGivenName());
			subject.setTimeZone(register.getTimeZone());

			// and generate email
			subject.setUuid(randomUUID().toString());

			// this will also save subject with new uuid
			subjectManager.save(subject);

			Map<String, Object> content = new TreeMap<>();
			content.put(SubjectModel.SUBJECT, subject);

			String url = new URL(request.getScheme(), request.getServerName(),
					request.getServerPort() == 80 || request.getServerPort() == 443 ? -1 : request.getServerPort(),
					request.getContextPath() + "/subject/activate?uuid=" + subject.getUuid()).toExternalForm()
					+ "&enabled=false";
			content.put("url", url);

			mailSender.send(subject, Message.CREATE_ACCOUNT, content);

			model.addAttribute(MESSAGE, "subjects.edit.send");
			model.addAttribute(ARGS, new String[] { register.getUsername().getUsername() });
		}

		return "/subjects/login";
	}

	@GetMapping(value = "/activate")
	public String activate(HttpServletRequest request, HttpServletResponse response,
			@Valid @ModelAttribute(name = UUID) UuidModel uuid, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) {

		if (result.hasErrors()) {
			redirectAttributes.addFlashAttribute(MESSAGE, ERRORS);
			redirectAttributes.addFlashAttribute(TYPE, DANGER);

			model.asMap().clear();
			return "redirect:/subject";
		}

		SubjectModel subject = subjectManager.findFirst(uuid.getUuid(), UUID);

		// activate user, and run Builder
		subjectManager.activate(subject);

		// redraw from database as settings have changed
		subject = subjectManager.get(subject.getId());

		// log in here because audit doesn't work in activate and we need the fully
		// loaded subject
		SubjectUtils.login(subject);

		// this will make any application specific calls - expects user to be logged in
		subjectHandler.build(subject, null);
		subjectHandler.login(subject, request, response);

		Map<String, Object> content = new TreeMap<>();
		content.put(SubjectModel.SUBJECT, subject);
		mailSender.send(subject, Message.WELCOME, content);

		model.asMap().clear();
		return "redirect:" + atomActivate;
	}

	@GetMapping(value = "/deactivate")
	public String deactivate(HttpServletRequest request, @Valid @ModelAttribute(name = UUID) UuidModel uuid,
			BindingResult result, Model model, RedirectAttributes redirectAttributes) throws ServletException {

		// need to test for deactivatable as well
		if (result.hasErrors()) {
			redirectAttributes.addFlashAttribute(MESSAGE, ERRORS);
			redirectAttributes.addFlashAttribute(TYPE, DANGER);

			model.asMap().clear();
			return "redirect:/subject";
		}

		SubjectModel subject = subjectManager.findFirst(uuid.getUuid(), UUID);
		Optional.ofNullable(subjectHandler).ifPresent(handler -> handler.deactivate(subject));

		subjectManager.deactivate(subject);
		SubjectUtils.logout(subject);
		request.logout();

		model.asMap().clear();
		return "redirect:" + atomDeactivate;

	}

	@GetMapping(value = "/disconnect")
	public String disconnect(@RequestParam(name = "clientRegistration", defaultValue = "google", required = false) ClientRegistration clientRegistration, HttpServletRequest request, HttpServletResponse response, Model model) {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		authorizedClientRepository.removeAuthorizedClient(clientRegistration.name(), authentication, request, response);

		SubjectModel subject = SubjectUtils.getSubject();
		subject.setSocial(false);

		// force reload of subject object
		SubjectUtils.login(subject);

		return "forward:" + atomProfile + "?source=oauth2.disconnect";
	}
}
