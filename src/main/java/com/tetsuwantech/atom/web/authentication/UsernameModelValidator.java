package com.tetsuwantech.atom.web.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.tetsuwantech.atom.manager.authentication.SubjectManager;

public class UsernameModelValidator implements Validator {

	@SuppressWarnings(value = "unused")
	private static final Logger log = LoggerFactory.getLogger(UsernameModelValidator.class);

	private SubjectManager subjectManager;
	private boolean isAvailable;

	public UsernameModelValidator() {
	}

	public UsernameModelValidator(SubjectManager subjectManager, boolean isAvailable) {
		this.subjectManager = subjectManager;
		this.isAvailable = isAvailable;
	}

	@Override
	public boolean supports(Class<?> target) {
		return UsernameModel.class.equals(target);
	}

	@Override
	public void validate(Object object, Errors errors) {

		UsernameModel username = (UsernameModel) object;

		// check not an invalid address
		if (isAvailable) {

			// check supplied username is available
			SubjectModel subject = subjectManager.findFirst(username.getUsername());
			if (subject != null) {
				errors.rejectValue(SubjectModel.USERNAME, "Subject.UsernameUsed");
			}

			// check it isn't banned
			if (!subjectManager.isDomainAllowed(username.getUsername())) {
				errors.rejectValue(SubjectModel.USERNAME, "Username.invalidDomain");
			}
		} else {

			// check it exists
			SubjectModel subject = subjectManager.findFirst(username.getUsername());

			// if still no match, then we return with error
			if (subject == null) {
				errors.rejectValue(SubjectModel.USERNAME, "Forgot.notFound");
			} else if (!subject.isEnabled()) {
				errors.rejectValue(SubjectModel.USERNAME, "subjects.login.locked");
			} else if (StringUtils.isEmpty(subject.getPassword())) {
				errors.rejectValue(SubjectModel.USERNAME, "oauth2.login.password");
			}
		}
	}
}
