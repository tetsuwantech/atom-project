
package com.tetsuwantech.atom.web;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.tetsuwantech.atom.database.GenericEntity;

public abstract class DeleteModel extends GenericModel implements Deletable {

	private static final long serialVersionUID = 1L;

	private Boolean delete = false;

	// this is taking over from GenericEntity
	private Boolean deletable = true;

	public DeleteModel() {
	}

	public DeleteModel(GenericEntity genericEntity) {
		super(genericEntity);
	}

	public DeleteModel(Long id) {
		super(id);
	}

	/**
	 * @return the delete
	 */
	@Override
	public Boolean getDelete() {
		return delete;
	}

	/**
	 * @param delete
	 *            the delete to set
	 */
	@Override
	public void setDelete(Boolean delete) {
		this.delete = delete;
	}

	@Override
	public Boolean getDeletable() {
		return this.deletable;
	}

	@Override
	public void setDeletable(Boolean deletable) {
		this.deletable = deletable;
	}

}
