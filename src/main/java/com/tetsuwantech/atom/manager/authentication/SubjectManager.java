
package com.tetsuwantech.atom.manager.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 - 2019 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.client.http.OAuth2ErrorResponseErrorHandler;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequestEntityConverter;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserService;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.OAuth2AuthorizationException;
import org.springframework.security.oauth2.core.OAuth2Error;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

import com.tetsuwantech.atom.database.authentication.Authority;
import com.tetsuwantech.atom.database.authentication.Authority.Type;
import com.tetsuwantech.atom.database.authentication.Subject;
import com.tetsuwantech.atom.database.authentication.Subject.Status;
import com.tetsuwantech.atom.database.authentication.SubjectService;
import com.tetsuwantech.atom.manager.GenericManager;
import com.tetsuwantech.atom.util.DateTimePeriodUtils;
import com.tetsuwantech.atom.util.MailSender;
import com.tetsuwantech.atom.util.MailSender.Message;
import com.tetsuwantech.atom.util.authentication.SubjectHandler;
import com.tetsuwantech.atom.util.authentication.SubjectUtils;
import com.tetsuwantech.atom.web.authentication.SubjectModel;

// not needed as it is defined in spring-context.xml
public class SubjectManager extends GenericManager<Subject, String, SubjectModel, SubjectModel> implements UserDetailsService, OAuth2UserService<OAuth2UserRequest, OAuth2User> {

	private static final String DELETED = "__deleted__";

	@Inject
	private SubjectService subjectService;

	@Inject
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Inject
	private DateTimePeriodUtils dateTimePeriodUtils;

	private static final ParameterizedTypeReference<Map<String, Object>> PARAMETERIZED_RESPONSE_TYPE = new ParameterizedTypeReference<>() {
	};

	private Converter<OAuth2UserRequest, RequestEntity<?>> requestEntityConverter = new OAuth2UserRequestEntityConverter();

	private RestOperations restOperations;

	public SubjectManager() {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new OAuth2ErrorResponseErrorHandler());
		restOperations = restTemplate;
	}

	@Override
	public void save(SubjectModel subjectModel) {

		Subject subject;
		boolean isNew = false;
		if (subjectModel.getId() == null) {

			// sets joinedDate to today
			// default status is PENDING
			subject = new Subject();
			subject.setJoined(dateTimePeriodUtils.now());

			isNew = true;

		} else {
			subject = subjectService.get(subjectModel.getId());
		}

		subject.setUsername(subjectModel.getUsername());
		subject.setGivenName(subjectModel.getGivenName());
		subject.setFamilyName(subjectModel.getFamilyName());
		subject.setLastLogin(subjectModel.getLastLogin());
		subject.setTimeZone(subjectModel.getTimeZone());

		// if the model has a uuid and it's different - then save it
		if (!StringUtils.equals(subjectModel.getUuid(), subject.getUuid())) {

			subject.setUuid(subjectModel.getUuid());
			subject.setUuidExpires(ChronoUnit.DAYS.addTo(dateTimePeriodUtils.now(), 1L));

		} else {
			// blank it out
			subject.setUuid(null);
			subject.setUuidExpires(null);
		}

		// new password might be null if not provided
		// current password will be null if new Subject
		// password will be same (encrypted) if no change
		// if password expired, then change regardless
		boolean newPasswordProvided = StringUtils.isNotEmpty(subjectModel.getPassword()) && !subjectModel.getPassword().equals(subject.getPassword());

		Instant passwordExpiryDate = subject.getPasswordExpires();
		boolean passwordHasExpired = passwordExpiryDate != null && passwordExpiryDate.isBefore(dateTimePeriodUtils.now());

		boolean passwordSame = subject.getPassword() != null && bCryptPasswordEncoder.matches(subjectModel.getPassword(), subject.getPassword());

		if (newPasswordProvided && (passwordHasExpired || !passwordSame)) {
			String password = bCryptPasswordEncoder.encode(subjectModel.getPassword());
			subject.setPassword(password);

			// add one day to now
			subject.setPasswordExpires(ChronoUnit.DAYS.addTo(dateTimePeriodUtils.now(), 365L));

			// as we have reset the password, and it's not new then we reset uuid and
			// expires
			if (!isNew) {
				subject.setUuid(null);
				subject.setUuidExpires(null);

				// and push the null back
				subjectModel.setUuid(null);
			}

			// save encrypted for return
			subjectModel.setPassword(password);
		}

		if (isNew) {
			subjectService.create(subject);
			subjectModel.setId(subject.getId());
		} else {
			subjectService.save(subject);
		}

	}

	public void activate(SubjectModel subjectModel) {

		List<Authority> authorities = new LinkedList<>();
		Subject subject = subjectService.get(subjectModel.getId());

		Authority authority = new Authority();
		authority.setAuthority(Type.ROLE_USER);
		authority.setSubject(subject);
		authorities.add(authority);

		// basic user only
		subject.setAuthorities(authorities);

		subject.setStatus(Status.ENABLED);
		subject.setUuid(null);
		subject.setUuidExpires(null);

		// need to grab subject as we use it in subjectBuilder.build()
		subjectService.save(subject);
	}

	public void deactivate(SubjectModel subjectModel) {

		Subject subject = subjectService.get(subjectModel.getId());

		// anonymise all the data, delete everything we can
		subject.setGivenName(DELETED);
		subject.setFamilyName(DELETED);
		subject.setUsername(DELETED + subject.getUuid() + "@tetsuwan-tech.com");
		subject.setPassword(DELETED);
		subject.setPasswordExpires(null);
		subject.setUuid(null);
		subject.setUuidExpires(null);
		subject.setStatus(Status.LOCKED);

		subject.getAuthorities().clear();

		subjectService.save(subject);
	}

	@Override
	protected SubjectModel createModel(Subject subject) {
		return new SubjectModel(subject);
	}

	public List<SubjectModel> findAll() {
		return super.findAll(null, null, "id");
	}

	@Override
	public List<SubjectModel> findAll(SubjectModel where) {
		throw new UnsupportedOperationException("Not supported.");
	}

	@Override
	public Long countAll(SubjectModel subjectModel) {
		return super.count(null, null);
	}

	public SubjectModel findFirst(String where) {
		return findFirst(where, SubjectModel.USERNAME);
	}

	@Override
	public SubjectModel findFirst(String where, String column) {
		return super.findFirst(where, column);
	}

	@Override
	public List<SubjectModel> search(SubjectModel subjectModel, String text) {
		return super.search(subjectModel.getUsername(), SubjectModel.USERNAME, text, new String[] { "username", "givenName", "familyName" });
	}

	@Override
	public UserDetails loadUserByUsername(String username) {

		SubjectModel subject = findFirst(username, SubjectModel.USERNAME);

		if (subject == null) {
			throw new UsernameNotFoundException(username);
		}

		return subject;
	}

	private static final String MISSING_USER_INFO_URI_ERROR_CODE = "missing_user_info_uri";
	private static final String MISSING_USER_NAME_ATTRIBUTE_ERROR_CODE = "missing_user_name_attribute";
	private static final String INVALID_USER_INFO_RESPONSE_ERROR_CODE = "invalid_user_info_response";
	private static final String INVALID_DOMAIN_NAME = "invalid_domain_name";

	@Inject
	private SubjectHandler subjectHandler;

	@Inject
	private MailSender mailSender;

	@Override
	public OAuth2User loadUser(OAuth2UserRequest userRequest) {

		if (StringUtils.isBlank(userRequest.getClientRegistration().getProviderDetails().getUserInfoEndpoint().getUri())) {
			OAuth2Error oauth2Error = new OAuth2Error(
					MISSING_USER_INFO_URI_ERROR_CODE,
					"Missing required UserInfo Uri in UserInfoEndpoint for Client Registration: " + userRequest.getClientRegistration().getRegistrationId(),
					null);
			throw new OAuth2AuthenticationException(oauth2Error, oauth2Error.toString());
		}
		String userNameAttributeName = userRequest.getClientRegistration().getProviderDetails().getUserInfoEndpoint().getUserNameAttributeName();

		if (StringUtils.isBlank(userNameAttributeName)) {
			OAuth2Error oauth2Error = new OAuth2Error(
					MISSING_USER_NAME_ATTRIBUTE_ERROR_CODE,
					"Missing required \"user name\" attribute name in UserInfoEndpoint for Client Registration: " + userRequest.getClientRegistration().getRegistrationId(),
					null);
			throw new OAuth2AuthenticationException(oauth2Error, oauth2Error.toString());
		}

		RequestEntity<?> request = requestEntityConverter.convert(userRequest);
		ResponseEntity<Map<String, Object>> response;
		try {
			response = restOperations.exchange(request, PARAMETERIZED_RESPONSE_TYPE);
		} catch (OAuth2AuthorizationException exception) {
			OAuth2Error oauth2Error = exception.getError();
			StringBuilder errorDetails = new StringBuilder();
			errorDetails.append("Error details: [");
			errorDetails.append("UserInfo Uri: ").append(userRequest.getClientRegistration().getProviderDetails().getUserInfoEndpoint().getUri());
			errorDetails.append(", Error Code: ").append(oauth2Error.getErrorCode());
			if (oauth2Error.getDescription() != null) {
				errorDetails.append(", Error Description: ").append(oauth2Error.getDescription());
			}
			errorDetails.append("]");
			oauth2Error = new OAuth2Error(INVALID_USER_INFO_RESPONSE_ERROR_CODE, "An error occurred while attempting to retrieve the UserInfo Resource: " + errorDetails.toString(), null);
			throw new OAuth2AuthenticationException(oauth2Error, oauth2Error.toString(), exception);
		} catch (RestClientException exception) {
			OAuth2Error oauth2Error = new OAuth2Error(INVALID_USER_INFO_RESPONSE_ERROR_CODE, "An error occurred while attempting to retrieve the UserInfo Resource: " + exception.getMessage(), null);
			throw new OAuth2AuthenticationException(oauth2Error, oauth2Error.toString(), exception);
		}

		Map<String, Object> attributes = response.getBody();
		String username = (String) attributes.get("email");
		if (!isDomainAllowed(username)) {
			OAuth2Error oAuth2Error = new OAuth2Error(INVALID_DOMAIN_NAME, "Domain name not allowed: " + username, null);
			throw new OAuth2AuthenticationException(oAuth2Error, oAuth2Error.toString(), null);
		}

		SubjectModel subject = findFirst(username, SubjectModel.USERNAME);

		// subject is new, never seen before
		if (subject == null) {
			subject = new SubjectModel();
			subject.setUsername(username);

			subject.setGivenName((String) attributes.get("given_name"));
			subject.setFamilyName((String) attributes.get("family_name"));

			save(subject);

			// this will auto-magically update them to full status - password will be blank
			activate(subject);

			// redraw from database as settings have changed
			subject = get(subject.getId());

			// this will make any application specific calls - user needs to be logged in?
			if (subjectHandler != null) {
				// to make this work, we log in
				SubjectUtils.doAuthenticatedAction(subject, s -> subjectHandler.build(s, Collections.emptyMap()));
			}

			Map<String, Object> content = new HashMap<>();
			content.put(SubjectModel.SUBJECT, subject);
			mailSender.send(subject, Message.WELCOME, content);
		} else {

			// these are set every time
			subject.setGivenName((String) attributes.get("given_name"));
			subject.setFamilyName((String) attributes.get("family_name"));

			SubjectUtils.doAuthenticatedAction(subject, this::save);
		}

		// set these here to save in the session
		subject.setSocial(true);
		subject.setClientRegistrationId(userRequest.getClientRegistration().getRegistrationId());
		subject.setAvatarUrl((String) attributes.get("picture"));

		return subject;
	}

	private String[] invalidDomains = {};
	private String[] validDomains = {};

	@Inject
	void setValidDomains(@Value("${atom.validDomains:}") String domains) {
		validDomains = StringUtils.isEmpty(domains) ? new String[] {} : domains.split(",");
	}

	@Inject
	void setInvalidDomains(@Value("${atom.invalidDomains:hotmail.com,yahoo.com,outlook.com,example.com,example.net,example.org,localhost.com}") String domains) {
		invalidDomains = StringUtils.isEmpty(domains) ? new String[] {} : domains.split(",");
	}

	public boolean isDomainAllowed(String username) {

		// check valid
		if (validDomains.length > 0) {
			for (String domain : validDomains) {
				if (username.endsWith(domain)) {
					return true;
				}
			}
			// no valid domains found
			return false;
		}

		// check it isn't banned
		if (invalidDomains.length > 0) {
			for (String domain : invalidDomains) {
				if (username.endsWith(domain)) {
					return false;
				}
			}
		}

		return true;
	}

}
