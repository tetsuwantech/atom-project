
package com.tetsuwantech.atom.manager;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.inject.Named;

import com.tetsuwantech.atom.database.GenericEntity;
import com.tetsuwantech.atom.database.GenericService;
import com.tetsuwantech.atom.web.Deletable;
import com.tetsuwantech.atom.web.GenericModel;
import com.tetsuwantech.atom.web.authentication.SubjectModel;

@Named(value = "GenericManager")
public abstract class GenericManager<E extends GenericEntity, K extends Object, M extends GenericModel, W extends GenericModel> {

	@Inject
	protected GenericService<E, K> genericService;

	protected abstract M createModel(E entity);

	private M createModelWithDeletable(E entity) {

		M model = createModel(entity);

		if (entity instanceof com.tetsuwantech.atom.database.Deletable && model instanceof Deletable) {
			((Deletable) model).setDeletable(((com.tetsuwantech.atom.database.Deletable) entity).isDeletable());
		}

		return model;
	}

	public M get(Long id) {
		E entity = genericService.get(id);
		return createModelWithDeletable(entity);
	}

	public abstract void save(M model);

	public void remove(Long id) {
		genericService.remove(id);
	}

	public abstract Long countAll(SubjectModel subject);

	public abstract List<M> search(W where, String text);

	public abstract List<M> findAll(W where);

	/*
	 * helper functions
	 */

	protected M findFirst(K where, String column) {
		E entity = genericService.findFirst(where, column);
		return Optional.ofNullable(entity).map(this::createModelWithDeletable).orElse(null);
	}

	protected List<M> findAll(K where, String column, String orderBy) {

		List<M> models = new LinkedList<>();

		// this will have a namespace clash of there where column is in multiple Service
		genericService.findAll(where, column, orderBy).forEach(entity -> models.add(createModelWithDeletable(entity)));
		return models;
	}

	protected Long count(K where, String column) {
		return genericService.count(where, column);
	}

	protected List<M> search(K where, String column, String text, String[] fields) {

		List<M> models = new LinkedList<>();
		genericService.search(where, column, text, fields).forEach(entity -> models.add(createModelWithDeletable(entity)));
		return models;
	}
}
