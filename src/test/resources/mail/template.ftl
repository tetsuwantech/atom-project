<#ftl encoding="UTF-8">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<#--
 #%L
 Atom Project
 %%
 Copyright (C) 2018 Tetsuwan Technology
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Lesser Public License for more details.
 
 You should have received a copy of the GNU General Lesser Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/lgpl-3.0.html>.
 #L%
-->

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html;UTF-8" />
  </head>
  <body>
	<p><#if subject.givenName??>givenName = ${subject.givenName}</#if></p>
	<p><#if subject.familyName??>familyName = ${subject.familyName}</#if></p>
	<#include path>
  </body>
</html>
