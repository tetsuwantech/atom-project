---
-- #%L
-- Atom Project
-- %%
-- Copyright (C) 2018 Tetsuwan Technology
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Lesser Public License for more details.
-- 
-- You should have received a copy of the GNU General Lesser Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/lgpl-3.0.html>.
-- #L%
---

--
-- import statements for testing, generally not used as we need to reset the data for each test
--
INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, uuid, uuid_expires, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$aCgAuLJDd870JC37bxA/xOHSpohvX3iQ/gLIwCg8yEwdwATrX5hta', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', null, null, 'ENABLED');
INSERT INTO subject_authority (authority, subject_id) VALUES ('ROLE_ADMIN', (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com'));
