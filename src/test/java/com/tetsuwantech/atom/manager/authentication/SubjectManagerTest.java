package com.tetsuwantech.atom.manager.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.atMostOnce;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.OAuth2AccessToken.TokenType;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.OAuth2AuthorizationException;
import org.springframework.security.oauth2.core.OAuth2Error;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;

import com.tetsuwantech.atom.database.authentication.Authority;
import com.tetsuwantech.atom.database.authentication.Authority.Type;
import com.tetsuwantech.atom.database.authentication.Subject;
import com.tetsuwantech.atom.database.authentication.Subject.Status;
import com.tetsuwantech.atom.database.authentication.SubjectService;
import com.tetsuwantech.atom.util.DateTimePeriodUtils;
import com.tetsuwantech.atom.util.MailSender;
import com.tetsuwantech.atom.util.authentication.SubjectHandler;
import com.tetsuwantech.atom.web.authentication.SubjectModel;

@ExtendWith(MockitoExtension.class)
class SubjectManagerTest {

	// https://github.com/mockito/mockito/issues/1066
	@Mock(name = "genericService")
	private SubjectService subjectService;

	@Mock
	private DateTimePeriodUtils dateTimePeriodUtils;

	@Spy
	private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

	@Mock
	private RestOperations restOperations;

	@Mock
	private MailSender mailSender;

	@Mock
	private SubjectHandler subjectHander;

	@InjectMocks
	private SubjectManager subjectManagerImpl;

	private static final Instant now = Instant.now();

	@BeforeEach
	void setUp() {
		subjectManagerImpl.setInvalidDomains(null);
		subjectManagerImpl.setValidDomains(null);
	}

	@Test
	void testSaveNullId() {

		when(dateTimePeriodUtils.now()).thenReturn(now);

		ArgumentCaptor<Subject> argumentCapture = ArgumentCaptor.forClass(Subject.class);
		doAnswer(invocation -> {
			Subject subject = invocation.getArgument(0);

			assertNull(subject.getId());

			subject.setId(1L);
			return null;
		}).when(subjectService).create(argumentCapture.capture());

		SubjectModel subject = new SubjectModel();
		subject.setUsername("example@tetsuwan-tech.com");
		subject.setGivenName("givenName");
		subject.setFamilyName("familyName");
		subject.setTimeZone(TimeZone.getDefault().getID());
		subject.setUuid("35cc3700-b238-4500-9c16-8efa9157ae4f");
		subject.setPassword("Example235!");

		subjectManagerImpl.save(subject);

		verify(subjectService, atMostOnce()).create(argumentCapture.getValue());
		assertEquals(1L, argumentCapture.getValue().getId());
		assertEquals(Status.PENDING, argumentCapture.getValue().getStatus());
		assertEquals("example@tetsuwan-tech.com", argumentCapture.getValue().getUsername());
		assertTrue(argumentCapture.getValue().getJoined().isBefore(dateTimePeriodUtils.now().plusSeconds(1L)));
		assertNull(argumentCapture.getValue().getLastLogin());
		assertTrue(bCryptPasswordEncoder.matches("Example235!", argumentCapture.getValue().getPassword()));
		assertTrue(argumentCapture.getValue().getPasswordExpires().isAfter(dateTimePeriodUtils.now()));

		assertEquals("35cc3700-b238-4500-9c16-8efa9157ae4f", argumentCapture.getValue().getUuid());
		assertTrue(argumentCapture.getValue().getUuidExpires().isAfter(dateTimePeriodUtils.now()));

		assertEquals("givenName", argumentCapture.getValue().getGivenName());
		assertEquals("familyName", argumentCapture.getValue().getFamilyName());
		assertEquals(TimeZone.getDefault().getID(), argumentCapture.getValue().getTimeZone());
	}

	@Test
	void testSaveNotNullId() {

		when(dateTimePeriodUtils.now()).thenReturn(now);
		when(subjectService.get(1L)).then(invocation -> {
			Subject subject = new Subject();
			subject.setJoined(dateTimePeriodUtils.now().minusSeconds(2L));
			subject.setUsername("example@tetsuwan-tech.com");
			subject.setStatus(Status.ENABLED);
			// subject.setId(invocation.getArgument(0));
			return subject;
		});

		ArgumentCaptor<Subject> argumentCapture = ArgumentCaptor.forClass(Subject.class);
		when(subjectService.save(argumentCapture.capture())).then(invocation -> (Subject) invocation.getArgument(0));

		SubjectModel subject = new SubjectModel(1L);
		subject.setUsername("example@tetsuwan-tech.com");
		subject.setGivenName("givenName");
		subject.setFamilyName("familyName");
		subject.setLastLogin(dateTimePeriodUtils.now().minusSeconds(1L));
		subject.setTimeZone(TimeZone.getDefault().getID());
		subject.setUuid("35cc3700-b238-4500-9c16-8efa9157ae4f");
		subject.setPassword("Example235!");

		subjectManagerImpl.save(subject);

		verify(subjectService, atMostOnce()).get(1L);
		verify(subjectService, atMostOnce()).save(argumentCapture.getValue());

		// assertEquals(Long.valueOf(1L), argumentCapture.getValue().getId());
		assertEquals(Status.ENABLED, argumentCapture.getValue().getStatus());
		assertEquals("example@tetsuwan-tech.com", argumentCapture.getValue().getUsername());
		assertTrue(argumentCapture.getValue().getJoined().isBefore(dateTimePeriodUtils.now()));
		assertTrue(argumentCapture.getValue().getLastLogin().isBefore(dateTimePeriodUtils.now()));
		assertTrue(bCryptPasswordEncoder.matches("Example235!", argumentCapture.getValue().getPassword()));
		assertTrue(argumentCapture.getValue().getPasswordExpires().isAfter(dateTimePeriodUtils.now()));

		// Blank because we're setting a new password
		assertNull(argumentCapture.getValue().getUuid());
		assertNull(argumentCapture.getValue().getUuidExpires());

		assertEquals("givenName", argumentCapture.getValue().getGivenName());
		assertEquals("familyName", argumentCapture.getValue().getFamilyName());
		assertEquals(TimeZone.getDefault().getID(), argumentCapture.getValue().getTimeZone());
	}

	@Test
	void testSaveExpiredPassword() {

		when(dateTimePeriodUtils.now()).thenReturn(now);
		when(subjectService.get(1L)).then(invocation -> {
			Subject subject = new Subject();
			subject.setJoined(dateTimePeriodUtils.now().minusSeconds(2L));
			subject.setUsername("example@tetsuwan-tech.com");
			subject.setStatus(Status.ENABLED);
			subject.setPasswordExpires(dateTimePeriodUtils.now().minusSeconds(1L));
			subject.setPassword("$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO");
			// subject.setId(invocation.getArgument(0));
			return subject;
		});

		ArgumentCaptor<Subject> argumentCapture = ArgumentCaptor.forClass(Subject.class);
		when(subjectService.save(argumentCapture.capture())).then(invocation -> (Subject) invocation.getArgument(0));

		SubjectModel subject = new SubjectModel(1L);
		subject.setUsername("example@tetsuwan-tech.com");
		subject.setGivenName("givenName");
		subject.setFamilyName("familyName");
		subject.setLastLogin(dateTimePeriodUtils.now().minusSeconds(1L));
		subject.setTimeZone(TimeZone.getDefault().getID());
		subject.setUuid("35cc3700-b238-4500-9c16-8efa9157ae4f");
		subject.setPassword("Example235!");

		subjectManagerImpl.save(subject);

		verify(subjectService, atMostOnce()).get(1L);
		verify(subjectService, atMostOnce()).save(argumentCapture.getValue());

		// assertEquals(Long.valueOf(1L), argumentCapture.getValue().getId());
		assertEquals(Status.ENABLED, argumentCapture.getValue().getStatus());
		assertEquals("example@tetsuwan-tech.com", argumentCapture.getValue().getUsername());
		assertTrue(argumentCapture.getValue().getJoined().isBefore(dateTimePeriodUtils.now()));
		assertTrue(argumentCapture.getValue().getLastLogin().isBefore(dateTimePeriodUtils.now()));
		assertTrue(bCryptPasswordEncoder.matches("Example235!", argumentCapture.getValue().getPassword()));
		assertTrue(argumentCapture.getValue().getPasswordExpires().isAfter(dateTimePeriodUtils.now()));

		// Blank because we're setting a new password
		assertNull(argumentCapture.getValue().getUuid());
		assertNull(argumentCapture.getValue().getUuidExpires());

		assertEquals("givenName", argumentCapture.getValue().getGivenName());
		assertEquals("familyName", argumentCapture.getValue().getFamilyName());
		assertEquals(TimeZone.getDefault().getID(), argumentCapture.getValue().getTimeZone());
	}

	@Test
	void testActivate() {

		when(dateTimePeriodUtils.now()).thenReturn(now);
		when(subjectService.get(1L)).then(invocation -> {
			Subject subject = new Subject();
			subject.setUsername("example@tetsuwan-tech.com");
			subject.setUuid("35cc3700-b238-4500-9c16-8efa9157ae4f");
			subject.setUuidExpires(ChronoUnit.DAYS.addTo(dateTimePeriodUtils.now(), 1L));
			// subject.setId((Long) invocation.getArgument(0));
			return subject;
		});

		ArgumentCaptor<Subject> argumentCapture = ArgumentCaptor.forClass(Subject.class);
		when(subjectService.save(argumentCapture.capture())).then(invocation -> (Subject) invocation.getArgument(0));

		SubjectModel subject = new SubjectModel(1L);

		subjectManagerImpl.activate(subject);

		verify(subjectService, atMostOnce()).get(1L);
		verify(subjectService, atMostOnce()).save(argumentCapture.getValue());

		assertEquals(Status.ENABLED, argumentCapture.getValue().getStatus());
		assertNull(argumentCapture.getValue().getUuid());
		assertNull(argumentCapture.getValue().getUuidExpires());
		assertFalse(argumentCapture.getValue().getAuthorities().isEmpty());
	}

	@Test
	void testDeactivate() {

		when(dateTimePeriodUtils.now()).thenReturn(now);
		when(subjectService.get(1L)).then(invocation -> {
			Subject subject = new Subject();
			subject.setUsername("example@tetsuwan-tech.com");
			subject.setGivenName("givenName");
			subject.setFamilyName("familyName");
			subject.setPassword(bCryptPasswordEncoder.encode("Example235!"));
			subject.setPasswordExpires(ChronoUnit.DAYS.addTo(dateTimePeriodUtils.now(), 365L));
			subject.setUuid("35cc3700-b238-4500-9c16-8efa9157ae4f");
			subject.setUuidExpires(ChronoUnit.DAYS.addTo(dateTimePeriodUtils.now(), 1L));
			subject.setStatus(Status.ENABLED);
			// subject.setId((Long) invocation.getArgument(0));

			List<Authority> authorities = new LinkedList<>();
			Authority authority = new Authority();
			authority.setAuthority(Type.ROLE_USER);
			authority.setSubject(subject);
			authorities.add(authority);

			// basic user only
			subject.setAuthorities(authorities);

			return subject;
		});

		ArgumentCaptor<Subject> argumentCapture = ArgumentCaptor.forClass(Subject.class);
		when(subjectService.save(argumentCapture.capture())).then(invocation -> (Subject) invocation.getArgument(0));

		SubjectModel subject = new SubjectModel(1L);

		subjectManagerImpl.deactivate(subject);

		verify(subjectService, atMostOnce()).get(1L);
		verify(subjectService, atMostOnce()).save(argumentCapture.getValue());

		assertEquals(Status.LOCKED, argumentCapture.getValue().getStatus());
		assertEquals("__deleted__35cc3700-b238-4500-9c16-8efa9157ae4f@tetsuwan-tech.com", argumentCapture.getValue().getUsername());
		assertEquals("__deleted__", argumentCapture.getValue().getGivenName());
		assertEquals("__deleted__", argumentCapture.getValue().getFamilyName());
		assertNull(argumentCapture.getValue().getUuid());
		assertNull(argumentCapture.getValue().getUuidExpires());
		assertTrue(argumentCapture.getValue().getAuthorities().isEmpty());
	}

	@Test
	void testFindAll() {

		when(subjectService.findAll(isNull(), isNull(), eq("id"))).then(invocation -> {
			Subject subject = new Subject("example@tetsuwan-tech.com");
			// subject.setId(1L);
			return Arrays.asList(subject);
		});

		List<SubjectModel> subjects = subjectManagerImpl.findAll();

		verify(subjectService, atMostOnce()).findAll(null, null, "id");
		assertEquals(1, subjects.size());
		// assertEquals(Long.valueOf(1L), subjects.get(0).getId());
		assertEquals("example@tetsuwan-tech.com", subjects.get(0).getUsername());
	}

	@Test
	void testCountAllSubjectModel() {

		when(subjectService.count(isNull(), isNull())).then(invocation -> Long.valueOf(1L));

		Long count = subjectManagerImpl.countAll(new SubjectModel(1L));

		verify(subjectService, atMostOnce()).count(null, null);
		assertEquals(1L, count);
	}

	@Test
	void testFindFirstStringString() {

		when(subjectService.findFirst(eq("example@tetsuwan-tech.com"), eq("username"))).then(invocation -> {
			Subject subject = new Subject((String) invocation.getArgument(0));
			// subject.setId(1L);
			return subject;
		});

		SubjectModel subject = subjectManagerImpl.findFirst("example@tetsuwan-tech.com", "username");

		verify(subjectService, atMostOnce()).findFirst("example@tetsuwan-tech.com", "username");
		// assertEquals(Long.valueOf(1L), subject.getId());
		assertEquals("example@tetsuwan-tech.com", subject.getUsername());
	}

	@Test
	void testSearch() {

		ArgumentCaptor<String> argumentCaptureWhere = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<String> argumentCaptureColumn = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<String> argumentCaptureOrderBy = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<String[]> argumentCaptureFields = ArgumentCaptor.forClass(String[].class);
		doAnswer(invocation -> {

			Subject subject = new Subject(invocation.getArgument(0));
			subject.setId(1L);
			return List.of(subject);

		}).when(subjectService).search(argumentCaptureWhere.capture(), argumentCaptureColumn.capture(), argumentCaptureOrderBy.capture(), argumentCaptureFields.capture());

		SubjectModel subject = new SubjectModel(1L);
		subject.setUsername("example@tetuwan-tech.com");

		List<SubjectModel> results = subjectManagerImpl.search(subject, "example@tetsuwan-tech.com");

		verify(subjectService, atMostOnce()).findAll(argumentCaptureWhere.getValue(), argumentCaptureColumn.getValue(), argumentCaptureOrderBy.getValue());
		assertEquals(1, results.size());
		assertEquals(1L, results.get(0).getId());

	}

	@Test
	void testLoadUserByUsernameString_exists() {

		when(subjectService.findFirst(eq("example@tetsuwan-tech.com"), eq("username"))).then(invocation -> {
			Subject subject = new Subject((String) invocation.getArgument(0));
			// subject.setId(1L);
			return subject;
		});

		UserDetails userDetails = subjectManagerImpl.loadUserByUsername("example@tetsuwan-tech.com");

		verify(subjectService, atMostOnce()).findFirst("example@tetsuwan-tech.com", "username");
		assertEquals("example@tetsuwan-tech.com", userDetails.getUsername());
	}

	@Test
	void testLoadUserByUsernameString_doesNotExist() {

		when(subjectService.findFirst(eq("example@tetsuwan-tech.com"), eq("username"))).thenReturn(null);

		Exception result = assertThrows(UsernameNotFoundException.class, () -> {
			subjectManagerImpl.loadUserByUsername("example@tetsuwan-tech.com");
		});

		verify(subjectService, atMostOnce()).findFirst("example@tetsuwan-tech.com", "username");
		assertEquals("example@tetsuwan-tech.com", result.getMessage());
	}

	@Test
	void testLoadUser_throwsMissingUserInfo() {

		ClientRegistration clientRegistration = ClientRegistration.withRegistrationId("clientRegistrationId")
				.authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
				.clientId("clientId")
				.redirectUriTemplate("https://tetsuwan-tech.com/atom-project")
				.authorizationUri("https://provider.com/oauth2/authorization")
				.tokenUri("https://provider.com/oauth2/token")
				.build();
		OAuth2AccessToken accessToken = new OAuth2AccessToken(TokenType.BEARER, "accessToken", now, now.plusSeconds(60), new LinkedHashSet<>(Arrays.asList("user", "profile")));
		OAuth2UserRequest oAuth2UserRequest = new OAuth2UserRequest(clientRegistration, accessToken, new HashMap<>());

		Exception result = assertThrows(OAuth2AuthenticationException.class, () -> subjectManagerImpl.loadUser(oAuth2UserRequest));

		assertEquals("[missing_user_info_uri] Missing required UserInfo Uri in UserInfoEndpoint for Client Registration: clientRegistrationId", result.getMessage());
	}

	@Test
	void testLoadUser_throwsMIssingUserNameAttribute() {

		ClientRegistration clientRegistration = ClientRegistration.withRegistrationId("clientRegistrationId")
				.authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
				.clientId("clientId")
				.redirectUriTemplate("https://tetsuwan-tech.com/atom-project")
				.authorizationUri("https://provider.com/oauth2/authorization")
				.tokenUri("https://provider.com/oauth2/token")
				.userInfoUri("https://provider.com/oauth2/user")
				.build();
		OAuth2AccessToken accessToken = new OAuth2AccessToken(TokenType.BEARER, "accessToken", now, now.plusSeconds(60), new LinkedHashSet<>(Arrays.asList("user", "profile")));
		OAuth2UserRequest oAuth2UserRequest = new OAuth2UserRequest(clientRegistration, accessToken, new HashMap<>());

		Exception result = assertThrows(OAuth2AuthenticationException.class, () -> subjectManagerImpl.loadUser(oAuth2UserRequest));

		assertEquals("[missing_user_name_attribute] Missing required \"user name\" attribute name in UserInfoEndpoint for Client Registration: clientRegistrationId", result.getMessage());
	}

	@Test
	void testLoadUser_throwsOAuth2AuthorizationException() {

		ClientRegistration clientRegistration = ClientRegistration.withRegistrationId("clientRegistrationId")
				.authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
				.clientId("clientId")
				.redirectUriTemplate("https://tetsuwan-tech.com/atom-project")
				.authorizationUri("https://provider.com/oauth2/authorization")
				.tokenUri("https://provider.com/oauth2/token")
				.userInfoUri("https://provider.com/oauth2/user")
				.userNameAttributeName("userNameAttributeName")
				.build();
		OAuth2AccessToken accessToken = new OAuth2AccessToken(TokenType.BEARER, "accessToken", now, now.plusSeconds(60), new LinkedHashSet<>(Arrays.asList("user", "profile")));
		OAuth2UserRequest oAuth2UserRequest = new OAuth2UserRequest(clientRegistration, accessToken, new HashMap<>());

		when(restOperations.exchange(ArgumentMatchers.<RequestEntity<?>>any(), ArgumentMatchers.<ParameterizedTypeReference<?>>any())).thenThrow(new OAuth2AuthorizationException(new OAuth2Error("boom", "boom", null)));
		Exception result = assertThrows(OAuth2AuthenticationException.class, () -> subjectManagerImpl.loadUser(oAuth2UserRequest));

		assertEquals("[invalid_user_info_response] An error occurred while attempting to retrieve the UserInfo Resource: Error details: [UserInfo Uri: https://provider.com/oauth2/user, Error Code: boom, Error Description: boom]", result.getMessage());
	}

	@Test
	void testLoadUser_throwsOAuth2AuthorizationExceptionNoMessage() {

		ClientRegistration clientRegistration = ClientRegistration.withRegistrationId("clientRegistrationId")
				.authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
				.clientId("clientId")
				.redirectUriTemplate("https://tetsuwan-tech.com/atom-project")
				.authorizationUri("https://provider.com/oauth2/authorization")
				.tokenUri("https://provider.com/oauth2/token")
				.userInfoUri("https://provider.com/oauth2/user")
				.userNameAttributeName("userNameAttributeName")
				.build();
		OAuth2AccessToken accessToken = new OAuth2AccessToken(TokenType.BEARER, "accessToken", now, now.plusSeconds(60), new LinkedHashSet<>(Arrays.asList("user", "profile")));
		OAuth2UserRequest oAuth2UserRequest = new OAuth2UserRequest(clientRegistration, accessToken, new HashMap<>());

		when(restOperations.exchange(ArgumentMatchers.<RequestEntity<?>>any(), ArgumentMatchers.<ParameterizedTypeReference<?>>any())).thenThrow(new OAuth2AuthorizationException(new OAuth2Error("boom", null, null)));
		assertThrows(OAuth2AuthenticationException.class, () -> subjectManagerImpl.loadUser(oAuth2UserRequest),
				"[invalid_user_info_response] An error occurred while attempting to retrieve the UserInfo Resource: Error details: [UserInfo Uri: https://provider.com/oauth2/user, Error Code: boom, Error Description: boom]");

	}

	@Test
	void testLoadUser_throwsRestClientException() {

		ClientRegistration clientRegistration = ClientRegistration.withRegistrationId("clientRegistrationId")
				.authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
				.clientId("clientId")
				.redirectUriTemplate("https://tetsuwan-tech.com/atom-project")
				.authorizationUri("https://provider.com/oauth2/authorization")
				.tokenUri("https://provider.com/oauth2/token")
				.userInfoUri("https://provider.com/oauth2/user")
				.userNameAttributeName("userNameAttributeName")
				.build();
		OAuth2AccessToken accessToken = new OAuth2AccessToken(TokenType.BEARER, "accessToken", now, now.plusSeconds(60), new LinkedHashSet<>(Arrays.asList("user", "profile")));
		OAuth2UserRequest oAuth2UserRequest = new OAuth2UserRequest(clientRegistration, accessToken, new HashMap<>());

		when(restOperations.exchange(ArgumentMatchers.<RequestEntity<?>>any(), ArgumentMatchers.<ParameterizedTypeReference<?>>any())).thenThrow(new RestClientException("boom"));
		assertThrows(OAuth2AuthenticationException.class, () -> subjectManagerImpl.loadUser(oAuth2UserRequest),
				"[invalid_user_info_response] An error occurred while attempting to retrieve the UserInfo Resource: Error details: [UserInfo Uri: https://provider.com/oauth2/user, Error Code: boom, Error Description: boom]");

	}

	@Test
	void testLoadUser_newUserWithNotAllowedEmailAddress() {

		ClientRegistration clientRegistration = ClientRegistration.withRegistrationId("clientRegistrationId")
				.authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
				.clientId("clientId")
				.redirectUriTemplate("https://tetsuwan-tech.com/atom-project")
				.authorizationUri("https://provider.com/oauth2/authorization")
				.tokenUri("https://provider.com/oauth2/token")
				.userInfoUri("https://provider.com/oauth2/user")
				.userNameAttributeName("userNameAttributeName")
				.build();
		OAuth2AccessToken accessToken = new OAuth2AccessToken(TokenType.BEARER, "accessToken", now, now.plusSeconds(60), new LinkedHashSet<>(Arrays.asList("user", "profile")));
		OAuth2UserRequest oAuth2UserRequest = new OAuth2UserRequest(clientRegistration, accessToken, new HashMap<>());

		Map<String, Object> attributes = new HashMap<>() {
			private static final long serialVersionUID = 1L;
			{
				put("email", "example@tetsuwan-tech.com");
			}
		};
		Mockito.<ResponseEntity<?>>when(restOperations.exchange(ArgumentMatchers.<RequestEntity<?>>any(), ArgumentMatchers.<ParameterizedTypeReference<?>>any())).thenReturn(ResponseEntity.ok(attributes));
		subjectManagerImpl.setInvalidDomains("tetsuwan-tech.com");
		subjectManagerImpl.setValidDomains(null);

		assertThrows(OAuth2AuthenticationException.class, () -> subjectManagerImpl.loadUser(oAuth2UserRequest), "[invalid_domain_name] Domain name not allowed: example@tetsuwan-tech.com");
	}

	@Test
	void testLoadUser_newUser() {

		ClientRegistration clientRegistration = ClientRegistration.withRegistrationId("clientRegistrationId")
				.authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
				.clientId("clientId")
				.redirectUriTemplate("https://tetsuwan-tech.com/atom-project")
				.authorizationUri("https://provider.com/oauth2/authorization")
				.tokenUri("https://provider.com/oauth2/token")
				.userInfoUri("https://provider.com/oauth2/user")
				.userNameAttributeName("userNameAttributeName")
				.build();
		OAuth2AccessToken accessToken = new OAuth2AccessToken(TokenType.BEARER, "accessToken", now, now.plusSeconds(60), new LinkedHashSet<>(Arrays.asList("user", "profile")));
		OAuth2UserRequest oAuth2UserRequest = new OAuth2UserRequest(clientRegistration, accessToken, new HashMap<>());

		Map<String, Object> attributes = new HashMap<>() {
			private static final long serialVersionUID = 1L;
			{
				put("email", "example@tetsuwan-tech.com");
				put("name", "givenName familyName");
				put("given_name", "givenName");
				put("family_name", "familyName");
				put("picture", "https://tetsuwan-tech.com/profile.png");
			}
		};
		Mockito.<ResponseEntity<?>>when(restOperations.exchange(ArgumentMatchers.<RequestEntity<?>>any(), ArgumentMatchers.<ParameterizedTypeReference<?>>any())).thenReturn(ResponseEntity.ok(attributes));

		when(subjectService.findFirst("example@tetsuwan-tech.com", "username")).thenReturn(null);
		when(dateTimePeriodUtils.now()).thenReturn(now);
		ArgumentCaptor<Subject> argumentCapture = ArgumentCaptor.forClass(Subject.class);
		doAnswer(invocation -> {
			Subject subject = invocation.getArgument(0);

			assertNull(subject.getId());

			subject.setId(1L);
			return null;
		}).when(subjectService).create(argumentCapture.capture());

		when(subjectService.get(1L)).then(invocation -> {
			Subject subject = new Subject();
			subject.setUsername("example@tetsuwan-tech.com");
			subject.setGivenName("givenName");
			subject.setFamilyName("familyName");
			// subject.setId((Long) invocation.getArgument(0));
			return subject;
		});

		OAuth2User result = subjectManagerImpl.loadUser(oAuth2UserRequest);

		assertEquals("givenName familyName", result.getName());
		assertEquals(attributes, result.getAttributes());
	}

	@Test
	void testLoadUser_userExists() {

		ClientRegistration clientRegistration = ClientRegistration.withRegistrationId("clientRegistrationId")
				.authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
				.clientId("clientId")
				.redirectUriTemplate("https://tetsuwan-tech.com/atom-project")
				.authorizationUri("https://provider.com/oauth2/authorization")
				.tokenUri("https://provider.com/oauth2/token")
				.userInfoUri("https://provider.com/oauth2/user")
				.userNameAttributeName("userNameAttributeName")
				.build();
		OAuth2AccessToken accessToken = new OAuth2AccessToken(TokenType.BEARER, "accessToken", now, now.plusSeconds(60), new LinkedHashSet<>(Arrays.asList("user", "profile")));
		OAuth2UserRequest oAuth2UserRequest = new OAuth2UserRequest(clientRegistration, accessToken, new HashMap<>());

		Map<String, Object> attributes = new HashMap<>() {
			private static final long serialVersionUID = 1L;
			{
				put("email", "example@tetsuwan-tech.com");
				put("name", "givenName familyName");
				put("given_name", "givenName");
				put("family_name", "familyName");
				put("picture", "https://tetsuwan-tech.com/profile.png");
			}
		};
		Mockito.<ResponseEntity<?>>when(restOperations.exchange(ArgumentMatchers.<RequestEntity<?>>any(), ArgumentMatchers.<ParameterizedTypeReference<?>>any())).thenReturn(ResponseEntity.ok(attributes));

		when(subjectService.findFirst(eq("example@tetsuwan-tech.com"), eq("username"))).then(invocation -> {
			Subject subject = new Subject((String) invocation.getArgument(0));
			subject.setGivenName("givenName");
			subject.setFamilyName("familyName");

			// subject.setId(1L);
			return subject;
		});

		OAuth2User result = subjectManagerImpl.loadUser(oAuth2UserRequest);

		assertEquals("givenName familyName", result.getName());
		assertEquals(attributes, result.getAttributes());
	}

	@Test
	void testIsDomainAllowed_ValidDomainExists() {

		subjectManagerImpl.setValidDomains("tetsuwan-tech.com");
		subjectManagerImpl.setInvalidDomains(null);

		boolean result = subjectManagerImpl.isDomainAllowed("example@tetsuwan-tech.com");

		assertTrue(result);
	}

	@Test
	void testIsDomainAllowed_ValidDomainNotExists() {

		subjectManagerImpl.setValidDomains("tetsuwan-tech.com");
		subjectManagerImpl.setInvalidDomains(null);

		boolean result = subjectManagerImpl.isDomainAllowed("example@thisisnotvalid.com");

		assertFalse(result);

	}

	@Test
	void testIsDomainAllowed_InvalidDomainExists() {

		subjectManagerImpl.setValidDomains(null);
		subjectManagerImpl.setInvalidDomains("tetsuwan-tech.com");

		boolean result = subjectManagerImpl.isDomainAllowed("example@tetsuwan-tech.com");

		assertFalse(result);

	}

	@Test
	void testIsDomainAllowed_InvalidDomainNotExists() {

		subjectManagerImpl.setValidDomains(null);
		subjectManagerImpl.setInvalidDomains("thisisnotvalid.com");

		boolean result = subjectManagerImpl.isDomainAllowed("example@tetsuwan-tech.com");

		assertTrue(result);

	}

	@Test
	void testIsDomainAllowed_NoDomain() {

		boolean result = subjectManagerImpl.isDomainAllowed("example@tetsuwan-tech.com");

		assertTrue(result);
	}

}
