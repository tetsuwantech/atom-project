package com.tetsuwantech.atom.manager;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 - 2020 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.atMostOnce;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tetsuwantech.atom.database.GenericService;
import com.tetsuwantech.atom.database.TestEntity;

@ExtendWith(MockitoExtension.class)
class GenericManagerTest {

	@Mock
	private GenericService<TestEntity, String> genericService;

	@InjectMocks
	private TestManager testManagerImpl;

	@Test
	void testCreateModel() {

		TestEntity testEntity = new TestEntity(1L);

		TestModel testModel = testManagerImpl.createModel(testEntity);

		assertEquals(1L, testModel.getId());
	}

	@Test
	void testGet() {

		ArgumentCaptor<Long> argumentCapture = ArgumentCaptor.forClass(Long.class);
		doAnswer(invocation -> {

			TestEntity testEntity = new TestEntity((Long) invocation.getArgument(0));
			return testEntity;

		}).when(genericService).get(argumentCapture.capture());

		TestModel result = testManagerImpl.get(1L);

		verify(genericService, atMostOnce()).get(argumentCapture.getValue());
		assertEquals(1L, result.getId());
	}

	@Test
	void testRemove() {

		doNothing().when(genericService).remove(1L);

		testManagerImpl.remove(1L);

		verify(genericService, atMostOnce()).remove(1L);
	}

	@Test
	void testFindFirst() {

		ArgumentCaptor<String> argumentCaptureWhere = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<String> argumentCaptureColumn = ArgumentCaptor.forClass(String.class);
		doAnswer(invocation -> {

			TestEntity testEntity = new TestEntity(1L);
			return testEntity;

		}).when(genericService).findFirst(argumentCaptureWhere.capture(), argumentCaptureColumn.capture());

		TestModel result = testManagerImpl.findFirst("text", "text");

		verify(genericService, atMostOnce()).findAll(argumentCaptureWhere.getValue(), argumentCaptureColumn.getValue());
		assertEquals(1L, result.getId());
	}

	@Test
	void testFindAll() {

		ArgumentCaptor<String> argumentCaptureWhere = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<String> argumentCaptureColumn = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<String> argumentCaptureOrderBy = ArgumentCaptor.forClass(String.class);
		doAnswer(invocation -> {

			TestEntity testEntity = new TestEntity(1L);
			return List.of(testEntity);

		}).when(genericService).findAll(argumentCaptureWhere.capture(), argumentCaptureColumn.capture(), argumentCaptureOrderBy.capture());

		List<TestModel> results = testManagerImpl.findAll("text", "text", "id");

		verify(genericService, atMostOnce()).findAll(argumentCaptureWhere.getValue(), argumentCaptureColumn.getValue(), argumentCaptureOrderBy.getValue());
		assertEquals(1, results.size());
		assertEquals(1L, results.get(0).getId());
	}

	@Test
	void testCount() {
		ArgumentCaptor<String> argumentCaptureWhere = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<String> argumentCaptureColumn = ArgumentCaptor.forClass(String.class);
		doAnswer(invocation -> 1L).when(genericService).count(argumentCaptureWhere.capture(), argumentCaptureColumn.capture());

		long result = testManagerImpl.count("text", "text");

		verify(genericService, atMostOnce()).count(argumentCaptureWhere.getValue(), argumentCaptureColumn.getValue());
		assertEquals(1L, result);
	}

	@Test
	void testSearch() {
		ArgumentCaptor<String> argumentCaptureWhere = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<String> argumentCaptureColumn = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<String> argumentCaptureOrderBy = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<String[]> argumentCaptureFields = ArgumentCaptor.forClass(String[].class);
		doAnswer(invocation -> {

			TestEntity testEntity = new TestEntity(1L);
			return List.of(testEntity);

		}).when(genericService).search(argumentCaptureWhere.capture(), argumentCaptureColumn.capture(), argumentCaptureOrderBy.capture(), argumentCaptureFields.capture());

		List<TestModel> results = testManagerImpl.search("text", "text", "id", new String[] { "id" });

		verify(genericService, atMostOnce()).findAll(argumentCaptureWhere.getValue(), argumentCaptureColumn.getValue(), argumentCaptureOrderBy.getValue());
		assertEquals(1, results.size());
		assertEquals(1L, results.get(0).getId());
	}
}
