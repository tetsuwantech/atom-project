package com.tetsuwantech.atom.manager;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 - 2020 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.List;

import com.tetsuwantech.atom.database.TestEntity;
import com.tetsuwantech.atom.web.authentication.SubjectModel;

class TestManager extends GenericManager<TestEntity, String, TestModel, TestModel> {

	@Override
	public void save(TestModel model) {

	}

	@Override
	public List<TestModel> findAll(TestModel where) {
		return super.findAll(null, null, "id");
	}

	public Long count(TestModel where) {
		return super.count(where.getText(), null);
	}

	@Override
	public Long countAll(SubjectModel subject) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<TestModel> search(TestModel where, String text) {
		return super.search(where.getText(), null, text, null);
	}

	@Override
	protected TestModel createModel(TestEntity testEntity) {
		return new TestModel(testEntity);
	}

}
