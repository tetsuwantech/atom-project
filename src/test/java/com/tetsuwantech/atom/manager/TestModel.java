package com.tetsuwantech.atom.manager;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 - 2020 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.tetsuwantech.atom.database.TestEntity;
import com.tetsuwantech.atom.web.DeleteModel;

public class TestModel extends DeleteModel {

	private static final long serialVersionUID = 1L;

	private String text;

	public TestModel() {

	}

	public TestModel(Long id) {
		super(id);
	}

	public TestModel(TestEntity testEntity) {
		super(testEntity);
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
