package com.tetsuwantech.atom.util;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class InstantConverterTest {

	private InstantConverter instantConverter;

	@BeforeEach
	void setUp() {
		new TimeZoneUtil().setTimeZone("UTC");
		Locale.setDefault(new Locale("en", "AU"));
		instantConverter = new InstantConverter("dd/MMM/yyyy h:mm a");
	}

	@Test
	void testConvertNotNull() {
		Instant instant = instantConverter.convert("13/Sep./2018 9:40 pm");
		assertEquals(LocalDateTime.parse("13/Sep./2018 9:40 pm", DateTimeFormatter.ofPattern("dd/MMM/yyyy h:mm a")).atZone(ZoneId.of("UTC")).toInstant(), instant);
	}

	@Test
	void testConvertNull() {
		Instant instant = instantConverter.convert(null);
		assertNull(instant);
	}

}
