package com.tetsuwantech.atom.util;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactory;

import com.tetsuwantech.atom.util.MailSender.Message;
import com.tetsuwantech.atom.web.authentication.SubjectModel;

@ExtendWith(MockitoExtension.class)
class MailSenderTest {

	@Mock
	private JavaMailSender javaMailSender;

	@InjectMocks
	private MailSender mailSender;

	@BeforeEach
	void setUp() throws Exception {
		FreeMarkerConfigurationFactory freemarkerConfigurationFactory = new VersionedFreeMarkerConfigurationFactoryBean();
		freemarkerConfigurationFactory.setTemplateLoaderPath("classpath:/");
		freemarkerConfigurationFactory.setDefaultEncoding("UTF-8");

		// not fussed about using @Spy on this
		mailSender.setFreemarkerConfiguration(freemarkerConfigurationFactory.createConfiguration());
		mailSender.setAtomFrom("example@tetsuwan-tech.com");
		mailSender.setAtomSubject("[Test Application]");
	}

	@Test
	void testSend() {

		ArgumentCaptor<MimeMessage> argumentCapture = ArgumentCaptor.forClass(MimeMessage.class);
		doNothing().when(javaMailSender).send(argumentCapture.capture());
		when(javaMailSender.createMimeMessage()).thenReturn(new JavaMailSenderImpl().createMimeMessage());

		SubjectModel subject = new SubjectModel(1L);
		subject.setUsername("example@tetsuwan-tech.com");
		subject.setGivenName("givenName");
		subject.setFamilyName("familyName");

		Map<String, Object> content = new TreeMap<>();
		content.put("subject", subject);

		mailSender.send(subject, Message.WELCOME, content);

		verify(javaMailSender).send(argumentCapture.getValue());
		try {
			assertTrue(Arrays.asList(argumentCapture.getValue().getFrom()).contains(new InternetAddress("example@tetsuwan-tech.com")));
			assertTrue(Arrays.asList(argumentCapture.getValue().getRecipients(RecipientType.TO)).contains(new InternetAddress("example@tetsuwan-tech.com", "givenName FamilyName")));
			assertNull(argumentCapture.getValue().getRecipients(RecipientType.BCC));
			assertEquals("[Test Application] Welcome", argumentCapture.getValue().getSubject());
		} catch (UnsupportedEncodingException | MessagingException exception) {
			fail(exception);
		}

	}
}
