package com.tetsuwantech.atom.util.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 - 2020 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atMostOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;

import com.tetsuwantech.atom.web.authentication.SubjectModel;

@ExtendWith(MockitoExtension.class)
class ConfigAuthenticationSuccessHandlerTest {

	@Mock
	private SubjectHandler subjectHandler;

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Mock
	private Authentication authentication;

	@InjectMocks
	private ConfigAuthenticationSuccessHandler configAuthenticationSuccessHandler;

	@Test
	void testOnAuthenticationSuccess() throws Exception {

		SubjectModel subject = new SubjectModel();
		when(authentication.getPrincipal()).thenReturn(subject);

		configAuthenticationSuccessHandler.onAuthenticationSuccess(request, response, authentication);

		verify(authentication, atMostOnce()).getPrincipal();
		verify(subjectHandler, atMostOnce()).login(eq(subject), eq(request), eq(response));

	}

}
