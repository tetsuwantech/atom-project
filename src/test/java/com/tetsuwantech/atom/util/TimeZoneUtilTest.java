package com.tetsuwantech.atom.util;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 - 2020 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Instant;
import java.time.ZoneId;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TimeZoneUtilTest {

	private TimeZoneUtil timeZoneUtil;

	@BeforeEach
	void setUp() {
		timeZoneUtil = new TimeZoneUtil();
		timeZoneUtil.setTimeZone("UTC");
	}

	@Test
	void testToServerTimeZone() {

		Instant now = Instant.ofEpochMilli(1591704397149L);
		ZoneId zoneId = ZoneId.of("Australia/Sydney");

		Instant result = TimeZoneUtil.toServerTimeZone(now, zoneId);

		assertEquals(1591668397149L, result.toEpochMilli());
	}

}
