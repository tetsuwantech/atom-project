package com.tetsuwantech.atom.util;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 - 2019 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Instant;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DateTimePeriodUtilsTest {

	@Test
	void testIsBetween() {

		DateTimePeriod dateTimePeriod = new DateTimePeriod() {

			@Override
			public Instant getStartDate() {
				return Instant.MIN;
			}

			@Override
			public Instant getEndDate() {
				return Instant.MAX;
			}
		};
		assertTrue(DateTimePeriodUtils.isBetween(dateTimePeriod));
	}

	@Test
	void testIsNotBetween() {

		DateTimePeriod dateTimePeriod = new DateTimePeriod() {

			@Override
			public Instant getStartDate() {
				return Instant.MIN;
			}

			@Override
			public Instant getEndDate() {
				return Instant.MIN;
			}
		};
		assertTrue(DateTimePeriodUtils.isNotBetween(dateTimePeriod));
	}

	@Test
	void testEndIsNullAndBetween() {

		DateTimePeriod dateTimePeriod = new DateTimePeriod() {

			@Override
			public Instant getStartDate() {
				return Instant.MIN;
			}

			@Override
			public Instant getEndDate() {
				return null;
			}
		};
		assertTrue(DateTimePeriodUtils.isBetween(dateTimePeriod));
	}

	@Test
	void testEndIsNullAndBetweenFalse() {

		DateTimePeriod dateTimePeriod = new DateTimePeriod() {

			@Override
			public Instant getStartDate() {
				return Instant.MAX;
			}

			@Override
			public Instant getEndDate() {
				return null;
			}
		};
		assertFalse(DateTimePeriodUtils.isBetween(dateTimePeriod));
	}

	@Test
	void testEndIsNullAndNotBetween() {

		DateTimePeriod dateTimePeriod = new DateTimePeriod() {

			@Override
			public Instant getStartDate() {
				return Instant.MIN;
			}

			@Override
			public Instant getEndDate() {
				return null;
			}
		};
		assertFalse(DateTimePeriodUtils.isNotBetween(dateTimePeriod));
	}
}
