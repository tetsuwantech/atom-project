package com.tetsuwantech.atom.util;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tetsuwantech.atom.util.authentication.SubjectHandler;
import com.tetsuwantech.atom.web.authentication.SubjectModel;

public class SubjectHandlerImpl implements SubjectHandler {

	@SuppressWarnings(value = "unused")
	private static final Logger log = LoggerFactory.getLogger(SubjectHandlerImpl.class);

	@Override
	public void build(SubjectModel subject, Map<String, String> map) {

	}

	@Override
	public void login(SubjectModel subject, HttpServletRequest request, HttpServletResponse response) {

	}

	@Override
	public boolean isDeactivatable(SubjectModel subject) {
		return subject.isDeactivatable();
	}

	@Override
	public void deactivate(SubjectModel subject) {

	}
}
