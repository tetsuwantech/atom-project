package com.tetsuwantech.atom.database.audit;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 - 2020 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.jdbc.JdbcTestUtils.countRowsInTable;
import static org.springframework.test.jdbc.JdbcTestUtils.countRowsInTableWhere;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.sql.DataSource;
import javax.transaction.Transactional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;
import org.springframework.test.jdbc.JdbcTestUtils;

import com.tetsuwantech.atom.database.GenericDAO;
import com.tetsuwantech.atom.database.GenericEntity;
import com.tetsuwantech.atom.database.GenericService;
import com.tetsuwantech.atom.database.GenericServiceImpl;

@Entity
@Table(name = "parent")
class Parent extends GenericEntity {

	private static final long serialVersionUID = 1L;

	public Parent() {
		super();
	}

	@Auditable
	@Column(name = "parent_field", length = LONG_LENGTH)
	private String parentField;

	@OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Child> kids = new LinkedList<>();

	public void setParentField(String parentField) {
		this.parentField = parentField;
	}

	public List<Child> getChildren() {
		return kids;
	}

	@Auditable(recurse = true, field = "kids")
	public void setChildren(List<Child> kids) {
		this.kids.clear();
		this.kids.addAll(kids);
	}

}

@Entity
@Table(name = "child")
class Child extends GenericEntity {

	private static final long serialVersionUID = 1L;

	public Child() {
		super();
	}

	@Auditable
	@ManyToOne
	@JoinColumn(name = "parent_id")
	private Parent parent;

	@Column(name = "child_field", length = LONG_LENGTH)
	private String childField;

	public void setParent(Parent parent) {
		this.parent = parent;
	}

	@Auditable
	public void setChildField(String childField) {
		this.childField = childField;
	}

}

@Named(value = "ParentDAO")
class ParentDAO extends GenericDAO<Parent, String> {

	public ParentDAO() {
		super(Parent.class);
	}

}

interface ParentService extends GenericService<Parent, String> {

}

@Transactional
@Named(value = "ParentService")
class ParentServiceImpl extends GenericServiceImpl<Parent, String> implements ParentService {

	@Override
	public Parent findFirst(String where, String column) {

		Parent parent = super.findFirst(where, column);
		parent.getChildren().size();

		return parent;

	}

}

@SpringJUnitWebConfig(locations = "classpath:spring-context.xml")
class AuditAspectIT {

	@Inject
	private DataSource dataSource;

	@Inject
	private ParentService parentService;

	private JdbcTemplate jdbcTemplate;

	@BeforeEach
	void setUp() {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@AfterEach
	void tearDown() {
		JdbcTestUtils.deleteFromTables(jdbcTemplate, "child", "parent");
		JdbcTestUtils.deleteFromTables(jdbcTemplate, "audit", "subject_authority", "subject");
	}

	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', 'ENABLED')")
	@Sql(statements = "INSERT INTO subject_authority (authority, subject_id) VALUES ('ROLE_USER', (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com'))")
	@Sql(statements = "INSERT INTO parent (parent_field) VALUES ('parentField')")
	@Sql(statements = "INSERT INTO child (parent_id, child_field) VALUES ((select id from parent where parent_field = 'parentField'), 'childField')")
	@WithAnonymousUser
	@Test
	void testDoUpdate_withAnonymousUser() {

		Parent parent = parentService.findFirst("parentField", "parentField");
		parent.setParentField("newparentField");
		List<Child> children = parent.getChildren();
		Child child = children.get(0);
		child.setChildField("newchildField");
		parent.setChildren(children);
		parentService.save(parent);

		assertEquals(0, countRowsInTable(jdbcTemplate, "audit"));
	}

	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', 'ENABLED')")
	@Sql(statements = "INSERT INTO subject_authority (authority, subject_id) VALUES ('ROLE_USER', (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com'))")
	@Sql(statements = "INSERT INTO parent (parent_field) VALUES ('parentField')")
	@Sql(statements = "INSERT INTO child (parent_id, child_field) VALUES ((select id from parent where parent_field = 'parentField'), 'childField')")
	@WithUserDetails(value = "example@tetsuwan-tech.com")
	@Test
	void testDoUpdate_withUserWithSavedEntity() {

		Parent parent = parentService.findFirst("parentField", "parentField");
		parent.setParentField("newparentField");
		List<Child> children = parent.getChildren();
		Child child = children.get(0);
		child.setChildField("newchildField");
		parent.setChildren(children);
		parentService.save(parent);

		assertEquals(1, countRowsInTableWhere(jdbcTemplate, "audit",
				"subject_id = (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com') AND audit_table = 'parent' AND audit_column = 'parent_field' AND old_value = 'parentField' AND new_value = 'newparentField' AND action = 'UPDATE'"));
		assertEquals(1, countRowsInTableWhere(jdbcTemplate, "audit",
				"subject_id = (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com') AND audit_table = 'child' AND audit_column = 'child_field' AND old_value = 'childField' AND new_value = 'newchildField' AND action = 'UPDATE'"));
	}

	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', 'ENABLED')")
	@Sql(statements = "INSERT INTO subject_authority (authority, subject_id) VALUES ('ROLE_USER', (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com'))")
	@Sql(statements = "INSERT INTO parent (parent_field) VALUES ('parentFieldOne')")
	@Sql(statements = "INSERT INTO parent (parent_field) VALUES ('parentFieldTwo')")
	@Sql(statements = "INSERT INTO child (parent_id, child_field) VALUES ((select id from parent where parent_field = 'parentFieldOne'), 'childField')")
	@WithUserDetails(value = "example@tetsuwan-tech.com")
	@Test
	void testDoUpdate_withUserWithSavedEntityUpdateChild() {

		Parent parentOne = parentService.findFirst("parentFieldOne", "parentField");
		Child child = parentOne.getChildren().get(0);
		parentOne.setChildren(Collections.emptyList());

		Parent parentTwo = parentService.findFirst("parentFieldTwo", "parentField");
		child.setParent(parentTwo);
		parentService.save(parentTwo);

		assertEquals(1, countRowsInTableWhere(jdbcTemplate, "audit",
				"subject_id = (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com') AND audit_table = 'child' AND audit_column = 'parent_id' AND old_value = CAST((select id from parent where parent_field = 'parentFieldOne') AS VARCHAR) AND new_value = CAST((select id from parent where parent_field = 'parentFieldTwo') AS VARCHAR) AND action = 'UPDATE'"));
	}

	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', 'ENABLED')")
	@Sql(statements = "INSERT INTO subject_authority (authority, subject_id) VALUES ('ROLE_USER', (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com'))")
	@Sql(statements = "INSERT INTO parent (parent_field) VALUES ('parentField')")
	@Sql(statements = "INSERT INTO child (parent_id, child_field) VALUES ((select id from parent where parent_field = 'parentField'), 'childField')")
	@WithAnonymousUser
	@Test
	void testDoDelete_withAnonymousUser() {

		Parent parent = parentService.findFirst("parentField", "parentField");
		parentService.remove(parent.getId());

		assertEquals(0, countRowsInTable(jdbcTemplate, "audit"));

	}

	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', 'ENABLED')")
	@Sql(statements = "INSERT INTO subject_authority (authority, subject_id) VALUES ('ROLE_USER', (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com'))")
	@Sql(statements = "INSERT INTO parent (parent_field) VALUES ('parentField')")
	@Sql(statements = "INSERT INTO child (parent_id, child_field) VALUES ((select id from parent where parent_field = 'parentField'), 'childField')")
	@WithUserDetails(value = "example@tetsuwan-tech.com")
	@Test
	void testDoDelete_withUser() {

		Parent parent = parentService.findFirst("parentField", "parentField");
		parentService.remove(parent.getId());

		assertEquals(1, countRowsInTableWhere(jdbcTemplate, "audit",
				"subject_id = (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com') AND audit_table = 'parent' AND audit_column = 'parent_field' AND old_value = 'parentField' AND new_value IS NULL AND action = 'DELETE'"));
		assertEquals(1, countRowsInTableWhere(jdbcTemplate, "audit",
				"subject_id = (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com') AND audit_table = 'child' AND audit_column = 'child_field' AND old_value = 'childField' AND new_value IS NULL AND action = 'DELETE'"));

	}

	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', 'ENABLED')")
	@Sql(statements = "INSERT INTO subject_authority (authority, subject_id) VALUES ('ROLE_USER', (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com'))")
	@WithAnonymousUser
	@Test
	void testDoSave_withAnonymousUser() {

		Parent parent = new Parent();
		parent.setParentField("newparentField");
		Child child = new Child();
		child.setChildField("childField");
		parent.setChildren(List.of(child));
		parentService.create(parent);

		assertEquals(0, countRowsInTable(jdbcTemplate, "audit"));
	}

	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', 'ENABLED')")
	@Sql(statements = "INSERT INTO subject_authority (authority, subject_id) VALUES ('ROLE_USER', (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com'))")
	@WithUserDetails(value = "example@tetsuwan-tech.com")
	@Test
	void testDoSave_withUser() {

		Parent parent = new Parent();
		parent.setParentField("parentField");
		Child child = new Child();
		child.setChildField("childField");
		parent.setChildren(List.of(child));
		parentService.create(parent);

		assertEquals(1, countRowsInTableWhere(jdbcTemplate, "audit",
				"subject_id = (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com') AND audit_table = 'parent' AND audit_column = 'parent_field' AND old_value IS NULL AND new_value = 'parentField' AND action = 'INSERT'"));
		assertEquals(0, countRowsInTableWhere(jdbcTemplate, "audit",
				"subject_id = (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com') AND audit_table = 'child' AND audit_column = 'parent_id' AND old_value IS NULL AND new_value = CAST((select id from parent where parent_field = 'parentField') AS VARCHAR) AND action = 'INSERT'"));
		assertEquals(1, countRowsInTableWhere(jdbcTemplate, "audit",
				"subject_id = (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com') AND audit_table = 'child' AND audit_column = 'child_field' AND old_value IS NULL AND new_value = 'childField' AND action = 'INSERT'"));

	}
}
