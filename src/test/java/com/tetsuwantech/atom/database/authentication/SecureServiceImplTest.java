package com.tetsuwantech.atom.database.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 - 2020 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.atMostOnce;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.tetsuwantech.atom.database.GenericDAO;
import com.tetsuwantech.atom.web.authentication.SubjectModel;

@ExtendWith(MockitoExtension.class)
class SecureServiceImplTest {

	@Mock
	private GenericDAO<SecureEntity, Object> genericDAO;

	@Mock
	private Authentication authentication;

	@Mock
	private SecurityContext securityContext;

	@InjectMocks
	private SecureTestServiceImpl secureTestServiceImpl;

	@BeforeEach
	protected void setUp() {
		when(securityContext.getAuthentication()).thenReturn(authentication);
		when(authentication.getPrincipal()).thenReturn(new SubjectModel(1L));
		verify(authentication, atMostOnce()).getPrincipal();
	}

	@Test
	void testSave() {

		Subject subject = new Subject("example@tetsuwan-tech.com");
		subject.setId(1L);

		SecureEntity secureEntity = new SecureEntity(subject);
		secureEntity.setId(1L);

		when(genericDAO.find(1L)).thenReturn(secureEntity);
		when(genericDAO.persistOrMerge(secureEntity)).thenReturn(secureEntity);

		SecurityContextHolder.setContext(securityContext);
		SecureEntity result = secureTestServiceImpl.save(secureEntity);
		SecurityContextHolder.clearContext();

		verify(genericDAO, atMostOnce()).persistOrMerge(secureEntity);
		assertEquals(secureEntity, result);
	}

	@Test
	void testGet_EntityExists() {

		Subject subject = new Subject("example@tetsuwan-tech.com");
		subject.setId(1L);

		SecureEntity secureEntity = new SecureEntity(subject);
		when(genericDAO.find(1L)).thenReturn(secureEntity);

		SecurityContextHolder.setContext(securityContext);
		SecureEntity result = secureTestServiceImpl.get(1L);
		SecurityContextHolder.clearContext();

		verify(genericDAO, atMostOnce()).find(1L);
		assertEquals(secureEntity, result);
	}

	@Test
	void testGet_EntityExistsNotOwner() {

		Subject subject = new Subject("other-example@tetsuwan-tech.com");
		subject.setId(2L);

		SecureEntity secureEntity = new SecureEntity(subject);
		secureEntity.setId(1L);
		when(genericDAO.find(1L)).thenReturn(secureEntity);

		SecurityContextHolder.setContext(securityContext);
		assertThrows(NotAllowedException.class, () -> secureTestServiceImpl.get(1L));
		SecurityContextHolder.clearContext();

		verify(genericDAO, atMostOnce()).find(1L);
	}

	@Test
	void testRemove() {

		Subject subject = new Subject("example@tetsuwan-tech.com");
		subject.setId(1L);

		SecureEntity secureEntity = new SecureEntity(subject);
		when(genericDAO.find(1L)).thenReturn(secureEntity);

		SecurityContextHolder.setContext(securityContext);
		secureTestServiceImpl.remove(1L);
		SecurityContextHolder.clearContext();

		verify(genericDAO, times(2)).find(1L);
		verify(genericDAO, atMostOnce()).remove(secureEntity);
	}
}
