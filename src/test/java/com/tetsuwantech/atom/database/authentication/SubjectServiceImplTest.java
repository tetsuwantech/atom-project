package com.tetsuwantech.atom.database.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.hibernate.search.exception.SearchException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tetsuwantech.atom.database.GenericDAO;

@ExtendWith(MockitoExtension.class)
class SubjectServiceImplTest {

	@Mock
	private GenericDAO<Subject, String> genericDAO;

	@InjectMocks
	private SubjectServiceImpl subjectServiceImpl;

	@Test
	void testCreate() {

		ArgumentCaptor<Subject> argumentCapture = ArgumentCaptor.forClass(Subject.class);
		when(genericDAO.persistOrMerge(argumentCapture.capture())).then(invocation -> {
			Subject subject = invocation.getArgument(0);
			assertNull(subject.getId());

			subject.setId(1L);
			return subject;
		});

		Subject subject = new Subject("example@tetsuwan-tech.com");
		assertNull(subject.getId());

		subjectServiceImpl.create(subject);

		verify(genericDAO).persistOrMerge(argumentCapture.getValue());
		assertEquals(argumentCapture.getValue().getId(), subject.getId());
	}

	@Test
	void testSave() {

		ArgumentCaptor<Subject> argumentCapture = ArgumentCaptor.forClass(Subject.class);
		when(genericDAO.persistOrMerge(argumentCapture.capture())).then(invocation -> {
			Subject subject = invocation.getArgument(0);
			assertNotNull(subject.getId());

			return subject;
		});

		Subject subject = new Subject("example@tetsuwan-tech.com");
		subject.setId(1L);

		subjectServiceImpl.save(subject);

		verify(genericDAO).persistOrMerge(argumentCapture.getValue());
		assertEquals(1L, argumentCapture.getValue().getId());
		assertEquals("example@tetsuwan-tech.com", argumentCapture.getValue().getUsername());
	}

	@Test
	void testGetLong() {

		when(genericDAO.find(anyLong())).then(invocation -> {
			Long id = invocation.getArgument(0);
			Subject subject = new Subject("example@tetsuwan-tech.com");
			subject.setId(id);
			return subject;
		});

		Subject subject = subjectServiceImpl.get(1L);

		verify(genericDAO).find(1L);
		assertEquals(1L, subject.getId());
	}

	@Test
	void testRemove() {

		ArgumentCaptor<Subject> argumentCapture = ArgumentCaptor.forClass(Subject.class);
		doNothing().when(genericDAO).remove(argumentCapture.capture());

		subjectServiceImpl.remove(1L);

		verify(genericDAO).remove(argumentCapture.getValue());
	}

	@Test
	void testFindAllStringStringString() {

		when(genericDAO.findAll("example@tetsuwan-tech.com", "username", "username")).then(invocation -> {
			Subject subject = new Subject((String) invocation.getArgument(0));
			subject.setId(1L);
			return Arrays.asList(subject);
		});

		List<Subject> subjects = subjectServiceImpl.findAll("example@tetsuwan-tech.com", "username", "username");

		verify(genericDAO).findAll("example@tetsuwan-tech.com", "username", "username");
		assertEquals(1, subjects.size());
		assertEquals("example@tetsuwan-tech.com", subjects.get(0).getUsername());
	}

	@Test
	void testFindFirstStringString() {

		when(genericDAO.findFirst("example@tetsuwan-tech.com", "username")).then(invocation -> {
			Subject subject = new Subject((String) invocation.getArgument(0));
			subject.setId(1L);
			return subject;
		});

		Subject subject = subjectServiceImpl.findFirst("example@tetsuwan-tech.com", "username");

		verify(genericDAO).findFirst("example@tetsuwan-tech.com", "username");
		assertEquals("example@tetsuwan-tech.com", subject.getUsername());
	}

	@Test
	void testCountKString() {

		when(genericDAO.count("example@tetsuwan-tech.com", "username")).thenReturn(1L);

		Long count = subjectServiceImpl.count("example@tetsuwan-tech.com", "username");

		verify(genericDAO).count("example@tetsuwan-tech.com", "username");
		assertEquals(1L, count);
	}

	@Test
	void testSearch() {

		when(genericDAO.search(anyString(), anyString(), anyString(), any(String[].class))).thenThrow(SearchException.class);

		assertThrows(SearchException.class, () -> {
			subjectServiceImpl.search("example@tetsuwan-tech.com", "username", "givenName", new String[] { "givenName" });
		});

		verify(genericDAO).search("example@tetsuwan-tech.com", "username", "givenName", new String[] { "givenName" });
	}

}
