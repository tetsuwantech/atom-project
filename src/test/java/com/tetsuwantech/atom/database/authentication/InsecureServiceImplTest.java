package com.tetsuwantech.atom.database.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 - 2020 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.atMostOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.tetsuwantech.atom.database.GenericDAO;

@ExtendWith(MockitoExtension.class)
class InsecureServiceImplTest {

	@Mock
	private GenericDAO<InsecureEntity, Object> genericDAO;

	@InjectMocks
	private InsecureTestServiceImpl insecureTestServiceImpl;

	@Test
	void testSave() {

		InsecureEntity insecureEntity = new InsecureEntity(1L);
		when(genericDAO.find(1L)).thenReturn(insecureEntity);

		assertThrows(NotSecurableException.class, () -> {
			insecureTestServiceImpl.save(insecureEntity);
		});

		verify(genericDAO, atMostOnce()).find(1L);
		verify(genericDAO, never()).persistOrMerge(insecureEntity);
	}

	@Test
	void testGet_EntityExists() {

		InsecureEntity exampleEntity = new InsecureEntity(1L);
		when(genericDAO.find(1L)).thenReturn(exampleEntity);

		assertThrows(NotSecurableException.class, () -> {
			insecureTestServiceImpl.get(1L);
		});

		verify(genericDAO, atMostOnce()).find(1L);
	}

	@Test
	void testRemove() {

		InsecureEntity exampleEntity = new InsecureEntity(1L);
		when(genericDAO.find(1L)).thenReturn(exampleEntity);

		assertThrows(NotSecurableException.class, () -> {
			insecureTestServiceImpl.get(1L);
		});

		verify(genericDAO, atMostOnce()).find(1L);
		verify(genericDAO, never()).remove(exampleEntity);

	}

}
