package com.tetsuwantech.atom.database;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 - 2020 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.atMostOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class GenericServiceImplTest {

	@Mock
	private GenericDAO<TestEntity, String> genericDAO;

	@InjectMocks
	private TestService testService;

	@Test
	void testCreate_EntityValid() {

		TestEntity testEntity = new TestEntity();

		testService.create(testEntity);

		verify(genericDAO, atMostOnce()).persistOrMerge(testEntity);

	}

	@Test
	void testCreate_EntityInvalid() {

		TestEntity testEntity = new TestEntity(1L);

		assertThrows(CreateNotAllowedException.class, () -> testService.create(testEntity));

		verify(genericDAO, never()).persistOrMerge(testEntity);

	}

	@Test
	void testSave_EntityValid() {

		TestEntity testEntity = new TestEntity(1L);

		testService.save(testEntity);

		verify(genericDAO, atMostOnce()).persistOrMerge(testEntity);

	}

	@Test
	void testSave_EntityInvalid() {

		TestEntity testEntity = new TestEntity();

		assertThrows(SaveNotAllowedException.class, () -> testService.save(testEntity));

		verify(genericDAO, never()).persistOrMerge(testEntity);

	}

	@Test
	void testGet() {

		ArgumentCaptor<Long> argumentCapture = ArgumentCaptor.forClass(Long.class);
		when(genericDAO.find(argumentCapture.capture())).then(invocation -> new TestEntity((Long) invocation.getArgument(0)));

		TestEntity result = testService.get(1L);

		verify(genericDAO, atMostOnce()).find(argumentCapture.getValue());
		assertEquals(1L, result.getId());
	}

	@Test
	void testGet_NotFound() {

		ArgumentCaptor<Long> argumentCapture = ArgumentCaptor.forClass(Long.class);
		when(genericDAO.find(argumentCapture.capture())).then(invocation -> null);

		assertThrows(EntityNotFoundException.class, () -> testService.get(1L));

		verify(genericDAO, atMostOnce()).find(argumentCapture.getValue());
	}

	@Test
	void testRemove_Exists() {

		ArgumentCaptor<Long> argumentCapture = ArgumentCaptor.forClass(Long.class);
		when(genericDAO.find(argumentCapture.capture())).then(invocation -> new TestEntity((Long) invocation.getArgument(0)));

		testService.remove(1L);

		verify(genericDAO, atMostOnce()).find(argumentCapture.getValue());
		verify(genericDAO, atMostOnce()).remove(null);

	}

	@Test
	void testRemove_NotDeletable() {

		ArgumentCaptor<Long> argumentCapture = ArgumentCaptor.forClass(Long.class);
		when(genericDAO.find(argumentCapture.capture())).then(invocation -> {
			TestEntity testEntity = new TestEntity((Long) invocation.getArgument(0));
			testEntity.setDeletable(false);
			return testEntity;
		});

		assertThrows(NotDeletableException.class, () -> testService.remove(1L));

		verify(genericDAO, atMostOnce()).find(argumentCapture.getValue());
		verify(genericDAO, never()).remove(null);
	}

	@Test
	void testFindAll() {

		TestEntity testEntity = new TestEntity(1L);
		testEntity.setText("text");
		when(genericDAO.findAll(null, null, null)).thenReturn(List.of(testEntity));

		List<TestEntity> results = testService.findAll();

		verify(genericDAO, atMostOnce()).findAll(null, null, null);
		assertEquals(List.of(testEntity), results);

	}

	@Test
	void testFindAllK() {

		TestEntity testEntity = new TestEntity(1L);
		testEntity.setText("text");
		when(genericDAO.findAll("text", null, null)).thenReturn(List.of(testEntity));

		List<TestEntity> results = testService.findAll("text");

		verify(genericDAO, atMostOnce()).findAll("text", null, null);
		assertEquals(List.of(testEntity), results);

	}

	@Test
	void testFindAllKString() {

		TestEntity testEntity = new TestEntity(1L);
		testEntity.setText("text");
		when(genericDAO.findAll("text", "text", null)).thenReturn(List.of(testEntity));

		List<TestEntity> results = testService.findAll("text", "text");

		verify(genericDAO, atMostOnce()).findAll("text", "text", null);
		assertEquals(List.of(testEntity), results);

	}

	@Test
	void testFindAllKStringString() {

		TestEntity testEntity = new TestEntity(1L);
		testEntity.setText("text");
		when(genericDAO.findAll("text", "text", "id")).thenReturn(List.of(testEntity));

		List<TestEntity> results = testService.findAll("text", "text", "id");

		verify(genericDAO, atMostOnce()).findAll("text", "text", "id");
		assertEquals(List.of(testEntity), results);

	}

	@Test
	void testFindFirstK() {

		TestEntity testEntity = new TestEntity(1L);
		when(genericDAO.findFirst("text", null)).thenReturn(testEntity);

		TestEntity result = testService.findFirst("text");

		verify(genericDAO, atMostOnce()).findFirst("text", null);
		assertEquals(1L, result.getId());

	}

	@Test
	void testFindFirstKString() {

		TestEntity testEntity = new TestEntity(1L);
		when(genericDAO.findFirst("text", "text")).thenReturn(testEntity);

		TestEntity result = testService.findFirst("text", "text");

		verify(genericDAO, atMostOnce()).findFirst("text", "text");
		assertEquals(1L, result.getId());

	}

	@Test
	void testCountK() {

		when(genericDAO.count("text", null)).thenReturn(1L);

		Long result = testService.count("text");

		verify(genericDAO, atMostOnce()).count("text", null);
		assertEquals(1L, result);

	}

	@Test
	void testCountKString() {

		when(genericDAO.count("text", "text")).thenReturn(1L);

		Long result = testService.count("text", "text");

		verify(genericDAO, atMostOnce()).count("text", "text");
		assertEquals(1L, result);

	}

	@Test
	void testSearch() {

		// List<E> entities = genericDAO.search(where, column, text, fields);
		TestEntity testEntity = new TestEntity(1L);
		testEntity.setText("text");
		when(genericDAO.search("text", "text", "text", new String[] { "text" })).thenReturn(List.of(testEntity));

		List<TestEntity> results = testService.search("text", "text", "text", new String[] { "text" });

		verify(genericDAO, atMostOnce()).search("text", "text", "text", new String[] { "text" });
		assertEquals(List.of(testEntity), results);

	}

}
