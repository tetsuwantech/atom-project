package com.tetsuwantech.atom.database;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 - 2020 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class GenericEntityTest {

	static class TestEntity extends GenericEntity {

		private static final long serialVersionUID = 1L;

	}

	@Test
	void testSetId() {

		TestEntity test = new TestEntity();
		test.setId(1L);

		Long result = test.getId();

		assertEquals(1L, result);
	}

	@Test
	void testHashCode_NotNullId() {

		TestEntity test = new TestEntity();
		test.setId(1L);

		int result = test.hashCode();

		assertEquals(32, result);

	}

	@Test
	void testHashCode_NullId() {

		TestEntity test = new TestEntity();

		int result = test.hashCode();

		assertEquals(31, result);

	}

	@Test
	void testEquals_SameObject() {

		TestEntity test = new TestEntity();

		boolean result = test.equals(test);

		assertTrue(result);

	}

	@Test
	void testEquals_ObjectNull() {

		TestEntity test = new TestEntity();

		boolean result = test.equals(null);

		assertFalse(result);

	}

	@Test
	void testEquals_DifferentObject() {

		TestEntity test = new TestEntity();

		boolean result = test.equals(new Object());

		assertFalse(result);

	}

	@Test
	void testEquals_ThisIdNull() {

		TestEntity test1 = new TestEntity();
		TestEntity test2 = new TestEntity();
		test2.setId(1L);

		boolean result = test1.equals(test2);

		assertFalse(result);

	}

	@Test
	void testEquals_BothIdNull() {

		TestEntity test1 = new TestEntity();
		TestEntity test2 = new TestEntity();

		boolean result = test1.equals(test2);

		assertTrue(result);

	}

	@Test
	void testEquals_IdsDifferent() {

		TestEntity test1 = new TestEntity();
		test1.setId(1L);
		TestEntity test2 = new TestEntity();
		test2.setId(2L);

		boolean result = test1.equals(test2);

		assertFalse(result);

	}

	@Test
	void testEquals_IdsSame() {

		TestEntity test1 = new TestEntity();
		test1.setId(1L);
		TestEntity test2 = new TestEntity();
		test2.setId(1L);

		boolean result = test1.equals(test2);

		assertTrue(result);

	}

	@Test
	void testToString() {

		TestEntity test = new TestEntity();
		test.setId(1L);

		String result = test.toString();

		assertEquals("TestEntity [id=1]", result);

	}

}
