package com.tetsuwantech.atom.database;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 - 2020 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.atMostOnce;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class GenericDAOTest {

	@Mock
	private EntityManager entityManager;

	@Mock
	private CriteriaBuilder criteriaBuilder;

	@Mock
	private CriteriaQuery<Long> criteriaQuery;

	@Mock
	private Root<TestEntity> rootQuery;

	@Mock
	private TypedQuery<Long> query;

	@Mock
	private Metamodel metamodel;

	@Mock
	private EntityType<TestEntity> entityType;

	@InjectMocks
	private TestDAO testDAO;

	@Test
	void testPersist() {

		ArgumentCaptor<TestEntity> argumentCapture = ArgumentCaptor.forClass(TestEntity.class);
		doAnswer(invocation -> {
			TestEntity testEntity = invocation.getArgument(0);

			assertNull(testEntity.getId());

			testEntity.setId(1L);
			return null;

		}).when(entityManager).persist(argumentCapture.capture());

		TestEntity testEntity = new TestEntity("text");

		TestEntity result = testDAO.persistOrMerge(testEntity);

		verify(entityManager, atMostOnce()).persist(argumentCapture.getValue());
		verify(entityManager, never()).merge(argumentCapture.getValue());

		assertEquals(1L, result.getId());
		assertEquals("text", result.getText());
	}

	@Test
	void testMerge() {

		ArgumentCaptor<TestEntity> argumentCapture = ArgumentCaptor.forClass(TestEntity.class);
		when(entityManager.merge(argumentCapture.capture())).then(invocation -> {

			TestEntity testEntity = invocation.getArgument(0);
			assertNotNull(testEntity.getId());

			return testEntity;
		});

		TestEntity testEntity = new TestEntity(1L);
		testEntity.setText("test");

		TestEntity result = testDAO.persistOrMerge(testEntity);

		verify(entityManager, never()).persist(argumentCapture.getValue());
		verify(entityManager, atMostOnce()).merge(argumentCapture.getValue());

		assertEquals(1L, result.getId());
		assertEquals("test", result.getText());

	}

	@Test
	void testFind_withId() {

		TestEntity testEntity = new TestEntity(1L);
		when(entityManager.find(TestEntity.class, 1L)).thenReturn(testEntity);

		TestEntity result = testDAO.find(1L);

		verify(entityManager, atMostOnce()).find(TestEntity.class, 1L);
		assertEquals(testEntity, result);
	}

	@Test
	void testFind_withNull() {

		TestEntity result = testDAO.find(null);

		verify(entityManager, never()).find(TestEntity.class, 1L);
		assertNull(result);
	}

	@Test
	void testRemove() {

		TestEntity testEntity = new TestEntity(1L);

		testDAO.remove(testEntity);

		verify(entityManager, atMostOnce()).remove(testEntity);
	}

	@Test
	void testCountOne() {

		when(entityManager.getCriteriaBuilder()).thenReturn(criteriaBuilder);
		when(criteriaBuilder.createQuery(Long.class)).thenReturn(criteriaQuery);
		when(criteriaQuery.from(TestEntity.class)).thenReturn(rootQuery);
		when(entityManager.getMetamodel()).thenReturn(metamodel);
		when(metamodel.entity(TestEntity.class)).thenReturn(entityType);
		when(entityManager.createQuery(criteriaQuery)).thenReturn(query);
		when(query.getSingleResult()).thenReturn(1L);

		Long result = testDAO.count("text", "text");

		verify(entityManager, atMostOnce()).getCriteriaBuilder();
		verify(criteriaBuilder, atMostOnce()).createQuery(Long.class);
		verify(criteriaQuery, atMostOnce()).from(TestEntity.class);
		verify(entityManager, atMostOnce()).createQuery(criteriaQuery);
		verify(query, atMostOnce()).getSingleResult();

		assertEquals(1L, result);

	}

	@Test
	void testCountAll() {

		when(entityManager.getCriteriaBuilder()).thenReturn(criteriaBuilder);
		when(criteriaBuilder.createQuery(Long.class)).thenReturn(criteriaQuery);
		when(criteriaQuery.from(TestEntity.class)).thenReturn(rootQuery);
		when(entityManager.createQuery(criteriaQuery)).thenReturn(query);
		when(query.getSingleResult()).thenReturn(1L);

		Long result = testDAO.count(null, null);

		verify(entityManager, atMostOnce()).getCriteriaBuilder();
		verify(criteriaBuilder, atMostOnce()).createQuery(Long.class);
		verify(criteriaQuery, atMostOnce()).from(TestEntity.class);
		verify(entityManager, atMostOnce()).createQuery(criteriaQuery);
		verify(query, atMostOnce()).getSingleResult();

		assertEquals(1L, result);

	}

}
