package com.tetsuwantech.atom.web;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 - 2020 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.tetsuwantech.atom.database.TestEntity;
import com.tetsuwantech.atom.manager.TestModel;

class DeleteModelTest {

	@Test
	void testDeleteModelGenericEntity() {

		TestEntity entity = new TestEntity(1L);

		DeleteModel result = new TestModel(entity);

		assertEquals(1L, result.getId());
	}

	@Test
	void testDeleteModelLong() {

		TestModel result = new TestModel(1L);

		assertEquals(1L, result.getId());
	}

	@Test
	void testGetDelete() {

		TestModel result = new TestModel(1L);

		assertEquals(false, result.getDelete());
	}

	@Test
	void testSetDelete() {

		TestModel result = new TestModel(1L);
		result.setDelete(true);

		assertEquals(true, result.getDelete());
	}

	@Test
	void testGetDeletable() {

		TestModel result = new TestModel(1L);

		assertEquals(true, result.getDeletable());
	}

	@Test
	void testSetDeletable() {

		TestModel result = new TestModel(1L);
		result.setDeletable(false);

		assertEquals(false, result.getDeletable());
	}
}
