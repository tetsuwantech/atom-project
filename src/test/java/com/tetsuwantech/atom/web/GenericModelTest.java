package com.tetsuwantech.atom.web;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 - 2020 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import com.tetsuwantech.atom.database.TestEntity;
import com.tetsuwantech.atom.manager.TestModel;

class GenericModelTest {

	@Test
	void testGenericModel_GenericEntityNotNull() {

		TestEntity testEntity = new TestEntity(1L);

		TestModel result = new TestModel(testEntity);

		assertEquals(1L, result.getId());

	}

	@Test
	void testSetId() {

		TestModel test = new TestModel(1L);

		Long result = test.getId();

		assertEquals(1L, result);
	}

	@Test
	void testHashCode_NotNullId() {

		TestModel test = new TestModel(1L);

		int result = test.hashCode();

		assertEquals(204, result);

	}

	@Test
	void testHashCode_NullId() {

		TestModel test = new TestModel();

		int result = test.hashCode();

		assertEquals(203, result);

	}

	@Test
	void testEquals_SameObject() {

		TestModel test = new TestModel();

		boolean result = test.equals(test);

		assertTrue(result);

	}

	@Test
	void testEquals_ObjectNull() {

		TestModel test = new TestModel();

		boolean result = test.equals(null);

		assertFalse(result);

	}

	@Test
	void testEquals_DifferentObject() {

		TestModel test = new TestModel();

		boolean result = test.equals(new Object());

		assertFalse(result);

	}

	@Test
	void testEquals_ThisIdNull() {

		TestModel test1 = new TestModel();
		TestModel test2 = new TestModel(2L);

		boolean result = test1.equals(test2);

		assertFalse(result);

	}

	@Test
	void testEquals_BothIdNull() {

		TestModel test1 = new TestModel();
		TestModel test2 = new TestModel();

		boolean result = test1.equals(test2);

		assertTrue(result);

	}

	@Test
	void testEquals_IdsDifferent() {

		TestModel test1 = new TestModel(1L);
		TestModel test2 = new TestModel(2L);

		boolean result = test1.equals(test2);

		assertFalse(result);

	}

	@Test
	void testEquals_IdsSame() {

		TestModel test1 = new TestModel(1L);
		TestModel test2 = new TestModel(1L);

		boolean result = test1.equals(test2);

		assertTrue(result);

	}

	@Test
	void testToString() {

		TestModel test = new TestModel(1L);

		String result = test.toString();

		assertEquals("TestModel [id=1]", result);

	}

}
