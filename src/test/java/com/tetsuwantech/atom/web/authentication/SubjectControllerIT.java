package com.tetsuwantech.atom.web.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.hamcrest.Matchers.hasItemInArray;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.oauth2Login;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;
import static org.springframework.test.jdbc.JdbcTestUtils.countRowsInTableWhere;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.sql.DataSource;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetupTest;
import com.tetsuwantech.atom.util.authentication.SubjectUtils;

@SpringJUnitWebConfig(locations = "classpath:spring-context.xml")
class SubjectControllerIT {

	@Inject
	private WebApplicationContext webApplicationContext;

	@Inject
	private DataSource dataSource;

	private JdbcTemplate jdbcTemplate;

	private MockMvc mockMvc;

	private GreenMail greenMail;

	@BeforeEach
	void setUp() {
		mockMvc = webAppContextSetup(webApplicationContext).apply(springSecurity()).build();

		greenMail = new GreenMail(ServerSetupTest.SMTP);
		greenMail.setUser("example@tetsuwan-tech.com", "Example235!");
		greenMail.start();

		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@AfterEach
	void tearDown() {
		greenMail.stop();
		JdbcTestUtils.deleteFromTables(jdbcTemplate, "audit", "subject_authority", "subject");
	}

	// GET /subject
	@WithAnonymousUser
	@Test
	void testMain() {
		try {
			mockMvc.perform(get("/subject")).andExpect(status().isOk()).andExpect(view().name("/subjects/login"));
		} catch (Exception exception) {
			fail(exception);
		}
	}

	// GET /subject/forgot
	@WithAnonymousUser
	@Test
	void testForgot() {
		try {
			mockMvc.perform(get("/subject/forgot")).andExpect(status().isOk())
					.andExpect(model().attribute("message", "subjects.forgot.edit")).andExpect(view().name("/subjects/forgot"));
		} catch (Exception exception) {
			fail(exception);
		}
	}

	// POST /subject/forgot
	// username=example@tetsuwan-tech.com
	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, uuid, uuid_expires, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', '9563a95d-1466-42fb-b6eb-eb87ca8ea82a', DATEADD('DAY', 1, CURRENT_TIMESTAMP()), 'ENABLED')")
	@Sql(statements = "INSERT INTO subject_authority (authority, subject_id) VALUES ('ROLE_USER', (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com'))")
	@WithAnonymousUser
	@Test
	void testForgot_UsernameIsValid() {
		try {

			mockMvc.perform(post("/subject/forgot").with(csrf()).param("username", "example@tetsuwan-tech.com"))
					.andExpect(status().is3xxRedirection()).andExpect(redirectedUrl("/subject"))
					.andExpect(flash().attribute("message", "subjects.forgot.send"))
					.andExpect(flash().attribute("args", hasItemInArray("example@tetsuwan-tech.com")))
					.andExpect(redirectedUrl("/subject"));

			// check email got sent, no async queues configured
			MimeMessage[] messages = greenMail.getReceivedMessages();
			assertNotNull(messages);
			assertEquals(1, messages.length);

			/*
			 * Content-Type: multipart/mixed; boundary="----=_Part_0_1886330374.1543406485409"
			 *
			 * ------=_Part_0_1886330374.1543406485409 Content-Type: multipart/related; boundary="----=_Part_1_851931109.1543406485450"
			 *
			 * ------=_Part_1_851931109.1543406485450 Content-Type: text/html; charset=us-ascii Content-Transfer-Encoding: 7bit
			 *
			 * <p>forgot password email</p>
			 *
			 * <p>url = https://tetsuwan-tech.com/subject/password?uuid=9563a95d-1466-42fb- b6eb- eb87ca8ea82a</p>
			 *
			 * -----=_Part_1_851931109.1543406485450--
			 *
			 * ------=_Part_0_1886330374.1543406485409--
			 */

			Message message = messages[0];
			assertTrue(message.isMimeType("multipart/mixed"));

			MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
			assertEquals(1, mimeMultipart.getCount());

			BodyPart bodyPart = mimeMultipart.getBodyPart(0);
			assertTrue(bodyPart.isMimeType("multipart/related"));

			MimeMultipart htmlMimeBodyPart = (MimeMultipart) bodyPart.getContent();
			assertEquals(1, htmlMimeBodyPart.getCount());

			BodyPart htmlBodyPart = htmlMimeBodyPart.getBodyPart(0);
			assertTrue(htmlBodyPart.isMimeType("text/html"));

			String content = (String) htmlBodyPart.getContent();

			Pattern pattern = Pattern.compile("<p>givenName = ([^<]+?)</p>");
			Matcher matcher = pattern.matcher(content);

			assertTrue(matcher.find());
			assertEquals(1, matcher.groupCount());

			String givenName = matcher.group(1);
			assertEquals("Example", givenName);

			pattern = Pattern.compile("<p>familyName = ([^<]+?)</p>");
			matcher = pattern.matcher(content);

			assertTrue(matcher.find());
			assertEquals(1, matcher.groupCount());

			String familyName = matcher.group(1);
			assertEquals("Application", familyName);

			pattern = Pattern.compile(
					"\\/subject\\/password\\?uuid=([0-9a-fA-F]{8}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{12})");
			matcher = pattern.matcher(content);

			assertTrue(matcher.find());
			assertEquals(1, matcher.groupCount());

			String uuid = matcher.group(1);
			assertEquals(1, countRowsInTableWhere(jdbcTemplate, "subject", "uuid = '" + uuid + "'"));

		} catch (Exception exception) {
			fail(exception);
		}
	}

	// POST /subject/forgot
	// username=idonotexist@tetsuwan-tech.com
	@WithAnonymousUser
	@Test
	void testForgot_UsernameIsUnknown() {
		try {
			mockMvc.perform(post("/subject/forgot").with(csrf()).param("username", "idonotexist@tetsuwan-tech.com"))
					.andExpect(status().isOk()).andExpect(model().attribute("message", "errors"))
					.andExpect(model().attribute("type", "danger"))
					.andExpect(model().attributeHasFieldErrorCode("username", "username", "Forgot.notFound"))
					.andExpect(view().name("/subjects/forgot"));

			// check email did not get sent, no async queues configured
			MimeMessage[] messages = greenMail.getReceivedMessages();
			assertNotNull(messages);
			assertEquals(0, messages.length);

		} catch (Exception exception) {
			fail(exception);
		}
	}

	// POST /subject/forgot
	// username=example@tetsuwan-tech.com
	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, uuid, uuid_expires, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', '9563a95d-1466-42fb-b6eb-eb87ca8ea82a', DATEADD('DAY', 1, CURRENT_TIMESTAMP()), 'PENDING')")
	@WithAnonymousUser
	@Test
	void testForgot_StatusIsPending() {
		try {
			mockMvc.perform(post("/subject/forgot").with(csrf()).param("username", "example@tetsuwan-tech.com"))
					.andExpect(status().isOk()).andExpect(model().attribute("message", "errors"))
					.andExpect(model().attribute("type", "danger"))
					.andExpect(model().attributeHasFieldErrorCode("username", "username", "subjects.login.locked"))
					.andExpect(view().name("/subjects/forgot"));

			// check email did not get sent, no async queues configured
			MimeMessage[] messages = greenMail.getReceivedMessages();
			assertNotNull(messages);
			assertEquals(0, messages.length);

		} catch (Exception exception) {
			fail(exception);
		}
	}

	// GET /subject/password?uuid=9563a95d-1466-42fb-b6eb-eb87ca8ea82a
	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, uuid, uuid_expires, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', '9563a95d-1466-42fb-b6eb-eb87ca8ea82a', DATEADD('DAY', -1, CURRENT_TIMESTAMP()), 'ENABLED')")
	@Sql(statements = "INSERT INTO subject_authority (authority, subject_id) VALUES ('ROLE_USER', (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com'))")
	@WithAnonymousUser
	@Test
	void testForgot_UuidExpired() {
		try {
			mockMvc.perform(get("/subject/password").param("uuid", "9563a95d-1466-42fb-b6eb-eb87ca8ea82a"))
					.andExpect(status().isOk()).andExpect(model().attribute("message", "errors"))
					.andExpect(model().attribute("type", "danger"))
					.andExpect(model().attributeHasFieldErrorCode("uuid", "uuid", "Uuid.expired"))
					.andExpect(view().name("/subjects/login"));
		} catch (Exception exception) {
			fail(exception);
		}
	}

	// GET /subject/password?uuid=9563a95d-1466-42fb-b6eb-eb87ca8ea82a
	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, uuid, uuid_expires, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', '9563a95d-1466-42fb-b6eb-eb87ca8ea82a', DATEADD('DAY', 1, CURRENT_TIMESTAMP()), 'ENABLED')")
	@Sql(statements = "INSERT INTO subject_authority (authority, subject_id) VALUES ('ROLE_USER', (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com'))")
	@WithAnonymousUser
	@Test
	void testPassword_UuidIsInvalid() {
		try {
			mockMvc.perform(get("/subject/password").param("uuid", "this_is_not_a_real_uuid")).andExpect(status().isOk())
					.andExpect(model().attribute("message", "errors")).andExpect(model().attribute("type", "danger"))
					.andExpect(view().name("/subjects/login"));
		} catch (Exception exception) {
			fail(exception);
		}
	}

	// GET /subject/password?uuid=9563a95d-1466-42fb-b6eb-eb87ca8ea82a
	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, uuid, uuid_expires, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', '9563a95d-1466-42fb-b6eb-eb87ca8ea82a', DATEADD('DAY', 1, CURRENT_TIMESTAMP()), 'ENABLED')")
	@Sql(statements = "INSERT INTO subject_authority (authority, subject_id) VALUES ('ROLE_USER', (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com'))")
	@WithAnonymousUser
	@Test
	void testPassword_UuidIsValid() {
		try {
			mockMvc.perform(get("/subject/password").param("uuid", "9563a95d-1466-42fb-b6eb-eb87ca8ea82a"))
					.andExpect(status().isOk()).andExpect(model().attribute("message", "subjects.password.edit"))
					.andExpect(view().name("/subjects/password"));
		} catch (Exception exception) {
			fail(exception);
		}
	}

	// POST /subject/password
	// uuid=this_is_not_a_real_uuid&password1=Example235!&password2=Example235!
	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, uuid, uuid_expires, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', '9563a95d-1466-42fb-b6eb-eb87ca8ea82a', DATEADD('DAY', 1, CURRENT_TIMESTAMP()), 'ENABLED')")
	@Sql(statements = "INSERT INTO subject_authority (authority, subject_id) VALUES ('ROLE_USER', (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com'))")
	@WithAnonymousUser
	@Test
	void testPasswordPost_UuidIsInvalid() {
		try {
			mockMvc.perform(post("/subject/password").with(csrf()).param("uuid", "this_is_not_a_real_uuid")
					.param("password1", "Example235!").param("password2", "Example235!")).andExpect(status().isOk())
					.andExpect(model().attribute("message", "errors")).andExpect(model().attribute("type", "danger"))
					.andExpect(model().attributeHasFieldErrorCode("uuid", "uuid", "Subject.notFound"))
					.andExpect(unauthenticated()).andExpect(view().name("/subjects/password"));
		} catch (Exception exception) {
			fail(exception);
		}
	}

	// POST /subject/password
	// uuid=9563a95d-1466-42fb-b6eb-eb87ca8ea82a&password1=Example235!&password2=Example235!
	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, uuid, uuid_expires, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', '9563a95d-1466-42fb-b6eb-eb87ca8ea82a', DATEADD('DAY', -1, CURRENT_TIMESTAMP()), 'ENABLED')")
	@Sql(statements = "INSERT INTO subject_authority (authority, subject_id) VALUES ('ROLE_USER', (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com'))")
	@WithAnonymousUser
	@Test
	void testPasswordPost_UuidIsExpired() {
		try {
			mockMvc.perform(post("/subject/password").with(csrf()).param("uuid", "9563a95d-1466-42fb-b6eb-eb87ca8ea82a")
					.param("password1", "Example235!").param("password2", "Example235!")).andExpect(status().isOk())
					.andExpect(model().attribute("message", "errors")).andExpect(model().attribute("type", "danger"))
					.andExpect(model().attributeHasFieldErrorCode("uuid", "uuid", "Uuid.expired")).andExpect(unauthenticated())
					.andExpect(view().name("/subjects/password"));
		} catch (Exception exception) {
			fail(exception);
		}
	}

	// POST /subject/password
	// uuid=9563a95d-1466-42fb-b6eb-eb87ca8ea82a&password1=Example235!&password2=Example235!
	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, uuid, uuid_expires, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', '9563a95d-1466-42fb-b6eb-eb87ca8ea82a', DATEADD('DAY', 1, CURRENT_TIMESTAMP()), 'ENABLED')")
	@Sql(statements = "INSERT INTO subject_authority (authority, subject_id) VALUES ('ROLE_USER', (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com'))")
	@WithAnonymousUser
	@Test
	void testPasswordPost_SubjectIsValid() {
		try {
			mockMvc.perform(post("/subject/password").with(csrf()).param("uuid", "9563a95d-1466-42fb-b6eb-eb87ca8ea82a")
					.param("password1", "Example235!").param("password2", "Example235!"))
					.andExpect(status().is3xxRedirection()).andExpect(redirectedUrl("/atom"))
					.andExpect(flash().attribute("message", "subjects.password.save"))
					.andExpect(authenticated().withUsername("example@tetsuwan-tech.com").withRoles("USER"));
		} catch (Exception exception) {
			fail(exception);
		}
	}

	// POST /subject/password
	// uuid=9563a95d-1466-42fb-b6eb-eb87ca8ea82a&password1=Example235!&password2=Example235!
	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, uuid, uuid_expires, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', DATEADD('DAY', -2, CURRENT_TIMESTAMP()), DATEADD('DAY', -1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', '9563a95d-1466-42fb-b6eb-eb87ca8ea82a', DATEADD('DAY', 1, CURRENT_TIMESTAMP()), 'ENABLED')")
	@Sql(statements = "INSERT INTO subject_authority (authority, subject_id) VALUES ('ROLE_USER', (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com'))")
	@WithAnonymousUser
	@Test
	void testPasswordPost_PasswordIsExpired() {
		try {
			mockMvc.perform(post("/subject/password").with(csrf()).param("uuid", "9563a95d-1466-42fb-b6eb-eb87ca8ea82a")
					.param("password1", "Example235!").param("password2", "Example235!")).andExpect(status().is3xxRedirection())
					.andExpect(flash().attribute("message", "subjects.password.save")).andExpect(redirectedUrl("/atom"))
					.andExpect(authenticated().withUsername("example@tetsuwan-tech.com").withRoles("USER"));
		} catch (Exception exception) {
			fail(exception);
		}
	}

	// POST /subject/password
	// uuid=9563a95d-1466-42fb-b6eb-eb87ca8ea82a&password1=Example235!&password2=Example235!
	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, uuid, uuid_expires, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', DATEADD('DAY', -2, CURRENT_TIMESTAMP()), DATEADD('DAY', -1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', '9563a95d-1466-42fb-b6eb-eb87ca8ea82a', DATEADD('DAY', 1, CURRENT_TIMESTAMP()), 'ENABLED')")
	@Sql(statements = "INSERT INTO subject_authority (authority, subject_id) VALUES ('ROLE_USER', (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com'))")
	@WithAnonymousUser
	@Test
	void testPasswordPost_PasswordIsExpiredAndSameAsBefore() {
		try {
			mockMvc.perform(post("/subject/password").with(csrf()).param("uuid", "9563a95d-1466-42fb-b6eb-eb87ca8ea82a")
					.param("password1", "Example235!").param("password2", "Example235!")).andExpect(status().is3xxRedirection())
					.andExpect(flash().attribute("message", "subjects.password.save")).andExpect(redirectedUrl("/atom"))
					.andExpect(authenticated().withUsername("example@tetsuwan-tech.com").withRoles("USER"));
		} catch (Exception exception) {
			fail(exception);
		}
	}

	// POST /subject/save
	// username=example@tetsuwan-tech.com&password1=Example235!&password2=Example235! etc ...
	@WithAnonymousUser
	@Test
	void testSave_RegistrationIsValid() {
		try {

			mockMvc.perform(post("/subject/save").with(csrf()).param("timeZone", "Australia/Sydney")
					.param("username.username", "example@tetsuwan-tech.com").param("password.password1", "Example235!")
					.param("password.password2", "Example235!").param("name.givenName", "Example")
					.param("name.familyName", "Application").param("human", "A").param("answer", "2")
					.param("termsAndConditions", "true")).andExpect(status().isOk())
					.andExpect(model().attribute("message", "subjects.edit.send"))
					.andExpect(model().attribute("args", hasItemInArray("example@tetsuwan-tech.com")))
					.andExpect(view().name("/subjects/login"));

			// check email got sent, no async queues configured
			MimeMessage[] messages = greenMail.getReceivedMessages();
			assertNotNull(messages);
			assertEquals(1, messages.length);

			/*
			 * Content-Type: multipart/mixed; boundary="----=_Part_0_1886330374.1543406485409"
			 *
			 * ------=_Part_0_1886330374.1543406485409 Content-Type: multipart/related; boundary="----=_Part_1_851931109.1543406485450"
			 *
			 * ------=_Part_1_851931109.1543406485450 Content-Type: text/html; charset=us-ascii Content-Transfer-Encoding: 7bit
			 *
			 * <p>create account email</p>
			 *
			 * <p>url = https://tetsuwan-tech.com/subject/activate?uuid=9563a95d-1466-42fb-b6eb- eb87ca8ea82a&enabled=false</p>
			 *
			 * -----=_Part_1_851931109.1543406485450--
			 *
			 * ------=_Part_0_1886330374.1543406485409--
			 */

			Message message = messages[0];
			assertTrue(message.isMimeType("multipart/mixed"));

			MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
			assertEquals(1, mimeMultipart.getCount());

			BodyPart bodyPart = mimeMultipart.getBodyPart(0);
			assertTrue(bodyPart.isMimeType("multipart/related"));

			MimeMultipart htmlMimeBodyPart = (MimeMultipart) bodyPart.getContent();
			assertEquals(1, htmlMimeBodyPart.getCount());

			BodyPart htmlBodyPart = htmlMimeBodyPart.getBodyPart(0);
			assertTrue(htmlBodyPart.isMimeType("text/html"));

			String content = (String) htmlBodyPart.getContent();

			Pattern pattern = Pattern.compile("<p>givenName = ([^<]+?)</p>");
			Matcher matcher = pattern.matcher(content);

			assertTrue(matcher.find());
			assertEquals(1, matcher.groupCount());

			String givenName = matcher.group(1);
			assertEquals("Example", givenName);

			pattern = Pattern.compile("<p>familyName = ([^<]+?)</p>");
			matcher = pattern.matcher(content);

			assertTrue(matcher.find());
			assertEquals(1, matcher.groupCount());

			String familyName = matcher.group(1);
			assertEquals("Application", familyName);

			pattern = Pattern.compile(
					"\\/subject\\/activate\\?uuid=([0-9a-fA-F]{8}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{12})&(enabled=false)");
			matcher = pattern.matcher(content);

			assertTrue(matcher.find());
			assertEquals(2, matcher.groupCount());

			String uuid = matcher.group(1);
			assertEquals(1, countRowsInTableWhere(jdbcTemplate, "subject",
					"username = 'example@tetsuwan-tech.com' AND status = 'PENDING' and uuid = '" + uuid + "'"));

		} catch (Exception exception) {
			fail(exception);
		}
	}

	// POST /subject/save
	// username=example@tetsuwan-tech.com
	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, uuid, uuid_expires, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', DATEADD('DAY', -2, CURRENT_TIMESTAMP()), DATEADD('DAY', -1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', '9563a95d-1466-42fb-b6eb-eb87ca8ea82a', DATEADD('DAY', 1, CURRENT_TIMESTAMP()), 'ENABLED')")
	@Sql(statements = "INSERT INTO subject_authority (authority, subject_id) VALUES ('ROLE_USER', (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com'))")
	@WithAnonymousUser
	@Test
	void testSave_UsernameIsUsed() {
		try {

			mockMvc.perform(post("/subject/save").with(csrf()).param("timeZone", "Australia/Sydney")
					.param("username.username", "example@tetsuwan-tech.com").param("password.password1", "Example235!")
					.param("password.password2", "Example235!").param("name.givenName", "givenName")
					.param("name.familyName", "familyName").param("human", "A").param("answer", "2")
					.param("termsAndConditions", "true")).andExpect(status().isOk())
					.andExpect(model().attribute("message", "errors")).andExpect(model().attribute("type", "danger"))
					.andExpect(model().attributeHasFieldErrorCode("register", "username.username", "Subject.UsernameUsed"))
					.andExpect(view().name("/subjects/login"));

			// check email got sent, no async queues configured
			MimeMessage[] messages = greenMail.getReceivedMessages();
			assertNotNull(messages);
			assertEquals(0, messages.length);

			assertEquals(0, countRowsInTableWhere(jdbcTemplate, "subject",
					"username = 'example@tetsuwan-tech.com' AND status = 'PENDING'"));

		} catch (Exception exception) {
			fail(exception);
		}
	}

	// POST /subject/save
	// username=example@notavaliddomain.com
	@WithAnonymousUser
	@Test
	void testSave_UsernameIsInvalid() {
		try {

			mockMvc.perform(post("/subject/save").with(csrf()).param("timeZone", "Australia/Sydney")
					.param("username.username", "example@hotmail.com").param("password.password1", "Example235!")
					.param("password.password2", "Example235!").param("name.givenName", "Example")
					.param("name.familyName", "Application").param("human", "A").param("answer", "2")
					.param("termsAndConditions", "true")).andExpect(status().isOk())
					.andExpect(model().attribute("message", "errors")).andExpect(model().attribute("type", "danger"))
					.andExpect(model().attributeHasFieldErrorCode("register", "username.username", "Username.invalidDomain"))
					.andExpect(view().name("/subjects/login"));

			// check no email get sent
			MimeMessage[] messages = greenMail.getReceivedMessages();
			assertNotNull(messages);
			assertEquals(0, messages.length);

			assertEquals(0, countRowsInTableWhere(jdbcTemplate, "subject",
					"username = 'example@hotmail.com' AND status = 'PENDING'"));

		} catch (Exception exception) {
			fail(exception);
		}
	}

	// GET /subject/activate?uuid=9563a95d-1466-42fb-b6eb-eb87ca8ea82a&enabled=false
	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, uuid, uuid_expires, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', '9563a95d-1466-42fb-b6eb-eb87ca8ea82a', DATEADD('DAY', 1, CURRENT_TIMESTAMP()), 'PENDING')")
	@WithAnonymousUser
	@Test
	void testActivate_UuidIsValid() {
		try {

			mockMvc.perform(
					get("/subject/activate").param("uuid", "9563a95d-1466-42fb-b6eb-eb87ca8ea82a").param("enabled", "false"))
					.andExpect(status().is3xxRedirection()).andExpect(redirectedUrl("/atom"))
					.andExpect(authenticated().withUsername("example@tetsuwan-tech.com").withRoles("USER"));

			// check email got sent, no async queues configured
			MimeMessage[] messages = greenMail.getReceivedMessages();
			assertNotNull(messages);
			assertEquals(1, messages.length);

			/*
			 * Content-Type: multipart/mixed; boundary="----=_Part_0_1886330374.1543406485409"
			 *
			 * ------=_Part_0_1886330374.1543406485409 Content-Type: multipart/related; boundary="----=_Part_1_851931109.1543406485450"
			 *
			 * ------=_Part_1_851931109.1543406485450 Content-Type: text/html; charset=us-ascii Content-Transfer-Encoding: 7bit
			 *
			 * <html> <head> <meta http-equiv="Content-Type" content="text/html;UTF-8" /> </head> <body> <p><#if
			 * subject.givenName??>givenName = givenName</#if></p> <p><#if subject.familyName??>familyName = familyName</#if></p> <#include
			 * path> </body> </html>
			 *
			 * -----=_Part_1_851931109.1543406485450--
			 *
			 * ------=_Part_0_1886330374.1543406485409--
			 */

			Message message = messages[0];
			assertTrue(message.isMimeType("multipart/mixed"));

			MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
			assertEquals(1, mimeMultipart.getCount());

			BodyPart bodyPart = mimeMultipart.getBodyPart(0);
			assertTrue(bodyPart.isMimeType("multipart/related"));

			MimeMultipart htmlMimeBodyPart = (MimeMultipart) bodyPart.getContent();
			assertEquals(1, htmlMimeBodyPart.getCount());

			BodyPart htmlBodyPart = htmlMimeBodyPart.getBodyPart(0);
			assertTrue(htmlBodyPart.isMimeType("text/html"));

			String content = (String) htmlBodyPart.getContent();

			Pattern pattern = Pattern.compile("<p>givenName = ([^<]+?)</p>");
			Matcher matcher = pattern.matcher(content);

			assertTrue(matcher.find());
			assertEquals(1, matcher.groupCount());

			String givenName = matcher.group(1);
			assertEquals("Example", givenName);

			pattern = Pattern.compile("<p>familyName = ([^<]+?)</p>");
			matcher = pattern.matcher(content);

			assertTrue(matcher.find());
			assertEquals(1, matcher.groupCount());

			String familyName = matcher.group(1);
			assertEquals("Application", familyName);

			assertEquals(1, countRowsInTableWhere(jdbcTemplate, "subject",
					"username = 'example@tetsuwan-tech.com' AND status = 'ENABLED'"));

		} catch (Exception exception) {
			fail(exception);
		}
	}

	// GET /subject/activate?uuid=this_is_not_a_real_uuid&enabled=false
	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, uuid, uuid_expires, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', '9563a95d-1466-42fb-b6eb-eb87ca8ea82a', DATEADD('DAY', 1, CURRENT_TIMESTAMP()), 'PENDING')")
	@WithAnonymousUser
	@Test
	void testActivate_UuidIsInvalid() {
		try {

			mockMvc.perform(get("/subject/activate").param("uuid", "this_is_not_a_real_uuid").param("enabled", "false"))
					.andExpect(status().is3xxRedirection()).andExpect(flash().attribute("message", "errors"))
					.andExpect(flash().attribute("type", "danger")).andExpect(unauthenticated())
					.andExpect(redirectedUrl("/subject"));

			// check email got sent, no async queues configured
			MimeMessage[] messages = greenMail.getReceivedMessages();
			assertNotNull(messages);
			assertEquals(0, messages.length);

		} catch (Exception exception) {
			fail(exception);
		}
	}

	// GET /subject/activate?uuid=9563a95d-1466-42fb-b6eb-eb87ca8ea82a&enabled=false
	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, uuid, uuid_expires, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', '9563a95d-1466-42fb-b6eb-eb87ca8ea82a', DATEADD('DAY', 1, CURRENT_TIMESTAMP()), 'ENABLED')")
	@Sql(statements = "INSERT INTO subject_authority (authority, subject_id) VALUES ('ROLE_USER', (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com'))")
	@WithAnonymousUser
	@Test
	void testActivate_SubjectIsNotPending() {
		try {

			mockMvc.perform(
					get("/subject/activate").param("uuid", "9563a95d-1466-42fb-b6eb-eb87ca8ea82a").param("enabled", "false"))
					.andExpect(status().is3xxRedirection()).andExpect(flash().attribute("message", "errors"))
					.andExpect(flash().attribute("type", "danger")).andExpect(unauthenticated())
					.andExpect(redirectedUrl("/subject"));

			// check no email got sent, no async queues configured
			MimeMessage[] messages = greenMail.getReceivedMessages();
			assertNotNull(messages);
			assertEquals(0, messages.length);

		} catch (Exception exception) {
			fail(exception);
		}
	}

	// GET /subject/activate?uuid=9563a95d-1466-42fb-b6eb-eb87ca8ea82a&enabled=false
	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, uuid, uuid_expires, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', '9563a95d-1466-42fb-b6eb-eb87ca8ea82a', DATEADD('DAY', -1, CURRENT_TIMESTAMP()), 'PENDING')")
	@WithAnonymousUser
	@Test
	void testActivate_UuidIsExpired() {
		try {

			mockMvc.perform(
					get("/subject/activate").param("uuid", "9563a95d-1466-42fb-b6eb-eb87ca8ea82a").param("enabled", "false"))
					.andExpect(status().is3xxRedirection()).andExpect(flash().attribute("message", "errors"))
					.andExpect(flash().attribute("type", "danger")).andExpect(unauthenticated())
					.andExpect(redirectedUrl("/subject"));

			// check email got sent, no async queues configured
			MimeMessage[] messages = greenMail.getReceivedMessages();
			assertNotNull(messages);
			assertEquals(0, messages.length);

		} catch (Exception exception) {
			fail(exception);
		}
	}

	// GET /subject/deactivate?uuid=9563a95d-1466-42fb-b6eb-eb87ca8ea82a
	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, uuid, uuid_expires, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', '9563a95d-1466-42fb-b6eb-eb87ca8ea82a', DATEADD('DAY', 1, CURRENT_TIMESTAMP()), 'ENABLED')")
	@Sql(statements = "INSERT INTO subject_authority (authority, subject_id) VALUES ('ROLE_USER', (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com'))")
	@Sql(executionPhase = AFTER_TEST_METHOD, statements = "UPDATE subject SET username = 'example@tetsuwan-tech.com' WHERE username = '__deleted__9563a95d-1466-42fb-b6eb-eb87ca8ea82a@tetsuwan-tech.com'")
	@WithAnonymousUser
	@Test
	void testDeactivate_SubjectIsValid() {
		try {

			mockMvc.perform(get("/subject/deactivate").param("uuid", "9563a95d-1466-42fb-b6eb-eb87ca8ea82a"))
					.andExpect(status().is3xxRedirection())
					.andExpect(redirectedUrl("/subject?status=deactivate.valid&type=danger")).andExpect(unauthenticated());

			// check email got sent, no async queues configured
			MimeMessage[] messages = greenMail.getReceivedMessages();
			assertNotNull(messages);
			assertEquals(0, messages.length);

			assertEquals(0, countRowsInTableWhere(jdbcTemplate, "subject",
					"username = 'example@tetsuwan-tech.com' AND status = 'ENABLED'"));

			assertEquals(1, countRowsInTableWhere(jdbcTemplate, "subject",
					"username = '__deleted__9563a95d-1466-42fb-b6eb-eb87ca8ea82a@tetsuwan-tech.com' AND status = 'LOCKED'"));

		} catch (Exception exception) {
			fail(exception);
		}
	}

	// GET /subject/deactivate?uuid=this_is_not_a_real_uuid
	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, uuid, uuid_expires, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', '9563a95d-1466-42fb-b6eb-eb87ca8ea82a', DATEADD('DAY', 1, CURRENT_TIMESTAMP()), 'ENABLED')")
	@Sql(statements = "INSERT INTO subject_authority (authority, subject_id) VALUES ('ROLE_USER', (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com'))")
	@WithAnonymousUser
	@Test
	void testDeactivate_UuidIsInvalid() {
		try {

			mockMvc.perform(get("/subject/deactivate").param("uuid", "this_is_not_a_real_uuid"))
					.andExpect(status().is3xxRedirection()).andExpect(flash().attribute("message", "errors"))
					.andExpect(flash().attribute("type", "danger")).andExpect(redirectedUrl("/subject"))
					.andExpect(unauthenticated());

			// check email got sent, no async queues configured
			MimeMessage[] messages = greenMail.getReceivedMessages();
			assertNotNull(messages);
			assertEquals(0, messages.length);

			assertEquals(0, countRowsInTableWhere(jdbcTemplate, "subject",
					"username = '__deleted__this_is_not_a_real_uuid@tetsuwan-tech.com' AND status = 'LOCKED'"));

		} catch (Exception exception) {
			fail(exception);
		}
	}

	// GET /subject/deactivate?uuid=9563a95d-1466-42fb-b6eb-eb87ca8ea82a
	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, uuid, uuid_expires, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', '9563a95d-1466-42fb-b6eb-eb87ca8ea82a', DATEADD('DAY', 1, CURRENT_TIMESTAMP()), 'PENDING')")
	@WithAnonymousUser
	@Test
	void testDeactivate_SubjectIsNotEnabled() {
		try {

			mockMvc.perform(get("/subject/deactivate").param("uuid", "9563a95d-1466-42fb-b6eb-eb87ca8ea82a"))
					.andExpect(status().is3xxRedirection()).andExpect(flash().attribute("message", "errors"))
					.andExpect(flash().attribute("type", "danger")).andExpect(redirectedUrl("/subject"))
					.andExpect(unauthenticated());

			// check email got sent, no async queues configured
			MimeMessage[] messages = greenMail.getReceivedMessages();
			assertNotNull(messages);
			assertEquals(0, messages.length);

			assertEquals(0, countRowsInTableWhere(jdbcTemplate, "subject",
					"username = '__deleted__9563a95d-1466-42fb-b6eb-eb87ca8ea82a@tetsuwan-tech.com' AND status = 'LOCKED'"));

		} catch (Exception exception) {
			fail(exception);
		}
	}

	// POST /subject/deactivate?uuid=9563a95d-1466-42fb-b6eb-eb87ca8ea82a
	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, uuid, uuid_expires, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', '9563a95d-1466-42fb-b6eb-eb87ca8ea82a', DATEADD('DAY', -1, CURRENT_TIMESTAMP()), 'ENABLED')")
	@Sql(statements = "INSERT INTO subject_authority (authority, subject_id) VALUES ('ROLE_USER', (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com'))")
	@WithAnonymousUser
	@Test
	void testDeactivate_UuidIsExpired() {
		try {

			mockMvc.perform(get("/subject/deactivate").param("uuid", "9563a95d-1466-42fb-b6eb-eb87ca8ea82a"))
					.andExpect(status().is3xxRedirection()).andExpect(flash().attribute("message", "errors"))
					.andExpect(flash().attribute("type", "danger")).andExpect(redirectedUrl("/subject"))
					.andExpect(unauthenticated());

			// check email got sent, no async queues configured
			MimeMessage[] messages = greenMail.getReceivedMessages();
			assertNotNull(messages);
			assertEquals(0, messages.length);

			assertEquals(0, countRowsInTableWhere(jdbcTemplate, "subject",
					"username = '__deleted__9563a95d-1466-42fb-b6eb-eb87ca8ea82a@tetsuwan-tech.com' AND status = 'LOCKED'"));

		} catch (Exception exception) {
			fail(exception);
		}
	}

	// GET /subject/disconnect?clientRegistrationId=google
	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, uuid, uuid_expires, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', '9563a95d-1466-42fb-b6eb-eb87ca8ea82a', DATEADD('DAY', -1, CURRENT_TIMESTAMP()), 'ENABLED')")
	@Sql(statements = "INSERT INTO subject_authority (authority, subject_id) VALUES ('ROLE_USER', (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com'))")
	@WithUserDetails(value = "example@tetsuwan-tech.com")
	@Test
	void testDisconnect() {
		try {

			ClientRegistration clientRegistration = ClientRegistration.withRegistrationId("google")
					.authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
					.clientId("clientId")
					.redirectUriTemplate("https://tetsuwan-tech.com/atom-project")
					.authorizationUri("https://provider.com/oauth2/authorization")
					.tokenUri("https://provider.com/oauth2/token")
					.userInfoUri("https://provider.com/oauth2/user")
					.userNameAttributeName("userNameAttributeName")
					.build();

			SubjectModel subject = SubjectUtils.getSubject();
			mockMvc.perform(get("/subject/disconnect")
					.with(oauth2Login().oauth2User(subject).clientRegistration(clientRegistration))
					.param("clientRegistrationId", "google"))
					.andExpect(forwardedUrl("/profile/reset?source=oauth2.disconnect"))
					.andExpect(authenticated());

			// check email got sent, no async queues configured
			MimeMessage[] messages = greenMail.getReceivedMessages();
			assertNotNull(messages);
			assertEquals(0, messages.length);

		} catch (Exception exception) {
			fail(exception);
		}
	}

	// GET /subject/disconnect
	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, uuid, uuid_expires, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', '9563a95d-1466-42fb-b6eb-eb87ca8ea82a', DATEADD('DAY', -1, CURRENT_TIMESTAMP()), 'ENABLED')")
	@Sql(statements = "INSERT INTO subject_authority (authority, subject_id) VALUES ('ROLE_USER', (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com'))")
	@WithUserDetails(value = "example@tetsuwan-tech.com")
	@Test
	void testDisconnect_withDefault() {
		try {

			ClientRegistration clientRegistration = ClientRegistration.withRegistrationId("google")
					.authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
					.clientId("clientId")
					.redirectUriTemplate("https://tetsuwan-tech.com/atom-project")
					.authorizationUri("https://provider.com/oauth2/authorization")
					.tokenUri("https://provider.com/oauth2/token")
					.userInfoUri("https://provider.com/oauth2/user")
					.userNameAttributeName("userNameAttributeName")
					.build();

			SubjectModel subject = SubjectUtils.getSubject();
			mockMvc.perform(get("/subject/disconnect")
					.with(oauth2Login().oauth2User(subject).clientRegistration(clientRegistration)))
					.andExpect(forwardedUrl("/profile/reset?source=oauth2.disconnect"))
					.andExpect(authenticated());

			// check email got sent, no async queues configured
			MimeMessage[] messages = greenMail.getReceivedMessages();
			assertNotNull(messages);
			assertEquals(0, messages.length);

		} catch (Exception exception) {
			fail(exception);
		}
	}
}
