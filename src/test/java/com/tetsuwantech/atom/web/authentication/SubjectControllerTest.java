package com.tetsuwantech.atom.web.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.atMostOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.net.MalformedURLException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizedClientRepository;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tetsuwantech.atom.database.authentication.ClientRegistration;
import com.tetsuwantech.atom.database.authentication.Subject;
import com.tetsuwantech.atom.database.authentication.Subject.Status;
import com.tetsuwantech.atom.manager.authentication.SubjectManager;
import com.tetsuwantech.atom.util.DateTimePeriodUtils;
import com.tetsuwantech.atom.util.MailSender;
import com.tetsuwantech.atom.util.MailSender.Message;
import com.tetsuwantech.atom.util.authentication.SubjectHandler;

@ExtendWith(MockitoExtension.class)
class SubjectControllerTest {

	@Mock
	private Authentication authentication;

	@Mock
	private SecurityContext securityContext;

	@Mock
	private MailSender mailSender;

	@Mock
	private SubjectHandler subjectHandler;

	@Mock
	private SubjectManager subjectManager;

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Mock
	private BindingResult bindingResult;

	@Mock
	private Model model;

	@Mock
	private RedirectAttributes redirectAttributes;

	@Mock
	private DateTimePeriodUtils dateTimePeriodUtils;

	@Mock
	private OAuth2AuthorizedClientRepository authorizedClientRepository;

	@InjectMocks
	private SubjectController subjectController;

	private static final Instant now = Instant.now();

	@BeforeEach
	void setUp() {
		subjectController.setAtomActivate("/atom");
		subjectController.setAtomDeactivate("/subject?status=deactivate.valid&type=danger");
	}

	// GET /subject
	@Test
	void testMain() {

		ArgumentCaptor<String> argumentCapture = ArgumentCaptor.forClass(String.class);
		when(model.addAttribute(eq("message"), argumentCapture.capture())).thenReturn(model);

		String result = subjectController.main("logout.success", model);

		verify(model).addAttribute("message", argumentCapture.getValue());
		assertEquals("subjects.logout.success", argumentCapture.getValue());
		assertEquals("/subjects/login", result);
	}

	// GET /subject/forgot
	@Test
	void testForgotModel() {

		when(model.addAttribute(anyString(), anyString())).thenReturn(model);

		String result = subjectController.forgot(model);

		verify(model).addAttribute("message", "subjects.forgot.edit");
		assertEquals("/subjects/forgot", result);
	}

	// POST /subject/forgot
	// username=example@tetsuwan-tech.com
	@Test
	void testForgotHttpServletRequestUsernameModelBindingResultModel_UsernameIsValid() {

		when(bindingResult.hasErrors()).thenReturn(false);
		ArgumentCaptor<String> argumentCapture = ArgumentCaptor.forClass(String.class);
		when(subjectManager.findFirst(argumentCapture.capture())).then(invocation -> {
			SubjectModel subject = new SubjectModel();
			subject.setUsername(invocation.getArgument(0));
			return subject;
		});

		when(request.getScheme()).thenReturn("https");
		when(request.getServerName()).thenReturn("tetsuwan-tech.com");
		when(request.getServerPort()).thenReturn(443);
		when(request.getContextPath()).thenReturn("");

		doNothing().when(subjectManager).save(any(SubjectModel.class));
		doNothing().when(mailSender).send(any(SubjectModel.class), eq(Message.FORGOT_PASSWORD), anyMap());

		UsernameModel username = new UsernameModel();
		username.setUsername("example@tetsuwan-tech.com");

		String result = "";
		try {
			result = subjectController.forgot(request, username, bindingResult, model, redirectAttributes);
		} catch (MalformedURLException exception) {
			fail(exception);
		}

		verify(bindingResult).hasErrors();
		verify(subjectManager).findFirst(argumentCapture.getValue());
		verify(subjectManager).save(any(SubjectModel.class));
		verify(mailSender).send(any(SubjectModel.class), eq(Message.FORGOT_PASSWORD), anyMap());

		verify(redirectAttributes).addFlashAttribute("message", "subjects.forgot.send");
		verify(redirectAttributes).addFlashAttribute("args", new String[] { argumentCapture.getValue() });

		assertEquals("example@tetsuwan-tech.com", argumentCapture.getValue());
		assertEquals("redirect:/subject", result);

	}

	// GET /subject/password?uuid=35cc3700-b238-4500-9c16-8efa9157ae4f
	@Test
	void testPasswordStringModel_UuidIsValid() {

		when(bindingResult.hasErrors()).thenReturn(false);
		when(model.addAttribute(anyString(), anyString())).thenReturn(model);
		UuidModel uuid = new UuidModel("35cc3700-b238-4500-9c16-8efa9157ae4f");

		String result = subjectController.password(uuid, bindingResult, model);

		verify(model).addAttribute("message", "subjects.password.edit");
		assertEquals("/subjects/password", result);
	}

	// POST
	// /subject/password?uuid=35cc3700-b238-4500-9c16-8efa9157ae4f&password1=Example235!&password2=Example235!
	@Test
	void testPasswordHttpServletRequestHttpServletResponsePasswordModelBindingResultModelRedirectAttributes() {

		when(dateTimePeriodUtils.now()).thenReturn(now);
		when(bindingResult.hasErrors()).thenReturn(false);
		ArgumentCaptor<String> argumentCapture = ArgumentCaptor.forClass(String.class);
		when(subjectManager.findFirst(argumentCapture.capture(), eq("uuid"))).then(invocation -> {

			Subject subject = new Subject();
			subject.setUsername("example@tetsuwan-tech.com");
			subject.setUuid(invocation.getArgument(0));
			subject.setUuidExpires(ChronoUnit.DAYS.addTo(dateTimePeriodUtils.now(), 1L));
			subject.setStatus(Status.ENABLED);

			SubjectModel subjectModel = new SubjectModel(subject);
			subjectModel.setId(1L);
			return subjectModel;
		});

		doNothing().when(subjectManager).save(any(SubjectModel.class));

		UuidModel uuid = new UuidModel("35cc3700-b238-4500-9c16-8efa9157ae4f");
		PasswordModel password = new PasswordModel("", "");

		SubjectModel subject = new SubjectModel(1L);
		subject.setUsername("example1@tetsuwan-tech.com");

		SecurityContextHolder.setContext(securityContext);
		String result = subjectController.password(request, response, password, bindingResult, uuid, bindingResult, model, redirectAttributes);
		SecurityContextHolder.clearContext();

		verify(bindingResult, times(2)).hasErrors();
		verify(subjectManager).findFirst("35cc3700-b238-4500-9c16-8efa9157ae4f", "uuid");
		verify(subjectManager).save(any(SubjectModel.class));
		verify(redirectAttributes).addFlashAttribute("message", "subjects.password.save");

		assertEquals("redirect:/atom", result);
	}

	// POST /subject/save
	// username=example@tetsuwan-tech&password1=Example235!&familyName=familyName&givenName=GivenName&timezone=Australia/Sydney
	@Test
	void testSave() {

		when(bindingResult.hasErrors()).thenReturn(false);
		doNothing().when(subjectManager).save(any(SubjectModel.class));

		when(request.getScheme()).thenReturn("https");
		when(request.getServerName()).thenReturn("tetsuwan-tech.com");
		when(request.getServerPort()).thenReturn(443);
		when(request.getContextPath()).thenReturn("");

		doNothing().when(mailSender).send(any(SubjectModel.class), eq(Message.CREATE_ACCOUNT), anyMap());

		when(model.addAttribute(anyString(), any())).thenReturn(model);

		RegisterModel register = new RegisterModel();
		register.setUsername(new UsernameModel("example@tetsuwan-tech.com"));

		PasswordModel password = new PasswordModel("Example235!", "Example235!");
		register.setPassword(password);
		register.setName(new NameModel("givenName", "familyName"));
		register.setTimeZone("Australia/Sydney");

		String result = "";
		try {
			result = subjectController.save(request, register, bindingResult, model, redirectAttributes);
		} catch (MalformedURLException exception) {
			fail(exception);
		}

		verify(bindingResult).hasErrors();
		verify(subjectManager).save(any(SubjectModel.class));
		verify(mailSender).send(any(SubjectModel.class), eq(Message.CREATE_ACCOUNT), anyMap());

		verify(model).addAttribute("message", "subjects.edit.send");
		verify(model).addAttribute("args", new String[] { "example@tetsuwan-tech.com" });

		assertEquals("/subjects/login", result);
	}

	// GET /subject/activate?uuid=35cc3700-b238-4500-9c16-8efa9157ae4f&enabled=false
	@Test
	void testActivate() {

		when(dateTimePeriodUtils.now()).thenReturn(now);
		when(bindingResult.hasErrors()).thenReturn(false);
		when(subjectManager.findFirst("35cc3700-b238-4500-9c16-8efa9157ae4f", "uuid")).then(invocation -> {

			Subject subject = new Subject();
			subject.setUsername("example@tetsuwan-tech.com");
			subject.setUuid(invocation.getArgument(0));
			subject.setUuidExpires(ChronoUnit.DAYS.addTo(dateTimePeriodUtils.now(), 1L));
			subject.setStatus(Status.PENDING);

			SubjectModel subjectModel = new SubjectModel(subject);
			subjectModel.setId(1L);

			return subjectModel;
		});

		doNothing().when(subjectManager).activate(any(SubjectModel.class));

		ArgumentCaptor<Long> argumentCapture = ArgumentCaptor.forClass(Long.class);
		when(subjectManager.get(argumentCapture.capture())).then(invocation -> {

			SubjectModel subject = new SubjectModel((Long) invocation.getArgument(0));
			subject.setUsername("example@tetsuwan-tech.com");
			subject.setStatus(Status.ENABLED);

			return subject;
		});

		doNothing().when(subjectHandler).build(any(SubjectModel.class), isNull());
		doNothing().when(subjectHandler).login(any(SubjectModel.class), eq(request), eq(response));
		doNothing().when(mailSender).send(any(SubjectModel.class), eq(Message.WELCOME), anyMap());

		UuidModel uuid = new UuidModel("35cc3700-b238-4500-9c16-8efa9157ae4f");
		uuid.setEnabled(false);

		SecurityContextHolder.setContext(securityContext);
		String result = subjectController.activate(request, response, uuid, bindingResult, model, redirectAttributes);
		SecurityContextHolder.clearContext();

		verify(subjectManager).findFirst("35cc3700-b238-4500-9c16-8efa9157ae4f", "uuid");
		verify(subjectManager).activate(any(SubjectModel.class));
		verify(subjectManager).get(argumentCapture.getValue());
		verify(subjectHandler).build(any(SubjectModel.class), isNull());
		verify(subjectHandler).login(any(SubjectModel.class), eq(request), eq(response));
		verify(mailSender).send(any(SubjectModel.class), eq(Message.WELCOME), anyMap());

		assertEquals("redirect:/atom", result);
	}

	// GET /subject/deactivate?uuid=35cc3700-b238-4500-9c16-8efa9157ae4f
	@Test
	void testDeactivate() {

		when(dateTimePeriodUtils.now()).thenReturn(now);
		when(bindingResult.hasErrors()).thenReturn(false);
		when(subjectManager.findFirst("35cc3700-b238-4500-9c16-8efa9157ae4f", "uuid")).then(invocation -> {

			Subject subject = new Subject();
			subject.setUsername("example@tetsuwan-tech.com");
			subject.setUuid(invocation.getArgument(0));
			subject.setUuidExpires(ChronoUnit.DAYS.addTo(dateTimePeriodUtils.now(), 1L));
			subject.setStatus(Status.ENABLED);

			SubjectModel subjectModel = new SubjectModel(subject);
			subjectModel.setId(1L);

			return subjectModel;
		});

		doNothing().when(subjectHandler).deactivate(any(SubjectModel.class));
		doNothing().when(subjectManager).deactivate(any(SubjectModel.class));

		String result = "";
		UuidModel uuid = new UuidModel("35cc3700-b238-4500-9c16-8efa9157ae4f");

		when(securityContext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securityContext);
		try {
			result = subjectController.deactivate(request, uuid, bindingResult, model, redirectAttributes);
		} catch (ServletException exception) {
			fail(exception);
		}
		SecurityContextHolder.clearContext();

		verify(subjectManager).findFirst("35cc3700-b238-4500-9c16-8efa9157ae4f", "uuid");
		verify(subjectManager).deactivate(any(SubjectModel.class));
		verify(subjectHandler).deactivate(any(SubjectModel.class));

		assertEquals("redirect:/subject?status=deactivate.valid&type=danger", result);
	}

	// GET /subject/disconnect
	@Test
	void testDisconnect() {

		SubjectModel subjectModel = new SubjectModel(1L);
		subjectModel.setUsername("example@tetsuwan-tech.com");

		when(securityContext.getAuthentication()).thenReturn(authentication);
		when(authentication.getPrincipal()).thenReturn(subjectModel);
		doNothing().when(authorizedClientRepository).removeAuthorizedClient("google", authentication, request, response);
		subjectController.setAtomProfile("/profile");

		SecurityContextHolder.setContext(securityContext);
		String result = subjectController.disconnect(ClientRegistration.google, request, response, model);
		SecurityContextHolder.clearContext();

		verify(securityContext, times(2)).getAuthentication();
		verify(authentication, times(2)).getPrincipal();
		verify(authorizedClientRepository, atMostOnce()).removeAuthorizedClient("google", authentication, request, response);

		assertEquals("forward:/profile?source=oauth2.disconnect", result);

	}

}
