package com.tetsuwantech.atom.web.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 - 2020 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Instant;
import java.time.ZoneId;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.tetsuwantech.atom.database.authentication.Authority;
import com.tetsuwantech.atom.database.authentication.Authority.Type;
import com.tetsuwantech.atom.database.authentication.Subject;
import com.tetsuwantech.atom.database.authentication.Subject.Status;

class SubjectModelTest {

	private SubjectModel subject;

	private Instant NOW = Instant.now();

	@BeforeEach
	protected void setUp() {

		Subject subjectEntity = new Subject("example@tetsuwan-tech.com");
		subjectEntity.setJoined(NOW.minusSeconds(1L));
		subjectEntity.setGivenName("givenName");
		subjectEntity.setFamilyName("familyName");
		subjectEntity.setLastLogin(NOW);
		subjectEntity.setTimeZone("UTC");
		subjectEntity.setStatus(Status.ENABLED);
		subjectEntity.setUuid("9563a95d-1466-42fb-b6eb-eb87ca8ea82a");
		subjectEntity.setUuidExpires(NOW.plusSeconds(1L));
		subjectEntity.setPassword("$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO");
		subjectEntity.setPasswordExpires(NOW.plusSeconds(1L));

		Authority authority = new Authority();
		authority.setAuthority(Type.ROLE_USER);
		List<Authority> authorities = List.of(authority);
		subjectEntity.setAuthorities(authorities);

		subject = new SubjectModel(subjectEntity);
	}

	@Test
	void testGetLastLogin() {

		Instant result = subject.getLastLogin();

		assertEquals(NOW, result);
	}

	@Test
	void testGetGivenName() {

		String result = subject.getGivenName();

		assertEquals("givenName", result);
	}

	@Test
	void testGetFamilyName() {

		String result = subject.getFamilyName();

		assertEquals("familyName", result);
	}

	@Test
	void testGetFullName() {

		String result = subject.getFullName();

		assertEquals("givenName familyName", result);
	}

	@Test
	void testGetTimeZone() {

		String result = subject.getTimeZone();

		assertEquals("UTC", result);
	}

	@Test
	void testGetTimeZoneId() {

		ZoneId result = subject.getTimeZoneId();

		assertEquals(ZoneId.of("UTC"), result);
	}

	@Test
	void testGetPassword() {

		String result = subject.getPassword();

		assertEquals("$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO", result);
	}

	@Test
	void testGetUsername() {

		String result = subject.getUsername();

		assertEquals("example@tetsuwan-tech.com", result);
	}

	@Test
	void testGetJoined() {

		Instant result = subject.getJoined();

		assertEquals(NOW.minusSeconds(1L), result);
	}

	@Test
	void testGetUuid() {

		String result = subject.getUuid();

		assertEquals("9563a95d-1466-42fb-b6eb-eb87ca8ea82a", result);
	}

	@Test
	void testGetUuidExpired() {

		Instant result = subject.getUuidExpired();

		assertEquals(NOW.plusSeconds(1L), result);
	}

	@Test
	void testGetStatus() {

		Status result = subject.getStatus();

		assertEquals(Status.ENABLED, result);
	}

	@Test
	void testIsSocial() {

		Boolean result = subject.isSocial();

		assertFalse(result);
	}

	@Test
	void testIsDeactivatable() {

		Boolean result = subject.isDeactivatable();

		assertTrue(result);
	}

	@Test
	void testIsAccountNonExpired_true() {

		Boolean result = subject.isAccountNonExpired();

		assertTrue(result);
	}

	@Test
	void testIsAccountNonExpired_false() {

		subject.setStatus(Status.EXPIRED);

		Boolean result = subject.isAccountNonExpired();

		assertFalse(result);
	}

	@Test
	void testIsAccountNonLocked_true() {

		Boolean result = subject.isAccountNonLocked();

		assertTrue(result);
	}

	@Test
	void testIsAccountNonLocked_false() {

		subject.setStatus(Status.LOCKED);

		Boolean result = subject.isAccountNonLocked();

		assertFalse(result);
	}

	@Test
	void testIsCredentialsNonExpired_true() {

		Boolean result = subject.isCredentialsNonExpired();

		assertTrue(result);
	}

	@Test
	void testIsCredentialsNonExpired_false() {

		subject.setPasswordExpires(Instant.MIN);

		Boolean result = subject.isCredentialsNonExpired();

		assertFalse(result);
	}

	@Test
	void testIsEnabled_true() {

		Boolean result = subject.isEnabled();

		assertTrue(result);
	}

	@Test
	void testIsEnabled_false() {

		subject.setStatus(Status.EXPIRED);

		Boolean result = subject.isEnabled();

		assertFalse(result);
	}

	@Test
	void testIsPending_true() {

		subject.setStatus(Status.PENDING);

		Boolean result = subject.isPending();

		assertTrue(result);
	}

	@Test
	void testIsPending_false() {

		subject.setStatus(Status.ENABLED);

		Boolean result = subject.isPending();

		assertFalse(result);
	}

	@Test
	void testGetAvatarUrl_noSocial() {

		String result = subject.getAvatarUrl();

		assertEquals("https://www.gravatar.com/avatar/48b4cadbacf99e2d6ad8f26b7d0f1600", result);
	}

	@Test
	void testGetAvatarUrl_isSocial() {

		subject.setSocial(true);
		subject.setAvatarUrl("https://lh3.googleusercontent.com/a-/AOh14GiAX7196DzIRbFPAq8L1Xf8MUHCm1NAGm3x0uwc");

		String result = subject.getAvatarUrl();

		assertEquals("https://lh3.googleusercontent.com/a-/AOh14GiAX7196DzIRbFPAq8L1Xf8MUHCm1NAGm3x0uwc", result);
	}

	@Test
	void testGetClientRegistrationId_isSetDirectly() {

		subject.setClientRegistrationId("clientRegistrationId");

		String result = subject.getClientRegistrationId();

		assertEquals("clientRegistrationId", result);
	}

	@Test
	void testGetClientRegistrationId_isNotSocial() {

		subject.setSocial(false);

		String result = subject.getClientRegistrationId();

		assertNull(result);
	}

	@Test
	void testGetAuthorities() {

		Collection<? extends GrantedAuthority> result = subject.getAuthorities();

		assertEquals(List.of(new SimpleGrantedAuthority("ROLE_USER")), result);
	}

	@Test
	void testGetAttributes() {

		Map<String, Object> results = subject.getAttributes();

		assertEquals(
				Map.of("name", "givenName familyName",
						"email", "example@tetsuwan-tech.com",
						"given_name", "givenName",
						"family_name", "familyName",
						"picture", "https://www.gravatar.com/avatar/48b4cadbacf99e2d6ad8f26b7d0f1600"),
				results);
	}

	@Test
	void testGetName() {

		String result = subject.getName();

		assertEquals("givenName familyName", result);
	}
}
