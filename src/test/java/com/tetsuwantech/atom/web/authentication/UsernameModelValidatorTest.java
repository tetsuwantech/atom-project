package com.tetsuwantech.atom.web.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import com.tetsuwantech.atom.database.authentication.Subject.Status;
import com.tetsuwantech.atom.manager.authentication.SubjectManager;

@ExtendWith(MockitoExtension.class)
class UsernameModelValidatorTest {

	@Mock
	private SubjectManager subjectManager;

	private UsernameModelValidator usernameModelValidator;

	@Test
	void test_Supports() {
		usernameModelValidator = new UsernameModelValidator(subjectManager, true);
		assertTrue(usernameModelValidator.supports(UsernameModel.class));
	}

	@Test
	void testValidate_UsernameExists() {

		usernameModelValidator = new UsernameModelValidator(subjectManager, false);
		ArgumentCaptor<String> argumentCapture = ArgumentCaptor.forClass(String.class);
		when(subjectManager.findFirst(argumentCapture.capture())).then(invocation -> {

			SubjectModel subject = new SubjectModel(1L);
			subject.setUsername(invocation.getArgument(0));
			subject.setPassword("Example235!");
			subject.setStatus(Status.ENABLED);

			return subject;
		});

		UsernameModel username = new UsernameModel("example@tetsuwan-tech.com");
		Errors errors = new BeanPropertyBindingResult(username, SubjectModel.USERNAME);
		usernameModelValidator.validate(username, errors);

		verify(subjectManager).findFirst(argumentCapture.getValue());
		assertFalse(errors.hasErrors());
	}

	@Test
	void testValidate_DoesNotExist() {

		ArgumentCaptor<String> argumentCapture = ArgumentCaptor.forClass(String.class);
		when(subjectManager.findFirst(argumentCapture.capture())).thenReturn(null);

		UsernameModel username = new UsernameModel("example@tetsuwan-tech.com");
		Errors errors = new BeanPropertyBindingResult(username, SubjectModel.USERNAME);

		usernameModelValidator = new UsernameModelValidator(subjectManager, false);
		usernameModelValidator.validate(username, errors);

		verify(subjectManager).findFirst(argumentCapture.getValue());
		assertTrue(errors.hasErrors());
		assertTrue(Arrays.asList(errors.getFieldError(SubjectModel.USERNAME).getCodes()).contains("Forgot.notFound"));
	}

	@Test
	void testValidate_IsNotEnabled() {

		usernameModelValidator = new UsernameModelValidator(subjectManager, false);
		ArgumentCaptor<String> argumentCapture = ArgumentCaptor.forClass(String.class);
		when(subjectManager.findFirst(argumentCapture.capture())).then(invocation -> {

			SubjectModel subject = new SubjectModel(1L);
			subject.setUsername(invocation.getArgument(0));
			subject.setStatus(Status.LOCKED);

			return subject;
		});

		UsernameModel username = new UsernameModel("example@tetsuwan-tech.com");
		Errors errors = new BeanPropertyBindingResult(username, SubjectModel.USERNAME);
		usernameModelValidator.validate(username, errors);

		verify(subjectManager).findFirst(argumentCapture.getValue());
		assertTrue(errors.hasErrors());
		assertTrue(Arrays.asList(errors.getFieldError(SubjectModel.USERNAME).getCodes()).contains("subjects.login.locked"));
	}

	@Test
	void testValidate_IsEmptyPassword() {

		usernameModelValidator = new UsernameModelValidator(subjectManager, false);
		ArgumentCaptor<String> argumentCapture = ArgumentCaptor.forClass(String.class);
		when(subjectManager.findFirst(argumentCapture.capture())).then(invocation -> {

			SubjectModel subject = new SubjectModel(1L);
			subject.setUsername(invocation.getArgument(0));
			subject.setStatus(Status.ENABLED);

			return subject;
		});

		UsernameModel username = new UsernameModel("example@tetsuwan-tech.com");
		Errors errors = new BeanPropertyBindingResult(username, SubjectModel.USERNAME);
		usernameModelValidator.validate(username, errors);

		verify(subjectManager).findFirst(argumentCapture.getValue());
		assertTrue(errors.hasErrors());
		assertTrue(Arrays.asList(errors.getFieldError(SubjectModel.USERNAME).getCodes()).contains("oauth2.login.password"));
	}

	@Test
	void testValidate_IsAvailable() {

		ArgumentCaptor<String> argumentCapture = ArgumentCaptor.forClass(String.class);
		when(subjectManager.findFirst(argumentCapture.capture())).thenReturn(null);
		when(subjectManager.isDomainAllowed(anyString())).thenReturn(true);

		UsernameModel username = new UsernameModel("example@tetsuwan-tech.com");
		Errors errors = new BeanPropertyBindingResult(username, SubjectModel.USERNAME);

		usernameModelValidator = new UsernameModelValidator(subjectManager, true);
		usernameModelValidator.validate(username, errors);

		verify(subjectManager).findFirst(argumentCapture.getValue());
		assertFalse(errors.hasErrors());
	}

	@Test
	void testValidate_IsNotAvailable() {

		ArgumentCaptor<String> argumentCapture = ArgumentCaptor.forClass(String.class);
		when(subjectManager.findFirst(argumentCapture.capture())).then(invocation -> {

			SubjectModel subject = new SubjectModel(1L);
			subject.setUsername(invocation.getArgument(0));

			return subject;
		});

		UsernameModel username = new UsernameModel("example@tetsuwan-tech.com");
		Errors errors = new BeanPropertyBindingResult(username, SubjectModel.USERNAME);

		usernameModelValidator = new UsernameModelValidator(subjectManager, true);
		usernameModelValidator.validate(username, errors);

		verify(subjectManager).findFirst(argumentCapture.getValue());
		assertTrue(errors.hasErrors());
		assertTrue(Arrays.asList(errors.getFieldError(SubjectModel.USERNAME).getCodes()).contains("Subject.UsernameUsed"));
	}

	@Test
	void testValidate_NotValidAddress() {

		ArgumentCaptor<String> argumentCapture = ArgumentCaptor.forClass(String.class);
		when(subjectManager.findFirst(argumentCapture.capture())).thenReturn(null);
		when(subjectManager.isDomainAllowed(anyString())).thenReturn(false);

		UsernameModel username = new UsernameModel("bob@notvaliddomain.com");
		Errors errors = new BeanPropertyBindingResult(username, SubjectModel.USERNAME);

		usernameModelValidator = new UsernameModelValidator(subjectManager, true);
		usernameModelValidator.validate(username, errors);

		verify(subjectManager).findFirst(argumentCapture.getValue());
		assertTrue(errors.hasErrors());
		assertTrue(Arrays.asList(errors.getFieldError(SubjectModel.USERNAME).getCodes()).contains("Username.invalidDomain"));
	}
}
