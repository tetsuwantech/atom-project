package com.tetsuwantech.atom.web.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 - 2019 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import com.tetsuwantech.atom.database.authentication.Subject;
import com.tetsuwantech.atom.database.authentication.Subject.Status;
import com.tetsuwantech.atom.manager.authentication.SubjectManager;
import com.tetsuwantech.atom.util.DateTimePeriodUtils;

@ExtendWith(MockitoExtension.class)
class UuidModelValidatorTest {

	@Mock
	private SubjectManager subjectManager;

	@Mock
	private DateTimePeriodUtils dateTimePeriodUtils;

	@InjectMocks
	private UuidModelValidator uuidModelValidator;

	private static final Instant now = Instant.now();

	@Test
	void testSupports() {
		assertTrue(uuidModelValidator.supports(UuidModel.class));
	}

	@Test
	void testValidate_uuidExpired() {

		when(dateTimePeriodUtils.now()).thenReturn(now);
		ArgumentCaptor<String> argumentCapture = ArgumentCaptor.forClass(String.class);
		when(subjectManager.findFirst(argumentCapture.capture(), eq("uuid"))).then(invocation -> {

			Subject subject = new Subject();
			subject.setUsername("example@tetsuwan-tech.com");
			subject.setUuid(invocation.getArgument(0));
			subject.setUuidExpires(dateTimePeriodUtils.now().minusSeconds(1L));
			subject.setStatus(Status.ENABLED);

			SubjectModel subjectModel = new SubjectModel(subject);
			subjectModel.setId(1L);
			return subjectModel;
		});

		UuidModel uuid = new UuidModel("35cc3700-b238-4500-9c16-8efa9157ae4f");

		// Don't use ValidationUtils.invokeValidator
		Errors errors = new BeanPropertyBindingResult(uuid, "uuid");
		uuidModelValidator.validate(uuid, errors);

		assertTrue(errors.hasErrors());
		assertTrue(Arrays.asList(errors.getFieldError("uuid").getCodes()).contains("Uuid.expired"));
	}

	@Test
	void testValidate_uuidNotExpired() {

		when(dateTimePeriodUtils.now()).thenReturn(now);
		ArgumentCaptor<String> argumentCapture = ArgumentCaptor.forClass(String.class);
		when(subjectManager.findFirst(argumentCapture.capture(), eq("uuid"))).then(invocation -> {

			Subject subject = new Subject();
			subject.setUsername("example@tetsuwan-tech.com");
			subject.setUuid(invocation.getArgument(0));
			subject.setUuidExpires(ChronoUnit.DAYS.addTo(dateTimePeriodUtils.now(), 1L));
			subject.setStatus(Status.ENABLED);

			SubjectModel subjectModel = new SubjectModel(subject);
			subjectModel.setId(1L);
			return subjectModel;
		});

		UuidModel uuid = new UuidModel("35cc3700-b238-4500-9c16-8efa9157ae4f");

		// Don't use ValidationUtils.invokeValidator
		Errors errors = new BeanPropertyBindingResult(uuid, "uuid");
		uuidModelValidator.validate(uuid, errors);

		assertFalse(errors.hasErrors());
	}

	@Test
	void testValidate_NonValidUUID() {

		uuidModelValidator.setSubjectManager(null);
		UuidModel uuid = new UuidModel("this_is_not_a_valid_uuid");

		// Don't use ValidationUtils.invokeValidator
		Errors errors = new BeanPropertyBindingResult(uuid, "uuid");
		uuidModelValidator.validate(uuid, errors);

		assertTrue(errors.hasErrors());
		assertTrue(Arrays.asList(errors.getFieldError("uuid").getCodes()).contains("Uuid.notValid"));
	}

	@Test
	void testValidate_uuidNotFound() {

		when(subjectManager.findFirst("35cc3700-b238-4500-9c16-8efa9157ae4f", "uuid")).thenReturn(null);
		UuidModel uuid = new UuidModel("35cc3700-b238-4500-9c16-8efa9157ae4f");

		Errors errors = new BeanPropertyBindingResult(uuid, "uuid");
		uuidModelValidator.validate(uuid, errors);

		verify(subjectManager).findFirst("35cc3700-b238-4500-9c16-8efa9157ae4f", "uuid");
		assertTrue(errors.hasErrors());
		assertTrue(Arrays.asList(errors.getFieldError("uuid").getCodes()).contains("Subject.notFound"));
	}

	@Test
	void testValidate_subjectIsNotEnabled() {

		when(dateTimePeriodUtils.now()).thenReturn(now);
		ArgumentCaptor<String> argumentCapture = ArgumentCaptor.forClass(String.class);
		when(subjectManager.findFirst(argumentCapture.capture(), eq("uuid"))).then(invocation -> {

			Subject subject = new Subject();
			subject.setUsername("example@tetsuwan-tech.com");
			subject.setUuid(invocation.getArgument(0));
			subject.setUuidExpires(ChronoUnit.DAYS.addTo(dateTimePeriodUtils.now(), 1L));
			subject.setStatus(Status.LOCKED);

			SubjectModel subjectModel = new SubjectModel(subject);
			subjectModel.setId(1L);
			return subjectModel;
		});

		UuidModel uuid = new UuidModel("35cc3700-b238-4500-9c16-8efa9157ae4f");
		uuid.setEnabled(false);

		Errors errors = new BeanPropertyBindingResult(uuid, "uuid");
		uuidModelValidator.validate(uuid, errors);

		verify(subjectManager).findFirst("35cc3700-b238-4500-9c16-8efa9157ae4f", "uuid");
		assertTrue(errors.hasErrors());
		assertTrue(Arrays.asList(errors.getFieldError("uuid").getCodes()).contains("Subject.notEnabled"));
	}

	@Test
	void testValidate_subjectIsNotPending() {

		when(dateTimePeriodUtils.now()).thenReturn(now);
		ArgumentCaptor<String> argumentCapture = ArgumentCaptor.forClass(String.class);
		when(subjectManager.findFirst(argumentCapture.capture(), eq("uuid"))).then(invocation -> {

			Subject subject = new Subject();
			subject.setUsername("example@tetsuwan-tech.com");
			subject.setUuid(invocation.getArgument(0));
			subject.setUuidExpires(ChronoUnit.DAYS.addTo(dateTimePeriodUtils.now(), 1L));
			subject.setStatus(Status.ENABLED);

			SubjectModel subjectModel = new SubjectModel(subject);
			subjectModel.setId(1L);
			return subjectModel;
		});

		UuidModel uuid = new UuidModel("35cc3700-b238-4500-9c16-8efa9157ae4f");
		uuid.setEnabled(false);

		Errors errors = new BeanPropertyBindingResult(uuid, "uuid");
		uuidModelValidator.validate(uuid, errors);

		verify(subjectManager).findFirst("35cc3700-b238-4500-9c16-8efa9157ae4f", "uuid");
		assertTrue(errors.hasErrors());
		assertTrue(Arrays.asList(errors.getFieldError("uuid").getCodes()).contains("Subject.notEnabled"));
	}
}
