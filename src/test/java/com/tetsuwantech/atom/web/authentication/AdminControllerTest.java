package com.tetsuwantech.atom.web.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.LinkedList;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.ui.Model;

import com.tetsuwantech.atom.manager.authentication.SubjectManager;

@ExtendWith(MockitoExtension.class)
class AdminControllerTest {

	@Mock
	private SubjectManager subjectManager;

	@Mock
	private Model model;

	@InjectMocks
	private AdminController adminController;

	// GET /admin/subject
	@Test
	void testAdmin() {

		when(subjectManager.findAll()).thenReturn(new LinkedList<>());
		when(model.addAttribute(eq("subjects"), anyList())).thenReturn(model);

		String result = adminController.admin(model);

		verify(subjectManager).findAll();
		verify(model).addAttribute(eq("subjects"), anyList());
		assertEquals("/subjects/main", result);
	}

}
