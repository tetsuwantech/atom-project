package com.tetsuwantech.atom.web.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 - 2020 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrlPattern;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.util.List;

import javax.inject.Inject;
import javax.sql.DataSource;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

@SpringJUnitWebConfig(locations = "classpath:spring-context.xml")
class AdminControllerIT {

	@Inject
	private WebApplicationContext webApplicationContext;

	@Inject
	private DataSource dataSource;

	private JdbcTemplate jdbcTemplate;

	private MockMvc mockMvc;

	@BeforeEach
	void setUp() {
		mockMvc = webAppContextSetup(webApplicationContext).apply(springSecurity()).build();

		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@AfterEach
	void tearDown() {
		JdbcTestUtils.deleteFromTables(jdbcTemplate, "audit", "subject_authority", "subject");
	}

	// GET /admin/subject
	@WithAnonymousUser
	@Test
	void testAdmin_AsAnonymous() {
		try {
			mockMvc.perform(get("/admin/subject")).andExpect(status().is3xxRedirection())
					.andExpect(redirectedUrlPattern("**/subject"));
		} catch (Exception exception) {
			fail(exception);
		}
	}

	// GET /admin/subject
	@WithMockUser
	@Test
	void testAdmin_AsUser() {
		try {
			mockMvc.perform(get("/admin/subject")).andExpect(status().is4xxClientError());
		} catch (Exception exception) {
			fail(exception);
		}
	}

	// GET /admin/subject
	@Sql(statements = "INSERT INTO subject (username, password, joined, password_expires, given_name, family_name, time_zone, uuid, uuid_expires, status) VALUES ('example@tetsuwan-tech.com', '$2a$10$ubP9fZaLbspy9et5apRFSORMBUZ7nY.XUiXWv.Qxa3oOtnllxQFoO', CURRENT_TIMESTAMP(), DATEADD('YEAR', 1, CURRENT_TIMESTAMP()), 'Example', 'Application', 'Australia/Sydney', '9563a95d-1466-42fb-b6eb-eb87ca8ea82a', DATEADD('DAY', 1, CURRENT_TIMESTAMP()), 'ENABLED')")
	@Sql(statements = "INSERT INTO subject_authority (authority, subject_id) VALUES ('ROLE_ADMIN', (SELECT id FROM subject WHERE username = 'example@tetsuwan-tech.com'))")
	@WithUserDetails(value = "example@tetsuwan-tech.com")
	@Test
	void testAdmin_AsAdmin() {
		try {
			mockMvc.perform(get("/admin/subject")).andExpect(status().isOk())
					.andExpect(model().attribute("subjects", instanceOf(List.class)));
		} catch (Exception exception) {
			fail(exception);
		}
	}

}
