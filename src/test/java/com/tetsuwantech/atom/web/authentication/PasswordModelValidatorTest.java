package com.tetsuwantech.atom.web.authentication;

/*-
 * #%L
 * Atom Project
 * %%
 * Copyright (C) 2018 Tetsuwan Technology
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

@ExtendWith(MockitoExtension.class)
class PasswordModelValidatorTest {

	@InjectMocks
	private PasswordModelValidator passwordModelValidator;

	@Test
	void testSupports() {
		assertTrue(passwordModelValidator.supports(PasswordModel.class));
	}

	@Test
	void testValidate_ComplexPassword() {

		PasswordModel password = new PasswordModel("Example235!", "Example235!");
		Errors errors = new BeanPropertyBindingResult(password, "password");

		passwordModelValidator.validate(password, errors);

		assertFalse(errors.hasErrors());
	}

	@Test
	void testValidate_SimplePassword() {

		PasswordModel password = new PasswordModel("password", "password");
		Errors errors = new BeanPropertyBindingResult(password, "password");

		passwordModelValidator.validate(password, errors);

		assertTrue(errors.hasErrors());
		assertTrue(Arrays.asList(errors.getFieldError("password1").getCodes()).contains("Password.invalid"));
	}

	@Test
	void testValidate_DifferentPassword() {

		PasswordModel password = new PasswordModel("First235!", "Second235!");
		Errors errors = new BeanPropertyBindingResult(password, "password");

		passwordModelValidator.validate(password, errors);

		assertTrue(errors.hasErrors());
		assertTrue(Arrays.asList(errors.getFieldError("password2").getCodes()).contains("Password.notSame"));
	}
}
