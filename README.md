[![Maven Central](https://img.shields.io/maven-central/v/com.tetsuwan-tech.atom/atom.svg?label=Maven%20Central)](https://search.maven.org/search?q=g:%22com.tetsuwan-tech.atom%22%20AND%20a:%22atom%22)
![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/tetsuwantech/atom-project)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=tetsuwantech_atom-project&metric=alert_status)](https://sonarcloud.io/dashboard?id=tetsuwantech_atom-project)
![Bitbucket open issues](https://img.shields.io/bitbucket/issues/tetsuwantech/atom-project)

# Atom Project
Atom Project is a starter kit for creating Java Spring MVC projects.

## Notes on versions
Atom Project is no longer under active development - the 4.0.x branch will be the final release version. You can read the announcement here
*  [No more Atom Project development](https://tetsuwan-tech.com/blog/no-more-atom-project-development/)

From version 2.1.0, we moved to a [semantic release number system](https://semver.org/).

## Features
* User registration with email verification
* Login for Google using OAUTH2
* Simple database agnostic layer with security validation
* Middle management layer for business logic
* Integration with Spring Security and Spring Session
* Liquibase for production database upgrades

## Requirements
* Java 11
* Maven (latest)
* SQL database (e.g. PostgreSQL, MySQL)

## Maven
Add to your `pom.xml`, preferably at the top of your `<dependencies>` section.

```xml
<dependency>
  <groupId>com.tetsuwan-tech.atom</groupId>
  <artifactId>atom</artifactId>
  <version>4.0.9.RELEASE</version>
</dependency>
```

## Local Installation for development
Clone the repository with git and check out the latest tag or master

```console
git clone https://bitbucket.org/tetsuwantech/atom-project.git
```

Then install with

```console
mvn install
```

When updates are published, `git pull` the latest code and update your `pom.xml` to use the new version.

```console
git pull
mvn clean install
```

## Usage
Javadoc can be found at [https://tetsuwantech.bitbucket.io/apidocs/](https://tetsuwantech.bitbucket.io/apidocs/)

Try the [Example application](https://bitbucket.org/tetsuwantech/example/) for a base starter project.

## Support
Community Support is through [Stack Overflow](https://stackoverflow.com/questions/tagged/atom-project) with the `atom-project` tag.

You can review current issues through the [Bitbucket issue tracker](https://bitbucket.org/tetsuwantech/atom-project/issues?status=new&status=open).

## Development
You can log an enhancement or proposal through the [Bitbucket issue tracker](https://bitbucket.org/tetsuwantech/atom-project/issues?status=new&status=open).

## Authors and acknowledgment
Atom Project is developed by Jim Richards and Tetsuwan Technology.

## License
Atom Project is licensed with the [GNU LESSER GENERAL PUBLIC LICENSE v. 3.0](https://www.gnu.org/licenses/lgpl-3.0.en.html).
